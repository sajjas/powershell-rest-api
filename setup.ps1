$appPools = 'ey-powershell-scripts', 'ey-rest-api-server'
#$sites = ['ey-powershell-scripts', 'ey-rest-api-server']
#$ports = ['', '']

$hostname = get-content env:computername
$pwd = (Get-Location).Path
$scripts_path = $pwd + "\\scripts"
$rest_api_path = $pwd

foreach ($appPoolName in $appPools) {
	$scriptBlock = {
		Import-Module WebAdministration
		New-Item -Path IIS:\AppPools\$using:appPoolName
		Set-ItemProperty -Path IIS:\AppPools\$using:appPoolName -Name managedRuntimeVersion -Value 'v4.0'
		
		if ($appPoolName -Match 'scripts') {
			$PhysicalPath = $scripts_path
			$port = 10003
		} elseif ($appPoolName -Match 'rest-api') {
			$PhysicalPath = $rest_api_path
			$port = 10009
		}
		
		$newSite = New-IISSite -Name $appPoolName -PhysicalPath $PhysicalPath -BindingInformation "*:${port}:"
		$newSite.Applications["/"].ApplicationPoolName = $site
	}
}

 
Invoke-Command -ComputerName $hostname -ScriptBlock $scriptBlock