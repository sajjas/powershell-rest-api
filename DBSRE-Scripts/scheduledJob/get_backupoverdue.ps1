param(
	#[CmdletBinding()]
    #[Parameter(Mandatory = $true)]
    [String]$JOB_INPUT,
    #[Parameter(Mandatory = $true)]
    [String]$SERVERNAME
	#[Parameter(Mandatory = $true, ValueFromPipeline = $true)]
    #[String]$INSTANCENAME,
	#[Parameter(Mandatory = $false, ValueFromPipeline = $true)]
    #[String]$DBNAME
)

#$json_data = $JOB_INPUT | ConvertFrom-Json 
#$scheduled_job_id = $json_data.scheduled_job_id
#$days_without_backup = $json_data.overdue_backup_days
#$backup_type = $json_data.backup_type
#$db_srv_instance = $SERVERNAME
#$sql_srv_instance = $INSTANCENAME

$scheduled_job_id = 68
$days_without_backup = 1
$backup_type = "FULL"
$db_srv_instance = "DEFRAVMQMISSQ01.eyqa.net"
$sql_srv_instance = "DEFRAVMQMISSQ01.eyqa.net\INST1"

# -----------------------------------------------------------------------------
#          Global Lists Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

# Overdue backup database return list, insert into Alert table and insert into ScheduledJobRunHistory table.
function getOverdueBackupDBS {
	if ($backup_type -eq "FULL"){
		$backup_type_id = "D"
	} elseif ($backup_type -eq "DIFF"){
		$backup_type_id = "I"
	}
	# -----------------------------------------------------------------------------
	log -Message "Db Server:	$db_srv_instance "
	log -Message "Sql Server Instance:	$sql_srv_instance "
	log -Message "Scheduled Job Id:	$scheduled_job_id "
	log -Message "Overdue Backup Days:	$days_without_backup "
	log -Message "Backup Type:	$backup_type "
	
	# -----------------------------------------------------------------------------
	# Fetching List of Overdue Backup Databases List
	$overdue_db_backup_queryString = "
		SELECT DISTINCT msdb.dbo.backupset.database_name 
		FROM msdb.dbo.backupset 
		WHERE msdb..backupset.type = '$backup_type_id' 
		AND (CONVERT(datetime, msdb.dbo.backupset.backup_finish_date, 102) >= GETDATE() - $days_without_backup)
		"
	log -Message "Overdue Backup Databases Query:	$overdue_db_backup_queryString "
	$run_start_time = Get-Date -format "yyyy-MM-dd HH:mm:ss"
	try {
		# Create Remote Session to DB Server Instance
		$remote_session = Create-PSSession -ServerName $db_srv_instance
		log -Message "Session Created to Remote Server: $remote_session"
		# Connect to sql server instance
		$databases = Invoke-Command -Session $remote_session -ScriptBlock {(invoke-sqlcmd -serverInstance $using:sql_srv_instance -query $using:overdue_db_backup_queryString).database_name}
		
		# Disconnect remote-session
		Remove-PSSession $remote_session
		log -Message "Session Removed to Remote Server: $remote_session"
		
		$run_status = "COMPLETED"
		$get_overdue_backup_dbs_query_output_data = @{output = $true; Success_Message = $databases | Out-String} | ConvertTo-Json
	} catch {
		$get_overdue_backup_dbs_query_output_data = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
		$run_status = "FAILED"
	}
	
	$run_end_time = Get-Date -format "yyyy-MM-dd HH:mm:ss"
	$run_output = @{OverdueBackupDatabasesList = $databases | Out-String} | ConvertTo-Json
	log -Message "Overdue Backup Databases List:	$get_overdue_backup_dbs_query_output_data "
	
	$backup_type = @{BackupType = $backup_type} | ConvertTo-Json
	# -----------------------------------------------------------------------------
	if ([String]::IsNullOrEmpty($databases) -ne $true) {
		foreach ($database in $databases) {
			$queryString = "
			INSERT INTO [automation].[Alert]
				   ([TypeId]
				   ,[CreationDate]
				   ,[StatusId]
				   ,[Info]
				   ,[Last_Modified_Date]
				   ,[ServerName]
				   ,[InstanceName]
				   ,[DBName])
				VALUES
				   ('OVERDUE_BACKUP'
				   ,getdate()
				   ,'CREATED'
				   ,'$backup_type'
				   ,getdate()
				   ,'$db_srv_instance'
				   ,'$sql_srv_instance'
				   ,'$database');
			"
			try {
				log -Message "invoke-sqlcmd -ServerInstance $VAR_SERVERINSTANCE -Query $queryString -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD"
				$insert_alert_table_query_output = invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
				$insert_alert_table_query_output_data = @{output = $true; Success_Message = $insert_alert_table_query_output} | ConvertTo-Json
			} catch {
				$insert_alert_table_query_output_data = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
			}
			
			log -Message "Insert into Alert Table Query Output:		$insert_alert_table_query_output_data"
		}
	} 
	# -----------------------------------------------------------------------------
	$queryString = "
		INSERT INTO [automation].[ScheduledJobsRunHistory]
			   ([ScheduledJobid]
			   ,[ServerName]
			   ,[InstanceName]
			   ,[DBName]
			   ,[RunStarttime]
			   ,[RunEndtime]
			   ,[RunStatus]
			   ,[RunOutput])
			VALUES
			   ('$scheduled_job_id'
			   ,'$db_srv_instance'
			   ,'$sql_srv_instance'
			   ,'NULL'
			   ,'$run_start_time'
			   ,'$run_end_time'
			   ,'$run_status'
			   ,'$run_output');
		"
	try {
		log -Message "invoke-sqlcmd -ServerInstance $VAR_SERVERINSTANCE -Query $queryString -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD"
		$insert_scheduled_jobs_run_history_query_output = invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
		$insert_scheduled_jobs_run_history_query_output_data = @{output = $true; Success_Message = $insert_alert_table_query_output} | ConvertTo-Json
	} catch {
		$insert_scheduled_jobs_run_history_query_output_data = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
	}
	log -Message "Insert into Scheduled Job Run History Table Output:	$insert_scheduled_jobs_run_history_query_output_data"
}

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

# Create Session with Remote Server
Function Create-PSSession {

[CmdletBinding()]
Param (
    [Parameter(Mandatory=$True)]
    [string]$ServerName
)

#$pwd = Get-Location
#$DBAToolPassword = Get-Content $("$pwd\\lib\\Security.dll")

    If ($ServerName -match "ey.net" -and $ServerName -notmatch "cloudapp"){
        $InventAccount = "EY\P.SMOO.SQL"
		$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c00000001000000096ace7f05aa0b6d07fc2efd61dc837590000000004800000a000000010000000dad8a97b8df34934ec0b7ca7ff9a8dc2200000007c3a14fd9a2705c7328738b273e01c8c22d1fa6e16f4bdeae0fb3d93b6e45447140000008b2b0878fa9bcebbaeebfc745e4d10c3f87450c4'
    } Elseif ($ServerName -match "eydmz.net"){
        $InventAccount = "EYDMZ.NET\Z.SMOO.SQL"
		$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c00000001000000042078b6ec97af875dc57a61f6e5acfb70000000004800000a00000001000000063fee40630f06fe65d5a890029e274db20000000f40a446ab73abbc9b9fc5a8ca730e95a239213b44bf8f8853555a895fe966e8514000000dae028a7b2685c8bb161610d8ec61ca347f7c58f'
    } Elseif ($ServerName -match "eyxstaging.net"){
        $InventAccount = "EYXSTAGING.NET\X.SMOO.SQL"
		$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c00000001000000013a4bb89ab9f281c99dfb8377aca19370000000004800000a0000000100000004dea6f3aaebc44640e78dfd7c902f26320000000f5dda27a71a8f5674cae5b6ec77f0fd506b25cea68672c0c4c6a7ef847edc7a01400000077599c63c0426eedb4389c3828e1891622dc7746'
    } Elseif ($ServerName -match "eyua.net"){
        $InventAccount = "EYUA\U.SMOO.SQL"
		$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c00000001000000055d6049b8c474aaf10513a187aaed3740000000004800000a00000001000000008dbc01b1f58f720123fd5456efafaea20000000274750b6729d432a9daf8bce46a022ecbafa43c6f2f506f285773ad30362284d1400000014302c7403d736f694d6da8f9d14b8a8527a3167'
    } Elseif ($ServerName -match "eyqa.net"){
        $InventAccount = "EYQA\Q.SMOO.SQL"
		$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c0000000100000009e0a53154ca0f4bbc3023e60360a00f10000000004800000a0000000100000002ffa26e35698842de59dc3ef5ecc6ced2000000076ec6065d55ff521d44a1082a05e7ed2b4e80a2cd0e0d2055d307e6c9a49354e14000000ea09fe22588c8402678de4b675d1a51338bcfbdf'
    } Elseif ($ServerName -match "eydev.net"){
        $InventAccount = "EYDEV\D.SMOO.SQL"
		$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c0000000100000003b01d350867fec8babb90161ef34e9e10000000004800000a00000001000000081b084584d22544eb84f56b07a473d562000000074a68d1d69810e2c3a97af8643b9771845a14db03b0bf89d99e5bd5851f8699f14000000613b04d180fa0326bca311875d9fe3d018da1cbd'
    } Elseif ($ServerName -match "ey.net" -and $ServerName -match "cloudapp"){
        $InventAccount = "CLOUDAPP\C.SQLINVENT"
    } Elseif ($ServerName -match "eydev.net" -and $ServerName -match "cloudapp"){
        $InventAccount = "CLOUDAPPDEV\A.SQLINVENT"
    } Else {
        If ($ServerName -match '^[A-Z]{6,7}[Pp]' -and ($($ServerName.Substring(0,2)) -notmatch "^AC" -and $ServerName -notmatch ".cloudapp.ey.net$")){
            $InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c00000001000000096ace7f05aa0b6d07fc2efd61dc837590000000004800000a000000010000000dad8a97b8df34934ec0b7ca7ff9a8dc2200000007c3a14fd9a2705c7328738b273e01c8c22d1fa6e16f4bdeae0fb3d93b6e45447140000008b2b0878fa9bcebbaeebfc745e4d10c3f87450c4'
            If ($ServerName -notmatch 'ey.net$'){
                $ServerName += ".ey.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Zz]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYDMZ.NET\Z.SMOO.SQL"
			$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c00000001000000042078b6ec97af875dc57a61f6e5acfb70000000004800000a00000001000000063fee40630f06fe65d5a890029e274db20000000f40a446ab73abbc9b9fc5a8ca730e95a239213b44bf8f8853555a895fe966e8514000000dae028a7b2685c8bb161610d8ec61ca347f7c58f'
            If ($ServerName -notmatch 'eydmz.net$'){
                $ServerName += ".eydmz.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Xx]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYXSTAGING.NET\X.SMOO.SQL"
			$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c00000001000000013a4bb89ab9f281c99dfb8377aca19370000000004800000a0000000100000004dea6f3aaebc44640e78dfd7c902f26320000000f5dda27a71a8f5674cae5b6ec77f0fd506b25cea68672c0c4c6a7ef847edc7a01400000077599c63c0426eedb4389c3828e1891622dc7746'
            If ($ServerName -notmatch 'eyxstaging.net$'){
                $ServerName += ".eyxstaging.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Uu]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYUA\U.SMOO.SQL"
			$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c00000001000000055d6049b8c474aaf10513a187aaed3740000000004800000a00000001000000008dbc01b1f58f720123fd5456efafaea20000000274750b6729d432a9daf8bce46a022ecbafa43c6f2f506f285773ad30362284d1400000014302c7403d736f694d6da8f9d14b8a8527a3167'
            If ($ServerName -notmatch 'eyua.net$'){
                $ServerName += ".eyua.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Qq]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYQA\Q.SMOO.SQL"
			$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c0000000100000009e0a53154ca0f4bbc3023e60360a00f10000000004800000a0000000100000002ffa26e35698842de59dc3ef5ecc6ced2000000076ec6065d55ff521d44a1082a05e7ed2b4e80a2cd0e0d2055d307e6c9a49354e14000000ea09fe22588c8402678de4b675d1a51338bcfbdf'
            If ($ServerName -notmatch 'eyqa.net$'){
                $ServerName += ".eyqa.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Dd]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYDEV\D.SMOO.SQL"
			$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c0000000100000003b01d350867fec8babb90161ef34e9e10000000004800000a00000001000000081b084584d22544eb84f56b07a473d562000000074a68d1d69810e2c3a97af8643b9771845a14db03b0bf89d99e5bd5851f8699f14000000613b04d180fa0326bca311875d9fe3d018da1cbd'
            If ($ServerName -notmatch 'eydev.net$'){
                $ServerName += ".eydev.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Pp]' -and $($ServerName.Substring(0,2)) -match "^AC"){
            $InventAccount = "CLOUDAPP\C.SQLINVENT"
            If ($ServerName -notmatch 'cloudapp.ey.net$'){
                $ServerName += ".cloudapp.ey.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Dd]' -and $($ServerName.Substring(0,2)) -match "^AC"){
            $InventAccount = "CLOUDAPPDEV\A.SQLINVENT"
            If ($ServerName -notmatch 'cloudapp.eydev.net$'){
                $ServerName += ".cloudapp.ey.net"
            }
        } Else {
            $InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '01000000d08c9ddf0115d1118c7a00c04fc297eb01000000fe1c79fa8079854594bf0fa189fcf5510000000002000000000003660000c00000001000000096ace7f05aa0b6d07fc2efd61dc837590000000004800000a000000010000000dad8a97b8df34934ec0b7ca7ff9a8dc2200000007c3a14fd9a2705c7328738b273e01c8c22d1fa6e16f4bdeae0fb3d93b6e45447140000008b2b0878fa9bcebbaeebfc745e4d10c3f87450c4'
            If ($ServerName -notmatch 'ey.net$'){
                $ServerName += ".ey.net"
            }
        }
    }
    If ($InventAccount -ne $null) {
		# retrieve the password.
		$securestring = convertto-securestring -string $DBAToolPassword
		$bstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($securestring)
		$passwsord = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstr)
		
        $SecurePassword = ConvertTo-SecureString $passwsord -AsPlainText -Force
        $InventCredential = New-Object System.Management.Automation.PSCredential ($InventAccount, $SecurePassword)
        $RemoteSessionOption = New-PSSessionOption -SkipCACheck -OpenTimeout 180000 -IdleTimeout 180000 #3 minutes
        $Session = New-PSSession -ComputerName $ServerName -Credential $InventCredential -SessionOption $RemoteSessionOption -ErrorAction Stop
        return $Session
    }
}

# -----------------------------------------------------------------------------
function log {
	[CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, Position = 1)]
        [string]$Message
    )
	
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> C:\\overdueBackupLog.txt
}

# Calling Main Function
getOverdueBackupDBS
