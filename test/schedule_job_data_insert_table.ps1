. overdue_backup_scheduled_job.ps1

$SCHEDULE_JOB_INSERT_API = New-UDEndpoint -Url "/schedule-job-table-insert-api" -Method "POST" -Endpoint {

	param($Body)

    # Validate all JSON fields are containing valid JSON
    try {
        $NewBody = ConvertFrom-Json $Body -ErrorAction Stop;
        $validJson = $true;
    } catch {
        $validJson = $false;
    }

    if ($validJson -eq $false) {
        return "Provided text is not a valid JSON string"
    }

	# ----------- Input Parameters --------------
    $Job_Name = $NewBody.Job_Name
	$Alert_Type = $NewBody.Alert_Type
	$Created_By = $NewBody.Created_By
    $Target_Servers = $NewBody.Target_Servers
    $Job_Script_File = $NewBody.Job_Script_File
    $Job_Inputs = $NewBody.Job_Inputs
	$Job_Frequency = $NewBody.Job_Frequency
    $Job_Time = $NewBody.Job_Time

    ######### Trigger Scheduled Job Data Insert into Table #########
	$remote_session = Create-PSSession -ServerName $db_srv_instance
    $db_backup_progress = Invoke-Command -Session $remote_session -ScriptBlock $ScriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name, "backup_create_progress"))
    Remove-PSSession $remote_session
}
