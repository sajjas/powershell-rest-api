use EYDBSRE;
go


DROP SCHEMA IF EXISTS automation;
CREATE SCHEMA automation;


--  Table Domain
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.[Domain] ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.[Domain] ; 
GO

CREATE TABLE EYDBSRE.automation.[Domain] (
	Id int IDENTITY(1,1) NOT NULL,
	DomainName nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Description nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_Domain PRIMARY KEY (Id),
	CONSTRAINT UQ_Domain UNIQUE (DomainName)
) GO
CREATE UNIQUE INDEX UX_Domain_DomainName ON EYDBSRE.automation.[Domain] (DomainName) GO


-- Table: Entity
-- DROP TABLE
IF OBJECT_ID('EYDBSRE.automation.Entity ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.Entity; 
GO

CREATE TABLE EYDBSRE.automation.Entity (
	Id int IDENTITY(1,1) NOT NULL,
	EntityName nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Description nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_Entity PRIMARY KEY (Id),
	CONSTRAINT UQ_Entity UNIQUE (EntityName)
) GO
CREATE UNIQUE INDEX UX_Entity ON EYDBSRE.automation.Entity (EntityName) GO

-- Table : EYDBSRE.automation.LookupMaster

-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.LookupMaster ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.LookupMaster; 
GO

CREATE TABLE EYDBSRE.automation.LookupMaster (
	Id int IDENTITY(1,1) NOT NULL,
	LookupType nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	LookupCode nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	LookupName nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	IsDeleted bit NOT NULL,
	CONSTRAINT PK_LookupMaster PRIMARY KEY (Id)
) GO
CREATE UNIQUE INDEX UX_LookupMaster_LookupTypeCode ON EYDBSRE.automation.LookupMaster (LookupType,LookupCode) GO

-- Table: EYDBSRE.automation.NASShare 
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.NASShare ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.NASShare; 
GO

CREATE TABLE EYDBSRE.automation.NASShare (
	Id int IDENTITY(1,1) NOT NULL,
	RegionId nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	BackupPath nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_NASShare PRIMARY KEY (Id),
	CONSTRAINT UQ_NASShare UNIQUE (RegionId)
) GO
CREATE UNIQUE INDEX UQ_NASShare_RegionId ON EYDBSRE.automation.NASShare (RegionId) GO

-- Table:  EYDBSRE.automation.Region 
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.Region ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.Region; 
GO

CREATE TABLE EYDBSRE.automation.Region (
	Id int IDENTITY(1,1) NOT NULL,
	RegionName nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Description nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_Region  PRIMARY KEY (Id),
	CONSTRAINT UQ_Region_RegionName UNIQUE (RegionName)
) GO
CREATE UNIQUE INDEX UX_Region_RegionName ON EYDBSRE.automation.Region (RegionName) GO

-- Table:  EYDBSRE.automation.Alert 
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.Alert ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.Alert; 
GO

CREATE TABLE EYDBSRE.automation.Alert (
	Id int IDENTITY(1,1) NOT NULL,
	TypeId nvarchar(64)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	StatusId nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Info nvarchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Last_Modified_Date datetime NULL,
	ServerName nvarchar(64)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	InstanceName nvarchar(64)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	DBName nvarchar(64)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Alert PRIMARY KEY (Id)
) GO
-- Add  Non unique Indexes on StatusId and TypeId
CREATE INDEX  IX_Alert_StatusId ON EYDBSRE.automation.Alert (StatusId) GO
CREATE INDEX  IX_Alert_TypeId ON EYDBSRE.automation.Alert (TypeId) GO

-- Table:  Application
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.Application ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.Application; 
GO

CREATE TABLE EYDBSRE.automation.Application (
	Id int IDENTITY(1,1) NOT NULL,
	ApplicationName nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Manageable bit NOT NULL,
	StatusId nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Description nvarchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_Application PRIMARY KEY (Id),
	CONSTRAINT UQ_Application_ApplicationName UNIQUE (ApplicationName)
) GO
CREATE UNIQUE INDEX  UX_Application_ApplicationName ON EYDBSRE.automation.Application (ApplicationName) GO

-- Table: EYDBSRE.automation.[Group] 
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.[Group]  ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.[Group] ; 
GO

CREATE TABLE EYDBSRE.automation.[Group] (
	Id int IDENTITY(1,1) NOT NULL,
	GroupName nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Description nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	EmailGroup nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	StatusId  nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_Group PRIMARY KEY (Id),
	CONSTRAINT UQ_Group_GroupName UNIQUE (GroupName)
) GO
CREATE UNIQUE INDEX UX_Group_GroupName ON EYDBSRE.automation.[Group] (GroupName) GO

-- Table  EYDBSRE.automation.Process
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.Process ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.Process; 
GO

CREATE TABLE EYDBSRE.automation.Process (
	Id int IDENTITY(1,1) NOT NULL,
	ProcessName nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	EntityId int NOT NULL,
	[Action] nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	StatusId  nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_Process PRIMARY KEY (Id),
	CONSTRAINT FK_Process_Entity FOREIGN KEY (EntityId) REFERENCES EYDBSRE.automation.Entity(Id)
) GO

-- Table:  EYDBSRE.automation.ProcessGroupMapping 
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.ProcessGroupMapping ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.ProcessGroupMapping; 
GO

CREATE TABLE EYDBSRE.automation.ProcessGroupMapping (
	Id int IDENTITY(1,1) NOT NULL,
	GroupId int NOT NULL,
	ProcessId int NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_ProcessGroupMapping PRIMARY KEY (Id),
	CONSTRAINT UQ_ProcessGroupMapping_GroupProcessId UNIQUE (GroupId,ProcessId),
	CONSTRAINT FK_GROUP_NAME_ProcessGroupMapping FOREIGN KEY (GroupId) REFERENCES EYDBSRE.automation.[Group](Id),
	CONSTRAINT FK_PROCESSID_ProcessGroupMapping FOREIGN KEY (ProcessId) REFERENCES EYDBSRE.automation.Process(Id)
) GO
CREATE UNIQUE INDEX UX_ProcessGroupMapping_GroupProcessId ON EYDBSRE.automation.ProcessGroupMapping (GroupId,ProcessId) GO



CREATE TABLE EYDBSRE.automation.ScheduledTasks (
	Id int IDENTITY(1,1) NOT NULL,
    ProcessId int  NULL,
	StatusId nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ServerName nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	InstanceName nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DBName nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	TaskInfo nvarchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ReturnInfo nvarchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ScheduledUser nvarchar(64) NULL,
	CreationDate datetime NULL,
	Last_Modified_Date datetime NULL,
	CONSTRAINT PK_ScheduledTasks PRIMARY KEY (Id),
	CONSTRAINT FK_PROCESSID_ScheduledTasks FOREIGN KEY (ProcessId) REFERENCES EYDBSRE.automation.Process(Id)
) GO

CREATE  INDEX IX_ScheduledTasks_ProcessId ON EYDBSRE.automation.ScheduledTasks (ProcessId) GO
CREATE  INDEX IX_ScheduledTasks_StatusId ON EYDBSRE.automation.ScheduledTasks (StatusId) GO
CREATE  INDEX IX_ScheduledTasks_ScheduledUser ON EYDBSRE.automation.ScheduledTasks (ScheduledUser) GO


-- Table: EYDBSRE.automation.Server
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.Server ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.Server; 
GO

CREATE TABLE EYDBSRE.automation.Server (
	Id int IDENTITY(1,1) NOT NULL,
	ServerName nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Description nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	StatusId nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	Last_Modified_Date datetime NULL,
	CONSTRAINT PK_Server PRIMARY KEY (Id),
	CONSTRAINT UQ_Server_ServerName UNIQUE (ServerName)
) GO
CREATE UNIQUE INDEX UX_Server_ServerName ON EYDBSRE.automation.Server (ServerName) GO

-- Table:  EYDBSRE.automation.ServerMapping 
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.ServerMapping ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.ServerMapping; 
GO

CREATE TABLE EYDBSRE.automation.ServerMapping (
	Id int IDENTITY(1,1) NOT NULL,
	ApplicationId int NOT NULL,
	ServerId int NOT NULL,
	Description nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_ServerMapping PRIMARY KEY (Id),
	CONSTRAINT UQ_ServerMapping_AppServerId UNIQUE (ApplicationId,ServerId),
	CONSTRAINT FK_APPLICATION_NAME_SERVERMAPPING FOREIGN KEY (ApplicationId) REFERENCES EYDBSRE.automation.Application(Id),
	CONSTRAINT FK_SERVER_NAME_SERVERMAPPING FOREIGN KEY (ServerId) REFERENCES EYDBSRE.automation.Server(Id)
) GO
CREATE UNIQUE INDEX UX_ServerMapping_AppServerId ON EYDBSRE.automation.ServerMapping (ApplicationId,ServerId) GO


-- Table: EYDBSRE.automation.ServiceRequest 
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.ServiceRequest ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.ServiceRequest; 
GO

CREATE TABLE EYDBSRE.automation.ServiceRequest (
	Id int IDENTITY(1,1) NOT NULL,
	TypeId nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	StatusId nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	AlertId int NULL,
	ServerName nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	InstanceName nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DBName nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SVRefNo nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Info nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RequestedUser nvarchar(64) NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_ServiceRequest PRIMARY KEY (Id),
	CONSTRAINT FK_ServiceRequest_AlertId FOREIGN KEY (AlertId) REFERENCES EYDBSRE.automation.Alert(Id)
) GO
CREATE  INDEX IX_ServiceRequest_TypeId ON EYDBSRE.automation.ServiceRequest (TypeId) GO
CREATE  INDEX IX_ServiceRequest_StatusId ON EYDBSRE.automation.ServiceRequest (StatusId) GO
CREATE  INDEX IX_ServiceRequest_AlertId ON EYDBSRE.automation.ServiceRequest (AlertId) GO
CREATE  INDEX IX_ServiceRequest_RequestedUser ON EYDBSRE.automation.ServiceRequest (RequestedUser) GO


-- Table: EYDBSRE.automation.[User]
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.[User]  ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.[User] ; 
GO

CREATE TABLE EYDBSRE.automation.[User] (
	Id int IDENTITY(1,1) NOT NULL,
	UserName nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Description nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Email nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	StatusId nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_User PRIMARY KEY (Id),
	CONSTRAINT UQ_User_UserName UNIQUE (UserName)
) GO
CREATE UNIQUE INDEX UX_User_UserName ON EYDBSRE.automation.[User] (UserName) GO

-- Table: EYDBSRE.automation.ApplicationMapping 
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.ApplicationMapping ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.ApplicationMapping; 
GO

CREATE TABLE EYDBSRE.automation.ApplicationMapping (
	Id int IDENTITY(1,1) NOT NULL,
	GroupId int NOT NULL,
	ApplicationId int NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_ApplicationMapping9 PRIMARY KEY (Id),
	CONSTRAINT UQ_ApplicationMapping_GroupAppId UNIQUE (GroupId,ApplicationId),
	CONSTRAINT FK_APPLICATION_NAME_APPLICATIONMAPPING FOREIGN KEY (ApplicationId) REFERENCES EYDBSRE.automation.Application(Id),
	CONSTRAINT FK_GROUP_NAME_APPLICATIONMAPPING FOREIGN KEY (GroupId) REFERENCES EYDBSRE.automation.[Group](Id)
) GO
CREATE UNIQUE INDEX UX_ApplicationMapping_GroupAppId ON EYDBSRE.automation.ApplicationMapping (GroupId,ApplicationId) GO

-- Table: EYDBSRE.automation.DbBackupRequest

-- DROP TABLE EYDBSRE.automation.DbBackupRequest GO
IF OBJECT_ID('EYDBSRE.automation.DbBackupRequest ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.DbBackupRequest; 
GO

-- Table:  EYDBSRE.automation.GroupMapping
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.GroupMapping ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.GroupMapping; 
GO

CREATE TABLE EYDBSRE.automation.GroupMapping (
	Id int IDENTITY(1,1) NOT NULL,
	GroupId int NOT NULL,
	UserId int NOT NULL,
	StatusId nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_GroupMapping PRIMARY KEY (Id),
	CONSTRAINT UQ_GroupMapping_GroupUserId UNIQUE (GroupId,UserId),
	CONSTRAINT FK_GROUP_NAME_GROUPMAPPING FOREIGN KEY (GroupId) REFERENCES EYDBSRE.automation.[Group](Id),
	CONSTRAINT FK_USER_NAME_GROUPMAPPING FOREIGN KEY (UserId) REFERENCES EYDBSRE.automation.[User](Id)
) GO
CREATE UNIQUE INDEX UX_GroupMapping_GroupUserId ON EYDBSRE.automation.GroupMapping (GroupId,UserId) GO

-- Table:  EYDBSRE.automation.[Instance] 
-- DROP TABLE
IF OBJECT_ID('EYDBSRE.automation.[Instance]  ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.[Instance] ; 
GO

CREATE TABLE EYDBSRE.automation.[Instance] (
	Id int IDENTITY(1,1) NOT NULL,
	InstanceName nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Description nvarchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ServerId int NOT NULL,
	StatusId nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_Instance PRIMARY KEY (Id),
	CONSTRAINT UQ_Instance_InstanceServerId UNIQUE (InstanceName,ServerId),
	CONSTRAINT FK_SERVER_NAME_INSTANCE FOREIGN KEY (ServerId) REFERENCES EYDBSRE.automation.Server(Id)
) GO
CREATE UNIQUE INDEX UX_Instance_InstanceServerId ON EYDBSRE.automation.[Instance] (InstanceName,ServerId) GO


-- Table: ConfigValues	
IF OBJECT_ID('EYDBSRE.automation.ConfigValues ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.ConfigValues; 
GO
CREATE TABLE EYDBSRE.automation.ConfigValues (
	id 	int IDENTITY(1,1) NOT NULL,
	ConfigType	nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ConfigId	int NOT NULL,
	ConfigName	nvarchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ConfigDescription	nvarchar(512)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ConfigClass	nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	IsShown bit NOT NULL,
	IsChangeable bit NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_ConfigValues PRIMARY KEY (Id),
	CONSTRAINT UQ_ConfigValues_ConfigTypeId UNIQUE (ConfigType,ConfigId)
	) GO
CREATE UNIQUE INDEX UX_ConfigValues_ConfigTypeId ON EYDBSRE.automation.ConfigValues(ConfigType,ConfigId) GO


-- Table: ConfigTemplates	
IF OBJECT_ID('EYDBSRE.automation.ConfigTemplates ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.ConfigTemplates; 
GO
CREATE TABLE EYDBSRE.automation.ConfigTemplates (
	id 	int IDENTITY(1,1) NOT NULL,
	ConfigType	nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	InstanceType	nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ConfigId	int NOT NULL,
	ConfigName	nvarchar(128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ValueType	nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	StdValue	nvarchar(1024) COLLATE SQL_Latin1_General_CP1_CI_AS,
	CreationDate datetime NULL,
	CONSTRAINT PK_ConfigTemplates PRIMARY KEY (Id),
	CONSTRAINT UQ_ConfigTemplates_ConfigInstance UNIQUE (ConfigType,InstanceType,ConfigId)
	) GO
CREATE UNIQUE INDEX UX_ConfigTemplates_ConfigInstance ON EYDBSRE.automation.ConfigTemplates(ConfigType,InstanceType,ConfigId) GO
	
-- Table: ScheduledJobs	
IF OBJECT_ID('EYDBSRE.automation.ScheduledJobs ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.ScheduledJobs; 
GO
CREATE TABLE [EYDBSRE].[automation].[ScheduledJobs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobName] [nvarchar](500) NOT NULL,
	[AlertType] [nvarchar](64) NOT NULL,
	[JobTarget] [nvarchar](1024) NOT NULL,
	[JobScriptFile] [nvarchar](500) NOT NULL,
	[JobInputs] [nvarchar](2048) NOT NULL,
	[ScheduledJobFrequency] [nvarchar] (64) NOT NULL,
	[ScheduledJobTime] [nvarchar](512) NOT NULL,
	[ScheduledJobStatus] [nvarchar](64) NOT NULL,
	[CreatedBy] [nvarchar](64) NOT NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [EYDBSRE].[automation].[ScheduledJobs] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

-- Table: ScheduledJobsRunHistory	
IF OBJECT_ID('EYDBSRE.automation.ScheduledJobsRunHistory ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.ScheduledJobsRunHistory; 
GO
CREATE TABLE [EYDBSRE].[automation].[ScheduledJobsRunHistory](
	Id int IDENTITY(1,1) NOT NULL,
	ScheduledJobid int  NOT NULL,
	ServerName nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	InstanceName nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DBName nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RunStarttime	 datetime NULL,
	RunEndtime	 datetime NULL,
	RunStatus	nvarchar(64) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
	RunOutput	nvarchar(2048) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
	CONSTRAINT PK_ScheduledJobsRunHistory PRIMARY KEY (Id),
	CONSTRAINT FK_ScheduledJobsRunHistory_ScheduledJobid FOREIGN KEY (ScheduledJobid) REFERENCES EYDBSRE.automation.ScheduledJobs(Id)
	) GO
	
	CREATE  INDEX IX_ScheduledJobsRunHistory_ScheduledJobid ON EYDBSRE.automation.ScheduledJobsRunHistory(ScheduledJobid) GO
	
-- Table: DBBackupPlans	
IF OBJECT_ID('EYDBSRE.automation.DBBackupPlans ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.DBBackupPlans; 
GO
CREATE TABLE [EYDBSRE].[automation].[DBBackupPlans](
	Id int IDENTITY(1,1) NOT NULL,
	DBBackupPlanName	nvarchar(64) NOT NULL,
	FullBackupFreq	nvarchar(64)  NOT NULL,
	FullBackupSchedule	nvarchar(512)  NULL,
	DiffBackupFreq	nvarchar(64) NOT NULL,
	DiffBackupSchedule	nvarchar(512)  NULL,
	LogBackupFreq	nvarchar(64) NOT NULL,
	LogBackupSchedule	nvarchar(512)  NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_DBBackupPlans PRIMARY KEY (Id),
	CONSTRAINT UQ_DBBackupPlans_DBBackupPlanName UNIQUE (DBBackupPlanName)
	) GO
CREATE UNIQUE INDEX UQ_DBBackupPlans_DBBackupPlanName ON EYDBSRE.automation.DBBackupPlans(DBBackupPlanName) GO

-- Drop tables
AlertType  
IF OBJECT_ID('EYDBSRE.automation.AlertType ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.AlertType; 
GO
-- GeneralStatus
IF OBJECT_ID('EYDBSRE.automation.GeneralStatus ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.GeneralStatus; 
GO
-- DatabaseMapping 
IF OBJECT_ID('EYDBSRE.automation.DatabaseMapping ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.DatabaseMapping; 
GO
-- ProcessStatus 
IF OBJECT_ID('EYDBSRE.automation.ProcessStatus ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.ProcessStatus; 
GO

-- ServiceRequestType  
IF OBJECT_ID('EYDBSRE.automation.ServiceRequestType ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.ProcessStatus; 
GO
-- Backup_Type 
IF OBJECT_ID('EYDBSRE.automation.Backup_Type ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.Backup_Type; 
GO
-- dbbkps  
IF OBJECT_ID('EYDBSRE.automation.dbbkps ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.dbbkps; 
GO
-- DbBackupRequest
IF OBJECT_ID('EYDBSRE.automation.DbBackupRequest ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.DbBackupRequest; 
GO

-- Table: EYDBSRE.automation.Schedule  (Use ScheduledTasks instead)
-- DROP TABLE 
IF OBJECT_ID('EYDBSRE.automation.Schedule ', 'U') IS NOT NULL 
  DROP TABLE dbo.EYDBSRE.automation.Schedule; 
GO

/* HA Keep this table as is with out any changes*/
-- DROP TABLE EYDBSRE.automation.Event GO
/*
CREATE TABLE EYDBSRE.automation.Event (
	Id int IDENTITY(1,1) NOT NULL,
	ProcessId int NOT NULL,
	ServerId int NOT NULL,
	UserId int NOT NULL,
	DateTimeStarted datetime NOT NULL,
	DateTimeCompleted datetime NOT NULL,
	ProcessParameters nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ServiceNowIncidentId nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ProcessStatusId int NOT NULL,
	CreationDate datetime NULL,
	CONSTRAINT PK_Event PRIMARY KEY (Id),
	CONSTRAINT FK_PROCESS FOREIGN KEY (ProcessId) REFERENCES EYDBSRE.automation.Process(Id),
	CONSTRAINT FK_SERVER_NAME_EVENT FOREIGN KEY (ServerId) REFERENCES EYDBSRE.automation.Server(Id),
	CONSTRAINT FK_USER_NAME_AUTH_EVENT FOREIGN KEY (UserId) REFERENCES EYDBSRE.automation.[User](Id)
) GO */