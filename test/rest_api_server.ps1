$Endpoint = New-UDEndpoint -Url "/create-backup" -Method "POST" -Endpoint {

    param($Body)
    $NewBody = $Body | ConvertFrom-Json 

    $db_srv_instance = $NewBody.db_srv_instance
    $db_name = $NewBody.db_name
    $backup_type = $NewBody.backup_type
    $nas_drive_letter = $NewBody.nas_drive_letter
    $db_splits = $NewBody.db_splits

    $drive_letter = $nas_drive_letter + ':'

    # Trigger DB Backup
    $Script = Invoke-webRequest -Uri 'http://localhost:8090/backup_db.ps1' -UseDefaultCredentials
    $ScriptBlock = [Scriptblock]::Create($Script.Content)
    $result = Invoke-Command -ScriptBlock $ScriptBlock -ArgumentList ($args + @($db_srv_instance, $db_name, $backup_type, $drive_letter, $db_splits))
    
    return $result
}

$Endpoint = New-UDEndpoint -Url "/list-backup" -Method "GET" -Endpoint {
    
    $db_srv_instance = $Request.Headers["db_srv_instance"]
    $backup_location = $Request.Headers["backup_location"]

    $scriptStr = "Get-Childitem $backup_location -Filter *.bak -Recurse"
    $scriptBlock = [scriptblock]::Create($scriptStr)
    $files = Invoke-Command -ComputerName $db_srv_instance -ScriptBlock $scriptBlock
    foreach ($f in $files) { 
        $backup_files_list += "{
            Directory: $($f.directoryname), 
            FileName: $($f.name), 
            Length: $($f.length) KB
        }"    
    }
    
    return $backup_files_list
}

$Endpoint = New-UDEndpoint -Url "/get-drive-letter" -Method "GET" -Endpoint {

     $db_srv_instance = $Request.Headers["db_srv_instance"]

    # --------------------Drive Letter Check---------------------
    try {
        $drive_letter = Invoke-Command -ComputerName $db_srv_instance -ScriptBlock {(68..90 | %{$L=[char]$_; if ((gdr).Name -notContains $L) {$L}})[0]}

        $success = @"
{
    "NAS_Drive_Letter": "$drive_letter"
}
"@

        $output_data = @"
{
    "output": "$true",
    "data": $success
}
"@
    } catch {
        $output_data = @"
{
    "output": $false,
    "Error": $_.Exception.Message
}
"@
    }

    return $output_data | ConvertFrom-Json | ConvertTo-Json
}

$Endpoint = New-UDEndpoint -Url "/create-nas-drive" -Method "POST" -Endpoint {

    param($Body)
    $NewBody = $Body | ConvertFrom-Json 

    $db_srv_instance = $NewBody.db_srv_instance
    $sql_srv_instance = $NewBody.sql_srv_instance
    $drive_letter = $NewBody.drive_letter
    $drive_letter_path = $NewBody.drive_letter_path

    $drive = $drive_letter + ':'

    $Script = Invoke-webRequest -Uri 'http://localhost:8090/create_nas_drive.ps1' -UseDefaultCredentials
    $scriptBlock = [scriptblock]::Create($Script.Content)
    $result_output = Invoke-Command -ComputerName $db_srv_instance -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $drive, $drive_letter_path))
    
    # $data = $result_output | ConvertTo-Json
    return $result_output 
    # if ($data.data.actual_output -Match "Successfully") {
    #     $result_output = "$true"
    # } elseif ($data.data.actual_output -Match "already in use") {
    #     $Script = Invoke-webRequest -Uri 'http://localhost:8090/delete_nas_drive.ps1' -UseDefaultCredentials
    #     $scriptBlock = [scriptblock]::Create($Script.Content)
    #     Invoke-Command -ComputerName $db_srv_instance -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $drive))
        
    #     $Script = Invoke-webRequest -Uri 'http://localhost:8090/create_nas_drive.ps1' -UseDefaultCredentials
    #     $scriptBlock = [scriptblock]::Create($Script.Content)
    #     $result_output = Invoke-Command -ComputerName $db_srv_instance -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $drive, $drive_letter_path))
    
    # }
    # return $result_output
    
}

$Endpoint = New-UDEndpoint -Url "/delete-nas-drive" -Method "POST" -Endpoint {
    
    param($Body)
    $NewBody = $Body | ConvertFrom-Json 

    $db_srv_instance = $NewBody.db_srv_instance
    $sql_srv_instance = $NewBody.sql_srv_instance
    $drive_letter = $NewBody.drive_letter

    $drive = $drive_letter + ':'
    $Script = Invoke-webRequest -Uri 'http://localhost:8090/delete_nas_drive.ps1' -UseDefaultCredentials
    $scriptBlock = [scriptblock]::Create($Script.Content)
    $delete_nas_drive = Invoke-Command -ComputerName $db_srv_instance -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $drive))
    
    return $delete_nas_drive
}

$Endpoint = New-UDEndpoint -Url "/db-size-checker" -Method "GET" -Endpoint {
    
    $db_srv_instance = $Request.Headers["db_srv_instance"]
    $sql_srv_instance = $Request.Headers["sql_srv_instance"]
    $db_name = $Request.Headers["db_name"] 

    $Script = Invoke-webRequest -Uri 'http://localhost:8090/db_size_check.ps1' -UseDefaultCredentials
    $scriptBlock = [scriptblock]::Create($Script.Content)
    try {
        $db_size = Invoke-Command -ComputerName $db_srv_instance -ScriptBlock { Get-SqlDatabase -Name "$using:db_name" -ServerInstance "$using:sql_srv_instance" | Select-Object -Property @{label="Size";expression={[math]::round($_.Size)}} | ConvertTo-Json}
        # $db_size = Invoke-Command -ComputerName $db_srv_instance -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name))
        $dbobj = $db_size | ConvertFrom-Json
        if ( $dbobj.Size -le 500000) {
            return "$true"
        } else {
            return "$false"
        }
    } catch {
        return "Command Execution Failed!"
    }
}

$Endpoint = New-UDEndpoint -Url "/db-backup-history" -Method "GET" -Endpoint {
    
    $db_srv_instance = $Request.Headers["db_srv_instance"]
    $sql_srv_instance = $Request.Headers["sql_srv_instance"]
    $db_name = $Request.Headers["db_name"]

    $Script = Invoke-webRequest -Uri 'http://localhost:8090/db_backup_history.ps1' -UseDefaultCredentials
    $scriptBlock = [scriptblock]::Create($Script.Content)
    $db_backup_history = Invoke-Command -ComputerName $db_srv_instance -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name))
    
    return $db_backup_history
}

$Endpoint = New-UDEndpoint -Url "/insert-events-table" -Method "POST" -Endpoint {
    
    param($Body)
    $NewBody = $Body | ConvertFrom-Json 

    $db_srv_instance = $NewBody.db_srv_instance
    $sql_srv_instance = $NewBody.sql_srv_instance
    $db_name = $NewBody.db_name
    $event_id = $NewBody.event_id
    $process_id = $NewBody.process_id
    $server_name = $NewBody.sql_srv_instance
    $user_name = $NewBody.user_name
    $date_time_started = $NewBody.date_time_started
    $date_time_completed = $NewBody.date_time_completed
    $process_parameters = $NewBody.process_parameters
    $service_now_incident_id = $NewBody.service_now_incident_id
    $process_status_id = $NewBody.process_status_id
    $creation_date = $NewBody.creation_date

    $Script = Invoke-webRequest -Uri 'http://localhost:8090/insert_events_table.ps1' -UseDefaultCredentials
    $scriptBlock = [scriptblock]::Create($Script.Content)
    $insert_cmd_output = Invoke-Command -ComputerName $db_srv_instance -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name, $event_id, $process_id, $server_name, $user_name, $date_time_started, $date_time_completed, $process_parameters, $service_now_incident_id, $process_status_id, $creation_date))
    
    return $insert_cmd_output
}

$Endpoint = New-UDEndpoint -Url "/test-last-backup" -Method "GET" -Endpoint {
    
    # $db_srv_instance = $Request.Headers["db_srv_instance"]
    $physical_device_name = $Request.Headers["physical_device_name"]
    $backupset_name = $Request.Headers["backupset_name"]

    $Script = Invoke-webRequest -Uri 'http://localhost:8090/test_guid_veeam.ps1' -UseDefaultCredentials
    $scriptBlock = [scriptblock]::Create($Script.Content)
    $test_last_backup_result = Invoke-Command -ScriptBlock $scriptBlock -ArgumentList ($args + @($physical_device_name, $backupset_name))
    
    return $test_last_backup_result
}

$Endpoint = New-UDEndpoint -Url "/backup-file-splits" -Method "GET" -Endpoint {
    
    $db_srv_instance = $Request.Headers["db_srv_instance"]
    $sql_srv_instance = $Request.Headers["sql_srv_instance"]
    $db_name = $Request.Headers["db_name"]

    $Script = Invoke-webRequest -Uri 'http://localhost:8090/last_backup_file_size.ps1' -UseDefaultCredentials
    $scriptBlock = [scriptblock]::Create($Script.Content)
    $last_backup_file_size_result = Invoke-Command -ComputerName $db_srv_instance -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name))
    
    $output = $last_backup_file_size_result | ConvertFrom-Json
    if ( $output.data.actual_output.BackupSize_GB -le 10 ) {
        $final_splits_count = 1
    } else {
        $splits_count = $output.data.actual_output.BackupSize_GB / 10
        if ( $splits_count -gt 10 ) {
            $final_splits_count =  10
        } else {
            $final_splits_count = $splits_count
        }
    }
    return $final_splits_count | ConvertTo-Json
}

$Endpoint = New-UDEndpoint -Url "/get-db-backup-progress" -Method "GET" -Endpoint {
    
    $db_srv_instance = $Request.Headers["db_srv_instance"]

    # Trigger DB Backup Progress
    $Script = Invoke-webRequest -Uri 'http://localhost:8090/db_backup_progress.ps1' -UseDefaultCredentials
    $ScriptBlock = [Scriptblock]::Create($Script.Content)
    $backup_progress = Invoke-Command -ScriptBlock $ScriptBlock -ArgumentList ($args + @($db_srv_instance))
    $backup_progress

}

Start-UDRestApi -Name Powershell-RestCalls -Endpoint $Endpoint -Port 6001 -Force

# Get-UDRestApi -Name Powershell-RestCalls | Stop-UDRestApi
