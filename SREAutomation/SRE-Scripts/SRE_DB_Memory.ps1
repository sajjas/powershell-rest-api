Function DBMemory
{
	Param (
		$CASE,
		$SQLINSTANCE,
		$VAR_SERVERINSTANCE, 
		$VAR_DATABASE, 
		$VAR_USERNAME, 
		$VAR_PASSWORD
	)

	if ($CASE -eq "R1") {
		#Get data below query result in r1 from remote Server
		$getInstanceDetailsQuery = "SELECT A.configuration_id, A.name, A.value, A.maximum, A.value_in_use, A.description
		FROM sys.configurations A
		ORDER BY A.configuration_id
		"

		$DB_MEMORY = invoke-sqlcmd -ServerInstance "$SQLINSTANCE" -query "$getInstanceDetailsQuery"
	} elseif ($CASE -eq "R2") {
		#Get data below query result in r2 from SREDB
		$getStandardValuesFromToolDBQuery = "SELECT B.ConfigId, B.ConfigClass, B.IsShown, B.IsChangeable
		FROM [automation].ConfigValues B 
		ORDER BY B.configid
		"
		
		$DB_MEMORY = invoke-sqlcmd -serverInstance "$VAR_SERVERINSTANCE" -query "$getStandardValuesFromToolDBQuery" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
	}
	
	return $DB_MEMORY
}