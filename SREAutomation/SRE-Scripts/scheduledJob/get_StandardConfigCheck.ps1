param(
	[String]$INSTANCENAME
)
# -----------------------------------------------------------------------------
#          Global Definition
# -----------------------------------------------------------------------------
$SMTP = "smtp.ey.net"
$FROM = "standard.config.check@ey.com"

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "USSECVMPMGMSQ02.EY.NET\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

# -----------------------------------------------------------------------------
function sendDBBackupNotification 
{
    Param (
		[array]$TO,
		[string]$TABLEdata,
		[string]$Message,
		[string]$Error
	)
    
	
	if ($Message -eq 'FAILED') {
		$Body = "Scheduled Job DB Backup $Message with following Details. 
				
				$Error"
	} else {
		$Body = "Scheduled Job DB Backup $Message with following Details. 
				
				$TABLEdata"
	}
	$messageParameters = @{
		Subject = "Scheduled Job DBBackup $Message"
		Body = "$Body"
		From = "$FROM"
		To = $TO.split(',')
		SmtpServer = "$SMTP"
	}
    log("EMAIL TO ADDR:	$TO")
	log("EMAIL FROM ADDR:	$FROM")
	LOG("BODY:	$Body")
	LOG("SMTP SERVER:	$SMTP")
	try {
		Send-MailMessage @messageParameters -BodyAsHtml -Encoding utf8
		log("----> Email sent From: $From To: $To  using smtp server: $SMTP")
		return $true
	} catch {
		log("Failed to send Email:	$_.Exception.Message")
		return $false
	}
}

# -----------------------------------------------------------------------------
function tableData($data) 
{
	$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
	$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
	$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
	$style = $style + "TD{border: 1px solid black; padding: 5px; }"
	$style = $style + "</style>"
	
	$new_data = $data | ConvertFrom-Json
	$tableOutput = $new_data | select-object -property @{N='ServiceNow Ticket';E={$_.SNRefNo}}, @{N='Server Name';E={$_.ServerName}}, @{N='Instance Name';E={$_.InstanceName}}, DBName, @{N='Backup Type';E={$_.BackupType}}, @{N='NasDrive Region';E={$_.NasDriveRegion}}, @{N='UserId';E={$_.UserId}}	| ConvertTo-Html -Head $style
	
	return $tableOutput
}

# -----------------------------------------------------------------------------
function nonStandardDBConfigCheck
{
	#Get data below query result in r1 from remote Server
	$getInstanceDetailsQuery = "SELECT A.configuration_id, A.name, A.value, A.maximum, A.value_in_use, A.description
	FROM sys.configurations A
	ORDER BY A.configuration_id
	"
	
	$r1 = invoke-sqlcmd -serverInstance $INSTANCENAME -query $getInstanceDetailsQuery
	return $r1
	log("Result R1:	$r1")
	#Get data below query result in r2 from SREDB
	$getStandardValuesFromToolDBQuery = "SELECT B.configid, B.ConfigClass, B.IsShown, B.IsChangeable
	FROM [automation].ConfigValues B 
	ORDER BY B.configid
	" 
	$r2 = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query $getStandardValuesFromToolDBQuery -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
	log("Result R2:	$r2")
	#Refer this: https://devblogs.microsoft.com/powershell/joining-multiple-tables-grouping-and-evaluating-totals/
	#https://stackoverflow.com/questions/1848821/in-powershell-whats-the-best-way-to-join-two-tables-into-one
	$FinalContent = $r1 | Join-Object $r2 -On configuration_id -Equals configid | Select-Object configuration_id, name, value, maximum, value_in_use, description, configid, ConfigClass, IsShown, IsChangeable
	return @{StandardConfigData=$FinalContent} | ConvertTo-Json
}

# -----------------------------------------------------------------------------
function log($Message)
{
	#$scriptDir = Get-Location
	$scriptDir = "C:"
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir/standard-config-check-output.log
}

# -----------------------------------------------------------------------------
Function Join-Object {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseLiteralInitializerForHashtable', '', Scope='Function')]
	[CmdletBinding(DefaultParameterSetName='Default')][OutputType([Object[]])]Param (

		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Default')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'On')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Expression')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Property')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Discern')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'OnProperty')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'OnDiscern')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'ExpressionProperty')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'ExpressionDiscern')]
		$LeftObject,

		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Default')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'On')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Expression')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Property')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Discern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'OnProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'OnDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'ExpressionProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'ExpressionDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Self')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfOn')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfExpression')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfOnProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfOnDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfExpressionProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfExpressionDiscern')]
		$RightObject,

		[Parameter(Position = 1, ParameterSetName = 'On', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'OnProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'OnDiscern', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfOn', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfOnProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfOnDiscern', Mandatory = $True)]
		[Alias("Using")][String[]]$On,

		[Parameter(Position = 1, ParameterSetName = 'Expression', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'ExpressionProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'ExpressionDiscern', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfExpression', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfExpressionProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfExpressionDiscern', Mandatory = $True)]
		[Alias("UsingExpression")][ScriptBlock]$OnExpression,

		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[String[]]$Equals,

		[Parameter(Position = 2, ParameterSetName = 'Discern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'OnDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'ExpressionDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'SelfDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'SelfOnDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'SelfExpressionDiscern', Mandatory = $True)]
		[AllowEmptyString()][String[]]$Discern,

		[Parameter(ParameterSetName = 'Property', Mandatory = $True)]
		[Parameter(ParameterSetName = 'OnProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'ExpressionProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'SelfProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'SelfOnProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'SelfExpressionProperty', Mandatory = $True)]
		$Property,

		[Parameter(Position = 3, ParameterSetName = 'Default')]
		[Parameter(Position = 3, ParameterSetName = 'On')]
		[Parameter(Position = 3, ParameterSetName = 'Expression')]
		[Parameter(Position = 3, ParameterSetName = 'Property')]
		[Parameter(Position = 3, ParameterSetName = 'Discern')]
		[Parameter(Position = 3, ParameterSetName = 'OnProperty')]
		[Parameter(Position = 3, ParameterSetName = 'OnDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'ExpressionProperty')]
		[Parameter(Position = 3, ParameterSetName = 'ExpressionDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'Self')]
		[Parameter(Position = 3, ParameterSetName = 'SelfOn')]
		[Parameter(Position = 3, ParameterSetName = 'SelfExpression')]
		[Parameter(Position = 3, ParameterSetName = 'SelfProperty')]
		[Parameter(Position = 3, ParameterSetName = 'SelfDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'SelfOnProperty')]
		[Parameter(Position = 3, ParameterSetName = 'SelfOnDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'SelfExpressionProperty')]
		[Parameter(Position = 3, ParameterSetName = 'SelfExpressionDiscern')]
		[ScriptBlock]$Where = {$True},

		[Parameter(ParameterSetName = 'Default')]
		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'Expression')]
		[Parameter(ParameterSetName = 'Property')]
		[Parameter(ParameterSetName = 'Discern')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'ExpressionProperty')]
		[Parameter(ParameterSetName = 'ExpressionDiscern')]
		[Parameter(ParameterSetName = 'Self')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfExpression')]
		[Parameter(ParameterSetName = 'SelfProperty')]
		[Parameter(ParameterSetName = 'SelfDiscern')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[Parameter(ParameterSetName = 'SelfExpressionProperty')]
		[Parameter(ParameterSetName = 'SelfExpressionDiscern')]
		[ValidateSet('Inner', 'Left', 'Right', 'Full', 'Cross')]$JoinType = 'Inner',

		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[Switch]$Strict,

		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[Alias("CaseSensitive")][Switch]$MatchCase
	)
	Begin {
		$HashTable = $Null; $Esc = [Char]27; $EscSeparator = $Esc + ','
		$Expression = [Ordered]@{}; $PropertyList = [Ordered]@{}; $Related = @()
		If ($RightObject -isnot [Array] -and $RightObject -isnot [Data.DataTable]) {$RightObject = @($RightObject)}
		$RightKeys = @(
			If ($RightObject -is [Data.DataTable]) {$RightObject.Columns | Select-Object -ExpandProperty 'ColumnName'}
			Else {
				$First = $RightObject | Select-Object -First 1
				If ($First -is [System.Collections.IDictionary]) {$First.Get_Keys()}
				Else {$First.PSObject.Properties | Select-Object -ExpandProperty 'Name'}
			}
		)
		$RightProperties = @{}; ForEach ($Key in $RightKeys) {$RightProperties.$Key = $Null}
		$RightVoid = New-Object PSCustomObject -Property $RightProperties
		$RightLength = @($RightObject).Length; $LeftIndex = 0; $InnerRight = @($False) * $RightLength
		Function OutObject($LeftIndex, $RightIndex, $Left = $LeftVoid, $Right = $RightVoid) {
			If (&$Where) {
				ForEach ($_ in $Expression.Get_Keys()) {$PropertyList.$_ = &$Expression.$_}
				New-Object PSCustomObject -Property $PropertyList
			}
		}
		Function SetExpression([String]$Key, [ScriptBlock]$ScriptBlock) {
			If ($Key -eq '*') {$Key = $Null}
			If ($Key -and $ScriptBlock) {$Expression.$Key = $ScriptBlock}
			Else {
				$Keys = If ($Key) {@($Key)} Else {$LeftKeys + $RightKeys}
				ForEach ($Key in $Keys) {
					If (!$Expression.Contains($Key)) {
						$InLeft  = $LeftKeys  -Contains $Key
						$InRight = $RightKeys -Contains $Key
						If ($InLeft -and $InRight) {
							$Expression.$Key = If ($ScriptBlock) {$ScriptBlock}
								ElseIf ($Related -NotContains $Key) {{$Left.$_, $Right.$_}}
								Else {{If ($Null -ne $LeftIndex) {$Left.$_} Else {$Right.$_}}}
						}
						ElseIf ($InLeft)  {$Expression.$Key = {$Left.$_}}
						ElseIf ($InRight) {$Expression.$Key = {$Right.$_}}
						Else {Throw [ArgumentException]"The property '$Key' cannot be found on the left or right object."}
					}
				}
			}
		}
	}
	Process {
		Try {
			$SelfJoin = !$PSBoundParameters.ContainsKey('LeftObject'); If ($SelfJoin) {$LeftObject = $RightObject}
			ForEach ($Left in @($LeftObject)) {
				$InnerLeft = $Null
				If (!$LeftIndex) {
					$LeftKeys = @(
						If ($Left -is [Data.DataRow]) {$Left.Table.Columns | Select-Object -ExpandProperty 'ColumnName'}
						ElseIf ($Left -is [System.Collections.IDictionary]) {$Left.Get_Keys()}
						Else {$Left.PSObject.Properties | Select-Object -ExpandProperty 'Name'}
					)
					$LeftProperties = @{}; ForEach ($Key in $LeftKeys) {$LeftProperties.$Key = $Null}
					$LeftVoid = New-Object PSCustomObject -Property $LeftProperties
					If ($Null -ne $On -or $Null -ne $Equals) {
						$On = If ($On) {,@($On)} Else {,@()}; $Equals = If ($Equals) {,@($Equals)} Else {,@()}
						For ($i = 0; $i -lt [Math]::Max($On.Length, $Equals.Length); $i++) {
							If ($i -ge $On.Length) {$On += $Equals[$i]}
							If ($LeftKeys -NotContains $On[$i]) {Throw [ArgumentException]"The property '$($On[$i])' cannot be found on the left object."}
							If ($i -ge $Equals.Length) {$Equals += $On[$i]}
							If ($RightKeys -NotContains $Equals[$i]) {Throw [ArgumentException]"The property '$($Equals[$i])' cannot be found on the right object."}
							If ($On[$i] -eq $Equals[$i]) {$Related += $On[$i]}
						}
						$HashTable = If ($MatchCase) {[HashTable]::New(0, [StringComparer]::Ordinal)} Else {@{}}
						$RightIndex = 0; ForEach ($Right in $RightObject) {
							$Keys = ForEach ($Name in @($Equals)) {$Right.$Name}
							$HashKey = If (!$Strict) {[String]::Join($EscSeparator, @($Keys))}
									   Else {[System.Management.Automation.PSSerializer]::Serialize($Keys)}
							[Array]$HashTable[$HashKey] += $RightIndex++
						}
					}
					If ($Discern) {
						If (@($Discern).Count -le 1) {$Discern = @($Discern) + ''}
						ForEach ($Key in $LeftKeys) {
							If ($RightKeys -Contains $Key) {
								If ($Related -Contains $Key) {
									$Expression[$Key] = {If ($Null -ne $LeftIndex) {$Left.$_} Else {$Right.$_}}
								} Else {
									$Name = If ($Discern[0].Contains('*')) {([Regex]"\*").Replace($Discern[0], $Key, 1)} Else {$Discern[0] + $Key}
									$Expression[$Name] = [ScriptBlock]::Create("`$Left.'$Key'")
								}
							} Else {$Expression[$Key] = {$Left.$_}}
						}
						ForEach ($Key in $RightKeys) {
							If ($LeftKeys -Contains $Key) {
								If ($Related -NotContains $Key) {
									$Name = If ($Discern[1].Contains('*')) {([Regex]"\*").Replace($Discern[1], $Key, 1)} Else {$Discern[1] + $Key}
									$Expression[$Name] = [ScriptBlock]::Create("`$Right.'$Key'")
								}
							} Else {$Expression[$Key] = {$Right.$_}}
						}
					} ElseIf ($Property) {
						ForEach ($Item in @($Property)) {
							If ($Item -is [ScriptBlock]) {SetExpression $Null $Item}
							ElseIf ($Item -is [System.Collections.IDictionary]) {ForEach ($Key in $Item.Get_Keys()) {SetExpression $Key $Item.$Key}}
							Else {SetExpression $Item}
						}
					} Else {SetExpression}
				}
				$RightList = `
					If ($On) {
						If ($JoinType -eq "Cross") {Throw [ArgumentException]"The On parameter cannot be used on a cross join."}
						$Keys = ForEach ($Name in @($On)) {$Left.$Name}
						$HashKey = If (!$Strict) {[String]::Join($EscSeparator, @($Keys))}
								   Else {[System.Management.Automation.PSSerializer]::Serialize($Keys)}
						$HashTable[$HashKey]
					} ElseIf ($OnExpression) {
						If ($JoinType -eq "Cross") {Throw [ArgumentException]"The OnExpression parameter cannot be used on a cross join."}
						For ($RightIndex = 0; $RightIndex -lt $RightLength; $RightIndex++) {
							$Right = $RightObject[$RightIndex]; If (&$OnExpression) {$RightIndex}
						}
					}
					ElseIf ($JoinType -eq "Cross") {0..($RightObject.Length - 1)}
					ElseIf ($LeftIndex -lt $RightLength) {$LeftIndex} Else {$Null}
				ForEach ($RightIndex in $RightList) {
					$Right = If ($RightObject -is [Data.DataTable]) {$RightObject.Rows[$RightIndex]} Else {$RightObject[$RightIndex]}
						$OutObject = OutObject -LeftIndex $LeftIndex -RightIndex $RightIndex -Left $Left -Right $Right
						If ($Null -ne $OutObject) {$OutObject; $InnerLeft = $True; $InnerRight[$RightIndex] = $True}
				}
				If (!$InnerLeft -and ($JoinType -eq "Left" -or $JoinType -eq "Full")) {OutObject -LeftIndex $LeftIndex -Left $Left}
				$LeftIndex++
			}
		} Catch [ArgumentException] {
			$PSCmdlet.ThrowTerminatingError($_)
		}
	}
	End {
		If ($JoinType -eq "Right" -or $JoinType -eq "Full") {$Left = $Null
			$RightIndex = 0; ForEach ($Right in $RightObject) {
				If (!$InnerRight[$RightIndex]) {OutObject -RightIndex $RightIndex -Right $Right}
				$RightIndex++
			}
		}
	}
}; Set-Alias Join Join-Object

# Main Function
log("+++++++++++++ START STANDARD CONFIG CHECK ++++++++++++++")
nonStandardDBConfigCheck
log("+++++++++++++ END STANDARD CONFIG CHECK ++++++++++++++")
