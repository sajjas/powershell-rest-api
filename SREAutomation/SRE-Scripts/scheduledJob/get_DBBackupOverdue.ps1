param(
	[String]$JOB_INPUT,
    [String]$SERVERNAME,
	[String]$INSTANCENAME
)

$json_data = $JOB_INPUT | ConvertFrom-Json 
$SCHEDULED_USER = $json_data.email
$scheduled_job_id = $json_data.scheduled_job_id
$days_without_backup = $json_data.overdue_backup_days
$backup_type = $json_data.backup_type
$db_srv_instance = $SERVERNAME
$sql_srv_instance = $INSTANCENAME
log -Message "INPUTS:	$db_srv_instance  $sql_srv_instance  $backup_type  $days_without_backup  $scheduled_job_id"

$EMAIL_DATA_COLLECTION = New-Object System.Collections.ArrayList
$TABLE_DATA_COLLECTION = New-Object System.Collections.ArrayList

# -----------------------------------------------------------------------------
#          Global Lists Definition
# -----------------------------------------------------------------------------
$SMTP = "smtp.ey.net"
$FROM = "overdue.backup.alert@ey.com"

$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

$IIS_HOST = "USSECVMPMGMSQ01.EY.NET:8000"

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

function sendDBBackupNotification {
    Param (
		[array]$TO,
		[string]$TABLEdata
	)
    
	$messageParameters = @{
		Subject = "Alert For Overdue Backup"
		Body = "Below Database is overdue for taking Backup. 
				
				$TABLEdata"
		From = "$FROM"
		To = $TO.split(',')
		SmtpServer = "$SMTP"
	}
    log("EMAIL TO ADDR:	$TO")
	log("EMAIL FROM ADDR:	$FROM")
	LOG("BODY:	$Body")
	LOG("SMTP SERVER:	$SMTP")
	try {
		Send-MailMessage @messageParameters -BodyAsHtml -Encoding utf8
		log("----> Email sent From: $From To: $To  using smtp server: $SMTP")
		return $true
	} catch {
		log("Failed to send Email:	$_.Exception.Message")
		return $false
	}
}

function tableData($data) {
	$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
	$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
	$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
	$style = $style + "TD{border: 1px solid black; padding: 5px; }"
	$style = $style + "</style>"
	
	$new_data = $data | ConvertFrom-Json
             
	$htmlTableOutput = $new_data | select-object -property @{N='Server Name';E={$_.ServerName}}, @{N='Instance Name';E={$_.InstanceName}}, @{N='DBName';E={$_.DBName}}, @{N='Backup Type';E={$_.BackupType}}, @{Label="Action";Expression={"<a href='$($_.Action)'>Manage Alert</a>"}} | ConvertTo-Html -Head $style
    
    Add-Type -AssemblyName System.Web
    $tableOutput = [System.Web.HttpUtility]::HtmlDecode($htmlTableOutput)
	
	return $tableOutput
}

function getOverdueBackupDBS {
	# ----------- Overdue backup database return list, insert into Alert table and insert into ScheduledJobRunHistory table. -----------
	if ($backup_type -eq "FULL"){
		$backup_type_id = "D"
	} elseif ($backup_type -eq "DIFF"){
		$backup_type_id = "I"
	} elseif ($backup_type -eq "T-LOG"){
		$backup_type_id = "L"
	}

	# -----------------------------------------------------------------------------
	log -Message "Db Server:	$db_srv_instance "
	log -Message "Sql Server Instance:	$sql_srv_instance "
	log -Message "Scheduled Job Id:	$scheduled_job_id "
	log -Message "Overdue Backup Days:	$days_without_backup "
	log -Message "Backup Type:	$backup_type "
    log -Message "Schduled User: $SCHEDULED_USER "
	
	# -----------------------------------------------------------------------------
	# Fetching List of Overdue Backup Databases List
	$overdue_db_backup_queryString = "
		SELECT d.name AS database_name,
		CONVERT(VARCHAR(16), MAX(b.backup_finish_date), 120) AS LastBackup
		FROM sys.databases d
		LEFT OUTER JOIN msdb.dbo.backupset b ON d.name = b.database_name
		WHERE d.name NOT IN ('tempdb', 'model', 'msdb','master','Statistics_DB') and b.[type] = '$backup_type_id'
		GROUP BY d.database_id, d.name
		HAVING MAX(b.backup_finish_date) < GETDATE() - $days_without_backup
		ORDER BY CASE WHEN d.database_id <= 4 THEN 0 ELSE 1 END, d.name
		"
	$run_start_time = Get-Date -format "yyyy-MM-dd HH:mm:ss"
	try {
		$databases = (invoke-sqlcmd -serverInstance $sql_srv_instance -query $overdue_db_backup_queryString).database_name
		$run_status = "COMPLETED"
		$get_overdue_backup_dbs_query_output_data = @{output = $true; Success_Message = $databases | Out-String} | ConvertTo-Json
		$run_output = @{OverdueBackupDatabasesList = $databases | Out-String} | ConvertTo-Json
	} catch {
		$run_status = "FAILED"
		$get_overdue_backup_dbs_query_output_data = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
		$run_output = @{Error = $get_overdue_backup_dbs_query_output_data} | ConvertTo-Json
	}
	
	log -Message "Overdue Backup Databases List:	$run_output "
	$run_end_time = Get-Date -format "yyyy-MM-dd HH:mm:ss"
	$json_backup_type = @{BackupType = $backup_type} | ConvertTo-Json

	# -----------------------------------------------------------------------------
    # Insert into Alerts Table and send Email to user
	if ([String]::IsNullOrEmpty($databases) -ne $true) {
		foreach ($database in $databases) {
			$queryString = "
			INSERT INTO [automation].[Alert]
				   ([TypeId]
				   ,[CreationDate]
				   ,[StatusId]
				   ,[Info]
				   ,[Last_Modified_Date]
				   ,[ServerName]
				   ,[InstanceName]
				   ,[DBName])
				VALUES
				   ('OVERDUE_BACKUP'
				   ,getdate()
				   ,'CREATED'
				   ,'$json_backup_type'
				   ,getdate()
				   ,'$db_srv_instance'
				   ,'$sql_srv_instance'
				   ,'$database');
		        SELECT SCOPE_IDENTITY();
			"
			try {
                $insert_alert_table_query_output = (invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD").Column1
				$insert_alert_table_query_output_data = @{output = $true; Success_Message = "$database database entry inserted into Alert Table"} | ConvertTo-Json
                
                # ---------------------------------------------------------------------------------
	            $data = @{ServerName= $db_srv_instance; InstanceName= $sql_srv_instance; DBName= $database | out-string; BackupType= $backup_type; Action="http://$IIS_HOST/EmailAlert/GetAlerts?alertId=$insert_alert_table_query_output&alertType=OVERDUE_BACKUP"} | ConvertTo-Json
                $EMAIL_DATA_COLLECTION.add($data) | Out-Null
			} catch {
				$insert_alert_table_query_output_data = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
			}
			log -Message "Insert into Alert Table Query Output:		$insert_alert_table_query_output_data"
		}
		# ---------------------------------------------------------------------------------
		# Send Email For this Alert
	    # Format Table data
		sendEmailAlert($EMAIL_DATA_COLLECTION)
	} 
	# -----------------------------------------------------------------------------
	$queryString = "
		INSERT INTO [automation].[ScheduledJobsRunHistory]
			   ([ScheduledJobid]
			   ,[ServerName]
			   ,[InstanceName]
			   ,[DBName]
			   ,[RunStarttime]
			   ,[RunEndtime]
			   ,[RunStatus]
			   ,[RunOutput])
			VALUES
			   ('$scheduled_job_id'
			   ,'$db_srv_instance'
			   ,'$sql_srv_instance'
			   ,'NULL'
			   ,'$run_start_time'
			   ,'$run_end_time'
			   ,'$run_status'
			   ,'$run_output');
		"
	try {
        $insert_scheduled_jobs_run_history_query_output = invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
        $insert_scheduled_jobs_run_history_query_output_data = @{output = $true; Success_Message = "scheduled job id $scheduled_job_id data inserted into ScheduledJobsRunHistory Table" } | ConvertTo-Json
	} catch {
		$insert_scheduled_jobs_run_history_query_output_data = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
	}
	log -Message "Insert into Scheduled Job Run History Table Output:	$insert_scheduled_jobs_run_history_query_output_data"
}

Function sendEmailAlert($data)
{
	$TABLE_DATA_COLLECTION = tableData($data)
	# -------------------------------------------------------------------------------------
	# Sending Email Notification to Scheduled User before DB Backup Start.
	$Email_Sent_Result = sendDBBackupNotification -TO $SCHEDULED_USER -TABLEdata $TABLE_DATA_COLLECTION
}

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

function log {
	[CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, Position = 1)]
        [string]$Message
    )
    #$scriptDir = Get-Location
	$scriptDir = "C:"
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir/overdue_backup_dbs.log
}

# Calling Main Function
log("+++++++++++++ START GETTING OVERDUE BACKUP DATABASES ++++++++++++++")
getOverdueBackupDBS
log("+++++++++++++ END GETTING OVERDUE BACKUP DATABASES ++++++++++++++")
