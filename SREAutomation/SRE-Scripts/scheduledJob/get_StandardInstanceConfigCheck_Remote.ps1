param(
	[String]$JOB_INPUT,
	[String]$SERVERNAME,
	[String]$INSTANCENAME
)

# -----------------------------------------------------------------------------
#          Global Definition
# -----------------------------------------------------------------------------
$SMTP = "smtp.ey.net"
$FROM = "standard.config.check@ey.com"

$JSON_DATA = $JOB_INPUT | ConvertFrom-Json 
$SCHEDULED_JOB_ID = $JSON_DATA.scheduled_job_id
$SCHEDULED_USER = $JSON_DATA.email

$NON_STANDARD_CONFIG_LIST = New-Object System.Collections.ArrayList
$TABLE_DATA_COLLECTION = New-Object System.Collections.ArrayList

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

# -----------------------------------------------------------------------------
function sendDBMemoryNotification 
{
    Param (
		[array]$TO,
		[string]$TABLEdata
	)
    
	$messageParameters = @{
		Subject = "Alert For Non Standard SQL Instance Config"
		Body = "Below Configurations are not as per Standard in this SQL Instance. 
				
				$TABLEdata"
		From = "$FROM"
		To = $TO.split(',')
		SmtpServer = "$SMTP"
	}
    log("EMAIL TO ADDR:	$TO")
	log("EMAIL FROM ADDR:	$FROM")
	LOG("BODY:	$Body")
	LOG("SMTP SERVER:	$SMTP")
	try {
		Send-MailMessage @messageParameters -BodyAsHtml -Encoding utf8
		log("----> Email sent From: $From To: $To  using smtp server: $SMTP")
		return $true
	} catch {
		log("Failed to send Email:	$_.Exception.Message")
		return $false
	}
}

# -----------------------------------------------------------------------------
function tableData($data) 
{
	$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
	$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
	$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
	$style = $style + "TD{border: 1px solid black; padding: 5px; }"
	$style = $style + "</style>"
	
    $new_data = $data | ConvertFrom-Json
	$htmlTableOutput = $new_data | select-object -property @{N='Server Name';E={$SERVERNAME}}, @{N='Instance Name';E={$INSTANCENAME}}, @{N='Configuration Id';E={$_.configuration_id}}, @{N='Name';E={$_.name}}, @{N='Maximum';E={$_.maximum}}, @{N='Value In Use';E={$_.value_in_use}}, @{N='Description';E={$_.description}}, @{N='Value Type';E={$_.ValueType}}, @{N='Std Value';E={$_.StdValue}} | ConvertTo-Html -Head $style
    
    Add-Type -AssemblyName System.Web
    $tableOutput = [System.Web.HttpUtility]::HtmlDecode($htmlTableOutput)
	
    return $tableOutput
}

# -----------------------------------------------------------------------------
function nonStandardInstanceConfigCheck
{
    param (
        $LIST_OF_RESULTS_TO_COMPARE, 
        $MEMORY_IN_BYTES    
    )
    
	foreach ($OBJ in $LIST_OF_RESULTS_TO_COMPARE) {
		# Simple
		if($OBJ.ValueType -eq "Simple") {
			if($OBJ.value_in_use -ne $OBJ.StdValue) {
				$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
			}
		# Range
		} elseif($OBJ.ValueType -eq "Range") {
			if ($OBJ.StdValue -Match "-") {
				$range = $OBJ.StdValue.split("-")
				$min = $range[0]
				$max = $range[1]
				if($min -gt $OBJ.value_in_use -And $OBJ.value_in_use -gt $max) {
					$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
				}
			} else {
				if($OBJ.value_in_use -lt $OBJ.StdValue) {
					$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
				}
			}
		# Complex
		} elseif($OBJ.ValueType -eq "Complex" -And $OBJ.ConfigId -eq 1544) {
			$80_PERC_VALUE = $MEMORY_IN_BYTES * (80/100)
			if($OBJ.value_in_use -gt $80_PERC_VALUE) {
				$NON_STANDARD_CONFIG_LIST.add($OBJ) | Out-Null
			}
		}
	}
	
	return $NON_STANDARD_CONFIG_LIST
}

# -----------------------------------------------------------------------------
function instanceConfig
{
	log("SERVERNAME:	$SERVERNAME")
	LOG("INSTANCENAME:	$INSTANCENAME")
	# ----------- Create Remote Session to DB Server/Jump Server -----------
	$remote_session = Create-PSSession -ServerName $SERVERNAME
	
	if ($remote_session.Count -eq 1) {
		$r1_result = Invoke-Command -Session $remote_session -ScriptBlock ${function:processSqlQuery} -ArgumentList ($args + @($INSTANCENAME))
		$memory_in_bytes = Invoke-Command -Session $remote_session -ScriptBlock {(Get-WmiObject -class "cim_physicalmemory" | Measure-Object -Property Capacity -Sum).Sum}
	} elseif ($remote_session.Count -eq 2) {
		$Jumpy_session = $remote_session[0]
		$r1_result = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:processSqlQuery});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @($using:INSTANCENAME))}
		$memory_in_bytes = Invoke-Command -Session $Jumpy_session -ScriptBlock {Invoke-Command -Session $(Get-PSSession) -ScriptBlock {(Get-WmiObject -class "cim_physicalmemory" | Measure-Object -Property Capacity -Sum).Sum}}
	}
	
	$r1_display = $r1_result | Format-Table | Out-String 
    log("Result R1:	$r1_display")
	
	# ----------- Remove Remote Session/s -----------
	if ($remote_session.Count -eq 1) {
		Remove-PSSession $remote_session
	} elseif ($remote_session.Count -eq 2) {
		Invoke-Command -Session $Jumpy_session -ScriptBlock {Remove-PSSession $(Get-PSSession)}
		Remove-PSSession $Jumpy_session
	}
	
	#Get data below query result in r2 from SREDB
	$getStandardValuesFromToolDBQuery = "SELECT B.ConfigId, B.ValueType, B.StdValue 
	 FROM automation.ConfigTemplates B 
	 Where InstanceType = 'Type1'
	 ORDER BY B.ConfigId
	"
	$r2_result = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query $getStandardValuesFromToolDBQuery -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
	$r2_display = $r2_result | Format-Table | Out-String 
    log("Result R2:	$r2_display")
	
	#Refer this: https://devblogs.microsoft.com/powershell/joining-multiple-tables-grouping-and-evaluating-totals/
	#https://stackoverflow.com/questions/1848821/in-powershell-whats-the-best-way-to-join-two-tables-into-one
	try {
		$FinalContent = $r1_result | Join-Object $r2_result -On configuration_id -Equals ConfigId | Select-Object configuration_id, name, value, maximum, value_in_use, description, ConfigId, ValueType, StdValue
		return @{output=$true; Success_Message=$FinalContent; Memory_In_Bytes=$memory_in_bytes} | ConvertTo-Json
	} catch {
		return @{output=$false; Fail_Message=$_.Exception.Message} | ConvertTo-Json
	}
}

# -----------------------------------------------------------------------------
function processSqlQuery
{
	param(
		$INSTANCENAME
	)
	
	#Get data below query result in r1 from remote Server
	$queryString = "SELECT A.configuration_id, A.name, A.value, A.maximum, A.value_in_use, A.description
	FROM sys.configurations A
	ORDER BY A.configuration_id
	"
	$result = invoke-sqlcmd -serverInstance $INSTANCENAME -query $queryString
	
	return $result
}

# -----------------------------------------------------------------------------
function log($Message)
{
	#$scriptDir = Get-Location
	$scriptDir = "C:"
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir\standard-config-check-output.log
}

# -----------------------------------------------------------------------------
Function Join-Object {
	[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseLiteralInitializerForHashtable', '', Scope='Function')]
	[CmdletBinding(DefaultParameterSetName='Default')][OutputType([Object[]])]Param (

		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Default')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'On')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Expression')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Property')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'Discern')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'OnProperty')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'OnDiscern')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'ExpressionProperty')]
		[Parameter(ValueFromPipeLine = $True, Mandatory = $True, ParameterSetName = 'ExpressionDiscern')]
		$LeftObject,

		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Default')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'On')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Expression')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Property')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Discern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'OnProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'OnDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'ExpressionProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'ExpressionDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'Self')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfOn')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfExpression')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfOnProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfOnDiscern')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfExpressionProperty')]
		[Parameter(Position = 0, Mandatory = $True, ParameterSetName = 'SelfExpressionDiscern')]
		$RightObject,

		[Parameter(Position = 1, ParameterSetName = 'On', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'OnProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'OnDiscern', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfOn', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfOnProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfOnDiscern', Mandatory = $True)]
		[Alias("Using")][String[]]$On,

		[Parameter(Position = 1, ParameterSetName = 'Expression', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'ExpressionProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'ExpressionDiscern', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfExpression', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfExpressionProperty', Mandatory = $True)]
		[Parameter(Position = 1, ParameterSetName = 'SelfExpressionDiscern', Mandatory = $True)]
		[Alias("UsingExpression")][ScriptBlock]$OnExpression,

		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[String[]]$Equals,

		[Parameter(Position = 2, ParameterSetName = 'Discern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'OnDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'ExpressionDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'SelfDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'SelfOnDiscern', Mandatory = $True)]
		[Parameter(Position = 2, ParameterSetName = 'SelfExpressionDiscern', Mandatory = $True)]
		[AllowEmptyString()][String[]]$Discern,

		[Parameter(ParameterSetName = 'Property', Mandatory = $True)]
		[Parameter(ParameterSetName = 'OnProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'ExpressionProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'SelfProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'SelfOnProperty', Mandatory = $True)]
		[Parameter(ParameterSetName = 'SelfExpressionProperty', Mandatory = $True)]
		$Property,

		[Parameter(Position = 3, ParameterSetName = 'Default')]
		[Parameter(Position = 3, ParameterSetName = 'On')]
		[Parameter(Position = 3, ParameterSetName = 'Expression')]
		[Parameter(Position = 3, ParameterSetName = 'Property')]
		[Parameter(Position = 3, ParameterSetName = 'Discern')]
		[Parameter(Position = 3, ParameterSetName = 'OnProperty')]
		[Parameter(Position = 3, ParameterSetName = 'OnDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'ExpressionProperty')]
		[Parameter(Position = 3, ParameterSetName = 'ExpressionDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'Self')]
		[Parameter(Position = 3, ParameterSetName = 'SelfOn')]
		[Parameter(Position = 3, ParameterSetName = 'SelfExpression')]
		[Parameter(Position = 3, ParameterSetName = 'SelfProperty')]
		[Parameter(Position = 3, ParameterSetName = 'SelfDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'SelfOnProperty')]
		[Parameter(Position = 3, ParameterSetName = 'SelfOnDiscern')]
		[Parameter(Position = 3, ParameterSetName = 'SelfExpressionProperty')]
		[Parameter(Position = 3, ParameterSetName = 'SelfExpressionDiscern')]
		[ScriptBlock]$Where = {$True},

		[Parameter(ParameterSetName = 'Default')]
		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'Expression')]
		[Parameter(ParameterSetName = 'Property')]
		[Parameter(ParameterSetName = 'Discern')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'ExpressionProperty')]
		[Parameter(ParameterSetName = 'ExpressionDiscern')]
		[Parameter(ParameterSetName = 'Self')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfExpression')]
		[Parameter(ParameterSetName = 'SelfProperty')]
		[Parameter(ParameterSetName = 'SelfDiscern')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[Parameter(ParameterSetName = 'SelfExpressionProperty')]
		[Parameter(ParameterSetName = 'SelfExpressionDiscern')]
		[ValidateSet('Inner', 'Left', 'Right', 'Full', 'Cross')]$JoinType = 'Inner',

		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[Switch]$Strict,

		[Parameter(ParameterSetName = 'On')]
		[Parameter(ParameterSetName = 'OnProperty')]
		[Parameter(ParameterSetName = 'OnDiscern')]
		[Parameter(ParameterSetName = 'SelfOn')]
		[Parameter(ParameterSetName = 'SelfOnProperty')]
		[Parameter(ParameterSetName = 'SelfOnDiscern')]
		[Alias("CaseSensitive")][Switch]$MatchCase
	)
	Begin {
		$HashTable = $Null; $Esc = [Char]27; $EscSeparator = $Esc + ','
		$Expression = [Ordered]@{}; $PropertyList = [Ordered]@{}; $Related = @()
		If ($RightObject -isnot [Array] -and $RightObject -isnot [Data.DataTable]) {$RightObject = @($RightObject)}
		$RightKeys = @(
			If ($RightObject -is [Data.DataTable]) {$RightObject.Columns | Select-Object -ExpandProperty 'ColumnName'}
			Else {
				$First = $RightObject | Select-Object -First 1
				If ($First -is [System.Collections.IDictionary]) {$First.Get_Keys()}
				Else {$First.PSObject.Properties | Select-Object -ExpandProperty 'Name'}
			}
		)
		$RightProperties = @{}; ForEach ($Key in $RightKeys) {$RightProperties.$Key = $Null}
		$RightVoid = New-Object PSCustomObject -Property $RightProperties
		$RightLength = @($RightObject).Length; $LeftIndex = 0; $InnerRight = @($False) * $RightLength
		Function OutObject($LeftIndex, $RightIndex, $Left = $LeftVoid, $Right = $RightVoid) {
			If (&$Where) {
				ForEach ($_ in $Expression.Get_Keys()) {$PropertyList.$_ = &$Expression.$_}
				New-Object PSCustomObject -Property $PropertyList
			}
		}
		Function SetExpression([String]$Key, [ScriptBlock]$ScriptBlock) {
			If ($Key -eq '*') {$Key = $Null}
			If ($Key -and $ScriptBlock) {$Expression.$Key = $ScriptBlock}
			Else {
				$Keys = If ($Key) {@($Key)} Else {$LeftKeys + $RightKeys}
				ForEach ($Key in $Keys) {
					If (!$Expression.Contains($Key)) {
						$InLeft  = $LeftKeys  -Contains $Key
						$InRight = $RightKeys -Contains $Key
						If ($InLeft -and $InRight) {
							$Expression.$Key = If ($ScriptBlock) {$ScriptBlock}
								ElseIf ($Related -NotContains $Key) {{$Left.$_, $Right.$_}}
								Else {{If ($Null -ne $LeftIndex) {$Left.$_} Else {$Right.$_}}}
						}
						ElseIf ($InLeft)  {$Expression.$Key = {$Left.$_}}
						ElseIf ($InRight) {$Expression.$Key = {$Right.$_}}
						Else {Throw [ArgumentException]"The property '$Key' cannot be found on the left or right object."}
					}
				}
			}
		}
	}
	Process {
		Try {
			$SelfJoin = !$PSBoundParameters.ContainsKey('LeftObject'); If ($SelfJoin) {$LeftObject = $RightObject}
			ForEach ($Left in @($LeftObject)) {
				$InnerLeft = $Null
				If (!$LeftIndex) {
					$LeftKeys = @(
						If ($Left -is [Data.DataRow]) {$Left.Table.Columns | Select-Object -ExpandProperty 'ColumnName'}
						ElseIf ($Left -is [System.Collections.IDictionary]) {$Left.Get_Keys()}
						Else {$Left.PSObject.Properties | Select-Object -ExpandProperty 'Name'}
					)
					$LeftProperties = @{}; ForEach ($Key in $LeftKeys) {$LeftProperties.$Key = $Null}
					$LeftVoid = New-Object PSCustomObject -Property $LeftProperties
					If ($Null -ne $On -or $Null -ne $Equals) {
						$On = If ($On) {,@($On)} Else {,@()}; $Equals = If ($Equals) {,@($Equals)} Else {,@()}
						For ($i = 0; $i -lt [Math]::Max($On.Length, $Equals.Length); $i++) {
							If ($i -ge $On.Length) {$On += $Equals[$i]}
							If ($LeftKeys -NotContains $On[$i]) {Throw [ArgumentException]"The property '$($On[$i])' cannot be found on the left object."}
							If ($i -ge $Equals.Length) {$Equals += $On[$i]}
							If ($RightKeys -NotContains $Equals[$i]) {Throw [ArgumentException]"The property '$($Equals[$i])' cannot be found on the right object."}
							If ($On[$i] -eq $Equals[$i]) {$Related += $On[$i]}
						}
						$HashTable = If ($MatchCase) {[HashTable]::New(0, [StringComparer]::Ordinal)} Else {@{}}
						$RightIndex = 0; ForEach ($Right in $RightObject) {
							$Keys = ForEach ($Name in @($Equals)) {$Right.$Name}
							$HashKey = If (!$Strict) {[String]::Join($EscSeparator, @($Keys))}
									   Else {[System.Management.Automation.PSSerializer]::Serialize($Keys)}
							[Array]$HashTable[$HashKey] += $RightIndex++
						}
					}
					If ($Discern) {
						If (@($Discern).Count -le 1) {$Discern = @($Discern) + ''}
						ForEach ($Key in $LeftKeys) {
							If ($RightKeys -Contains $Key) {
								If ($Related -Contains $Key) {
									$Expression[$Key] = {If ($Null -ne $LeftIndex) {$Left.$_} Else {$Right.$_}}
								} Else {
									$Name = If ($Discern[0].Contains('*')) {([Regex]"\*").Replace($Discern[0], $Key, 1)} Else {$Discern[0] + $Key}
									$Expression[$Name] = [ScriptBlock]::Create("`$Left.'$Key'")
								}
							} Else {$Expression[$Key] = {$Left.$_}}
						}
						ForEach ($Key in $RightKeys) {
							If ($LeftKeys -Contains $Key) {
								If ($Related -NotContains $Key) {
									$Name = If ($Discern[1].Contains('*')) {([Regex]"\*").Replace($Discern[1], $Key, 1)} Else {$Discern[1] + $Key}
									$Expression[$Name] = [ScriptBlock]::Create("`$Right.'$Key'")
								}
							} Else {$Expression[$Key] = {$Right.$_}}
						}
					} ElseIf ($Property) {
						ForEach ($Item in @($Property)) {
							If ($Item -is [ScriptBlock]) {SetExpression $Null $Item}
							ElseIf ($Item -is [System.Collections.IDictionary]) {ForEach ($Key in $Item.Get_Keys()) {SetExpression $Key $Item.$Key}}
							Else {SetExpression $Item}
						}
					} Else {SetExpression}
				}
				$RightList = `
					If ($On) {
						If ($JoinType -eq "Cross") {Throw [ArgumentException]"The On parameter cannot be used on a cross join."}
						$Keys = ForEach ($Name in @($On)) {$Left.$Name}
						$HashKey = If (!$Strict) {[String]::Join($EscSeparator, @($Keys))}
								   Else {[System.Management.Automation.PSSerializer]::Serialize($Keys)}
						$HashTable[$HashKey]
					} ElseIf ($OnExpression) {
						If ($JoinType -eq "Cross") {Throw [ArgumentException]"The OnExpression parameter cannot be used on a cross join."}
						For ($RightIndex = 0; $RightIndex -lt $RightLength; $RightIndex++) {
							$Right = $RightObject[$RightIndex]; If (&$OnExpression) {$RightIndex}
						}
					}
					ElseIf ($JoinType -eq "Cross") {0..($RightObject.Length - 1)}
					ElseIf ($LeftIndex -lt $RightLength) {$LeftIndex} Else {$Null}
				ForEach ($RightIndex in $RightList) {
					$Right = If ($RightObject -is [Data.DataTable]) {$RightObject.Rows[$RightIndex]} Else {$RightObject[$RightIndex]}
						$OutObject = OutObject -LeftIndex $LeftIndex -RightIndex $RightIndex -Left $Left -Right $Right
						If ($Null -ne $OutObject) {$OutObject; $InnerLeft = $True; $InnerRight[$RightIndex] = $True}
				}
				If (!$InnerLeft -and ($JoinType -eq "Left" -or $JoinType -eq "Full")) {OutObject -LeftIndex $LeftIndex -Left $Left}
				$LeftIndex++
			}
		} Catch [ArgumentException] {
			$PSCmdlet.ThrowTerminatingError($_)
		}
	}
	End {
		If ($JoinType -eq "Right" -or $JoinType -eq "Full") {$Left = $Null
			$RightIndex = 0; ForEach ($Right in $RightObject) {
				If (!$InnerRight[$RightIndex]) {OutObject -RightIndex $RightIndex -Right $Right}
				$RightIndex++
			}
		}
	}
}; Set-Alias Join Join-Object

# -----------------------------------------------------------------------------
Function Create-PSSession {

[CmdletBinding()]
Param (
    [Parameter(Mandatory=$True)]
    [string]$ServerName
)

$Session_Jumpy = $null

    If ($ServerName -match "ey.net" -and $ServerName -notmatch "cloudapp"){
        $InventAccount = "EY\P.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
    } Elseif ($ServerName -match "eydmz.net"){
        $Session_Jumpy = JumpServer
		$InventAccount = "EYDMZ.NET\Z.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoASQA2ADEANQBjAGEAdgB4AFcATQBUAGYAZgA3AEkAVQBCAHMAUgBTAHcAPQA9AHwAOQA2ADAAMwA5ADcAZQBlADYANQAzADQAYgA3AGYAYQA5ADYAMAA3ADgAOAA5ADIANQAzADYAOQBmADEAMgBhAGMAMAA1ADkAYQAyADcAZAA0ADcAZAA5ADEAZgA3ADcAYQAyADEAZABjAGEAMgBkADUAMABiAGIAMgBkAGQAYwA='
    } Elseif ($ServerName -match "eyxstaging.net"){
		$Session_Jumpy = JumpServer
		$InventAccount = "EYXSTAGING.NET\X.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoAWgByAHYAMAB4AFkAUQBVAFAAQgBXAEEAUgA5ADAAawBmAEQAbAB5AEEAPQA9AHwAMgA4ADkAZgBmAGEANQA3AGUANwAwADQAZAA3ADUAYQBkADMAZgAwADMANQBmADQAYwA1AGQAZgA0ADIAMwA0ADgAYwA3AGYANQBkAGUAMwA1ADUANgA3ADYAYgBhAGQAOQAxADcAMgBmAGQAOAA0ADkAMABhAGIAYgBkADUAYQA='
	} Elseif ($ServerName -match "eyua.net"){
        $InventAccount = "EYUA\U.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHIAZgBzAGkAOABxADUAOQBmAGIAVwB4AFcAKwBHAEEANQA4ADYANQBpAGcAPQA9AHwAMQBkADUAOAA3ADEAYwA2AGYANwA3AGIANwA2AGUANQAwADUAYQA2AGYAZQBkADAAZgAwAGEAMQA5ADAAMQBkADMAYQAwADcAYwAyAGIAZQAwADUAMgBiADYAMQBjAGYAMwA5ADcAMAA2ADUAMgBjADYAMwA5ADkAYgA0ADUAOQA='
    } Elseif ($ServerName -match "eyqa.net"){
        $InventAccount = "EYQA\Q.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHcAdgA3AFcAcQBIAEYAUQAwAHoAUABNAGkAZAB4AHYANQBoAFcAVABWAHcAPQA9AHwANQAxADYAYwBiADcAOABmADMAYQAzADUAMgBlAGYANgBmAGUAMwA0ADEAMwA0AGMANwBkADcAYgA2ADMAZgAxADEAYQA4AGIAMQAxADcANwA2ADcAYwAwADYAMABkADQANwAyADcAMwA1ADMAYQA1ADYAZABlADgAMABmAGIAZQA='
    } Elseif ($ServerName -match "eydev.net"){
        $InventAccount = "EYDEV\D.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEYANABNAFcAbgBTAHAAUQBMAGYAYgAxAGgASAAyAGUAQgBtAGYAZQBTAFEAPQA9AHwAMwBlAGEAZQA1AGYAMgBlAGUANABkADQAOABlAGYAMwBlAGMAMABhADYAMAAzADkAMgBjADgAYQAxADQAMgA0AGEAMQA4ADQAZQA4AGMAMAAzADgAMgA5AGIANwBmADEAYgA0ADkAZgBmADIAYgA3AGEAYQBhAGYAZABhADIANwA='
    } Elseif ($ServerName -match "ey.net" -and $ServerName -match "cloudapp"){
        $InventAccount = "CLOUDAPP.NET\C.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEEAaQBJADUAMgA1AEMAVABXAC8AbQBSAHgAWgBaAHgAUABoAHIAVwA0AEEAPQA9AHwAZAAwAGYAOQAzAGUAMwAzADIAMwA2AGYAZgA4ADUAOABlADkANQBkADcANwAzAGYAOQBiADMAMAA2AGIANwBkADMAZABiADIAOAAyAGMANgBhADEAYgA5ADQAZAAwAGYAYQA4ADMAMgAyADAANwA1ADQAZQA4ADkAOAAxADAAMAA='
    } Elseif ($ServerName -match "eydev.net" -and $ServerName -match "cloudapp"){
        $InventAccount = "CLOUDAPPDEV\A.SQLINVENT"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AFQAYgBWAGMANwBWAHQAbQBRAHAATgA0AHIAQQBUAEIAbQBiAG4AbgBtAEEAPQA9AHwAMgA5AGYAZgA3ADUAOQAwAGEAZAAwADYAYQA2ADQAZgAxAGMAYgBjADIANQBmAGEAYQAxADcAZgBmAGQAOQBkADYAZAAzAGYAOAAxADEAOAA4ADgAMAAyAGUANABkADAAYgBkADgANQBhAGEANwA1AGYAYgBlADIAMAAwADEAZAA='
    } Else {
        If ($ServerName -match '^[A-Z]{6,7}[Pp]' -and ($($ServerName.Substring(0,2)) -notmatch "^AC" -and $ServerName -notmatch ".cloudapp.ey.net$")){
            $InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
            If ($ServerName -notmatch 'ey.net$'){
                $ServerName += ".ey.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Zz]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
			$Session_Jumpy = JumpServer
            $InventAccount = "EYDMZ.NET\Z.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoASQA2ADEANQBjAGEAdgB4AFcATQBUAGYAZgA3AEkAVQBCAHMAUgBTAHcAPQA9AHwAOQA2ADAAMwA5ADcAZQBlADYANQAzADQAYgA3AGYAYQA5ADYAMAA3ADgAOAA5ADIANQAzADYAOQBmADEAMgBhAGMAMAA1ADkAYQAyADcAZAA0ADcAZAA5ADEAZgA3ADcAYQAyADEAZABjAGEAMgBkADUAMABiAGIAMgBkAGQAYwA='
            If ($ServerName -notmatch 'eydmz.net$'){
                $ServerName += ".eydmz.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Xx]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $Session_Jumpy = JumpServer
			$InventAccount = "EYXSTAGING.NET\X.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoAWgByAHYAMAB4AFkAUQBVAFAAQgBXAEEAUgA5ADAAawBmAEQAbAB5AEEAPQA9AHwAMgA4ADkAZgBmAGEANQA3AGUANwAwADQAZAA3ADUAYQBkADMAZgAwADMANQBmADQAYwA1AGQAZgA0ADIAMwA0ADgAYwA3AGYANQBkAGUAMwA1ADUANgA3ADYAYgBhAGQAOQAxADcAMgBmAGQAOAA0ADkAMABhAGIAYgBkADUAYQA='
            If ($ServerName -notmatch 'eyxstaging.net$'){
                $ServerName += ".eyxstaging.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Uu]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYUA\U.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHIAZgBzAGkAOABxADUAOQBmAGIAVwB4AFcAKwBHAEEANQA4ADYANQBpAGcAPQA9AHwAMQBkADUAOAA3ADEAYwA2AGYANwA3AGIANwA2AGUANQAwADUAYQA2AGYAZQBkADAAZgAwAGEAMQA5ADAAMQBkADMAYQAwADcAYwAyAGIAZQAwADUAMgBiADYAMQBjAGYAMwA5ADcAMAA2ADUAMgBjADYAMwA5ADkAYgA0ADUAOQA='
            If ($ServerName -notmatch 'eyua.net$'){
                $ServerName += ".eyua.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Qq]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYQA\Q.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHcAdgA3AFcAcQBIAEYAUQAwAHoAUABNAGkAZAB4AHYANQBoAFcAVABWAHcAPQA9AHwANQAxADYAYwBiADcAOABmADMAYQAzADUAMgBlAGYANgBmAGUAMwA0ADEAMwA0AGMANwBkADcAYgA2ADMAZgAxADEAYQA4AGIAMQAxADcANwA2ADcAYwAwADYAMABkADQANwAyADcAMwA1ADMAYQA1ADYAZABlADgAMABmAGIAZQA='
            If ($ServerName -notmatch 'eyqa.net$'){
                $ServerName += ".eyqa.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Dd]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYDEV\D.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEYANABNAFcAbgBTAHAAUQBMAGYAYgAxAGgASAAyAGUAQgBtAGYAZQBTAFEAPQA9AHwAMwBlAGEAZQA1AGYAMgBlAGUANABkADQAOABlAGYAMwBlAGMAMABhADYAMAAzADkAMgBjADgAYQAxADQAMgA0AGEAMQA4ADQAZQA4AGMAMAAzADgAMgA5AGIANwBmADEAYgA0ADkAZgBmADIAYgA3AGEAYQBhAGYAZABhADIANwA='
            If ($ServerName -notmatch 'eydev.net$'){
                $ServerName += ".eydev.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Pp]' -and $($ServerName.Substring(0,2)) -match "^AC"){
            $InventAccount = "CLOUDAPP.NET\C.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEEAaQBJADUAMgA1AEMAVABXAC8AbQBSAHgAWgBaAHgAUABoAHIAVwA0AEEAPQA9AHwAZAAwAGYAOQAzAGUAMwAzADIAMwA2AGYAZgA4ADUAOABlADkANQBkADcANwAzAGYAOQBiADMAMAA2AGIANwBkADMAZABiADIAOAAyAGMANgBhADEAYgA5ADQAZAAwAGYAYQA4ADMAMgAyADAANwA1ADQAZQA4ADkAOAAxADAAMAA='
            If ($ServerName -notmatch 'cloudapp.ey.net$'){
                $ServerName += ".cloudapp.ey.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Dd]' -and $($ServerName.Substring(0,2)) -match "^AC"){
            $InventAccount = "CLOUDAPPDEV\A.SQLINVENT"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AFQAYgBWAGMANwBWAHQAbQBRAHAATgA0AHIAQQBUAEIAbQBiAG4AbgBtAEEAPQA9AHwAMgA5AGYAZgA3ADUAOQAwAGEAZAAwADYAYQA2ADQAZgAxAGMAYgBjADIANQBmAGEAYQAxADcAZgBmAGQAOQBkADYAZAAzAGYAOAAxADEAOAA4ADgAMAAyAGUANABkADAAYgBkADgANQBhAGEANwA1AGYAYgBlADIAMAAwADEAZAA='
            If ($ServerName -notmatch 'cloudapp.eydev.net$'){
                $ServerName += ".cloudapp.ey.net"
            }
        } Else {
            $InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
            If ($ServerName -notmatch 'ey.net$'){
                $ServerName += ".ey.net"
            }
        }
    }
    If ($InventAccount -ne $null) {
		# retrieve the password.
		[Byte[]] $key = (1..16)  	
        $SecurePassword = ConvertTo-SecureString $DBAToolPassword -Key $key
        $InventCredential = New-Object System.Management.Automation.PSCredential ($InventAccount, $SecurePassword)
        $RemoteSessionOption = New-PSSessionOption -SkipCACheck -OpenTimeout 180000 -IdleTimeout 180000 #3 minutes
        if ($Session_Jumpy -ne $null) {
			$Session = Invoke-Command -Session $Session_Jumpy -ScriptBlock {New-PSSession -ComputerName $using:ServerName -Credential $using:InventCredential -SessionOption $using:RemoteSessionOption -ErrorAction Stop}
			return $Session_Jumpy, $Session.Name
		} else {
			$Session = New-PSSession -ComputerName $ServerName -Credential $InventCredential -SessionOption $RemoteSessionOption -ErrorAction Stop
			return $Session
		}
    }
}

Function JumpServer() {
	$InventAccount_Jumpy = "EY\P.SMOO.SQL"
	$DBAToolPassword_Jumpy = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
    [Byte[]] $key = (1..16)  	
    $SecurePassword_Jumpy = ConvertTo-SecureString $DBAToolPassword_Jumpy -Key $key
	$InventCredential_Jumpy = New-Object System.Management.Automation.PSCredential ($InventAccount_Jumpy, $SecurePassword_Jumpy)
	$RemoteSessionOption_Jumpy = New-PSSessionOption -SkipCACheck -OpenTimeout 180000 -IdleTimeout 180000 #3 minutes
	$Session_Jumpy = New-PSSession -ComputerName "USSECVMPDBTSQ01.ey.net" -Credential $InventCredential_Jumpy -SessionOption $RemoteSessionOption_Jumpy -ErrorAction Stop
	return $Session_Jumpy
}

# -----------------------------------------------------------------------------
Function sendEmailAlert($data)
{
	$TABLE_DATA_COLLECTION = tableData($data)
	# -------------------------------------------------------------------------------------
	# Sending Email Notification to Scheduled User before DB Backup Start.
	$Email_Sent_Result = sendDBMemoryNotification -TO $SCHEDULED_USER -TABLEdata $TABLE_DATA_COLLECTION
}

# -----------------------------------------------------------------------------
Function scheduleJobHistoryRecord($param=@{})
{
    $INSERT_ID = $param.insert_id
    # -----------------------------------------------------------------------------
	if($param.query_type -eq "INSERT") {
		$queryString = "
		INSERT INTO [automation].[ScheduledJobsRunHistory]
			   ([ScheduledJobid]
			   ,[ServerName]
			   ,[InstanceName]
			   ,[DBName]
			   ,[RunStarttime]
			   ,[RunEndtime]
			   ,[RunStatus]
			   ,[RunOutput])
			VALUES
			   ('$SCHEDULED_JOB_ID'
			   ,'$SERVERNAME'
			   ,'$INSTANCENAME'
			   ,'NULL'
			   ,'$RUN_START_TIME'
			   ,'$RUN_START_TIME'
			   ,'$RUN_STATUS'
			   ,'NULL');
			   SELECT SCOPE_IDENTITY();
		"
	} elseif($param.query_type -eq "UPDATE") {
		$queryString=" 
		UPDATE [automation].[ScheduledJobsRunHistory] SET
			RunEndtime='$RUN_END_TIME', 
			RunStatus='$RUN_STATUS',
            RunOutput='$RUN_OUTPUT'
		WHERE Id='$INSERT_ID'; 
		SELECT SCOPE_IDENTITY();
		"
	}
	
	try {
        $Schedulejob_Run_History_Query_Output = invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
        $Schedulejob_Run_History_Query_Output_Json = @{output = $true; Success_Message = $Schedulejob_Run_History_Query_Output.Column1} | ConvertTo-Json
	} catch {
		$Schedulejob_Run_History_Query_Output_Json = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
	}
	
	return $Schedulejob_Run_History_Query_Output_Json
}

# -----------------------------------------------------------------------------
Function AlertRecord($info)
{
	$queryString = "
	INSERT INTO [automation].[Alert]
		   ([TypeId]
		   ,[CreationDate]
		   ,[StatusId]
		   ,[Info]
		   ,[Last_Modified_Date]
		   ,[ServerName]
		   ,[InstanceName]
		   ,[DBName])
		VALUES
		   ('NON_STANDARD_INSTANCE'
		   ,getdate()
		   ,'CREATED'
		   ,'$INFO'
		   ,getdate()
		   ,'$SERVERNAME'
		   ,'$INSTANCENAME'
		   ,'NULL');
		SELECT SCOPE_IDENTITY();
	"
	try {
		$Alert_Insert_Query_Output = (invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD").Column1
		$Alert_Insert_Query_Output_Json = @{output = $true; Success_Message = $Alert_Insert_Query_Output} | ConvertTo-Json
	} catch {
		$Alert_Insert_Query_Output_Json = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
	}
	
	return $Alert_Insert_Query_Output_Json
}

# -----------------------------------------------------------------------------
Function mainFunction
{
	# ----------- Starting with Creating an Entry in ScheduledJobRunHistory Table -----------
	log("=========== Starting with Creating an Entry in ScheduledJobRunHistory Table ===========")
	
	$RUN_START_TIME = Get-Date -format "yyyy-MM-dd HH:mm:ss"

	$RUN_STATUS = "STARTED"
	
	$Schedule_Job_Run_History_Data = scheduleJobHistoryRecord(@{query_type="INSERT"})

	$Schedule_Job_Run_History_Data_Parsed = $Schedule_Job_Run_History_Data | ConvertFrom-Json

	if($schedule_Job_Run_History_Data_Parsed.output -eq $false) {
		log($Schedule_Job_Run_History_Data_Parsed.Fail_Message)

	} elseif($Schedule_Job_Run_History_Data_Parsed.output -eq $true -And [String]::IsNullOrEmpty($Schedule_Job_Run_History_Data_Parsed.Success_Message) -eq $true) {
		log("ERROR: Schedule Job Eun History InsertID missing")

	} else {
		$Schedule_Job_Run_History_InsertID = $Schedule_Job_Run_History_Data_Parsed.Success_Message
		log("ScheduledJobRunHistory Insert ID:	$Schedule_Job_Run_History_InsertID")
	}

	# ----------- Fetching the Sql Instance Configurations -----------
	log("=========== Fetching the Sql Instance Confiugrations ===========")

	$LIST_OF_RESULTS_TO_COMPARE = instanceConfig

	$JSON_PARSED_DATA = $LIST_OF_RESULTS_TO_COMPARE | ConvertFrom-Json

	if ($JSON_PARSED_DATA.output -eq $true) {
		# ----------- Checking For Non Standard Sql Instance Configurations -----------
		log("=========== Checking For Non Standard Sql Instance Configurations ===========")

		$NON_STD_DB_CONFIG_DATA = nonStandardInstanceConfigCheck $JSON_PARSED_DATA.Success_Message $JSON_PARSED_DATA.Memory_In_Bytes.value | ConvertTo-Json
		
		if($NON_STD_DB_CONFIG_DATA.Count -le 1) {
			$INFO = @"
{
		"NonStandardInstanceConfig": [$NON_STD_DB_CONFIG_DATA]
}
"@
		} else {
			$INFO = @"
{
		"NonStandardInstanceConfig": $NON_STD_DB_CONFIG_DATA
}
"@
		}
		
		if ([String]::IsNullOrEmpty($NON_STD_DB_CONFIG_DATA) -eq $false) {

			# ----------- Inserting Entry with Non Standard Sql Instance Configurations in Alert Table -----------
			log("=========== Inserting Entry with Non Standard Sql Instance Configurations in Alert Table ===========")
			AlertRecord($INFO)
			
			# ----------- Sending an Email to Scheduled user with Non Standard Sql Instance Configurations -----------
			log("=========== Sending an Email with Non Standard Sql Instance Configurations ===========")
			sendEmailAlert($NON_STD_DB_CONFIG_DATA)
		
		} else {
			log("=========== No Non Standard Sql Instance Configurations Found! ===========")
		}
		
		$RUN_OUTPUT = $INFO
		
		$RUN_STATUS = "COMPLETED"

	} elseif ($JSON_PARSED_DATA.output -eq $false) {
		log($JSON_PARSED_DATA.Fail_Message)
		
		$RUN_OUTPUT = $JSON_PARSED_DATA.Fail_Message
		
		$RUN_STATUS = "FAILED"
	}

	# ----------- Finishing with Creating an Entry in ScheduledJobRunHistory Table -----------
	log("=========== Finishing with Creating an Entry in ScheduledJobRunHistory Table ===========")

	$RUN_END_TIME = Get-Date -format "yyyy-MM-dd HH:mm:ss"

	scheduleJobHistoryRecord(@{query_type="UPDATE"; insert_id=$Schedule_Job_Run_History_InsertID})
}

#Main Function
log("+++++++++++++ START STANDARD CONFIG CHECK ++++++++++++++")
mainFunction
log("+++++++++++++ END STANDARD CONFIG CHECK ++++++++++++++")
  