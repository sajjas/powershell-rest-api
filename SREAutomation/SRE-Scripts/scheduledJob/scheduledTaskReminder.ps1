$SMTP = "smtp.ey.net"
$FROM = "scheduled.task.reminder@ey.com"

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

# -----------------------------------------------------------------------------
function sendReminderNotification {
    Param (
		[array]$TO,
		[string]$TABLEdata,
		[Int32]$REMINDERMINS,
		[string]$PROCESSACTION
	)
    
	$messageParameters = @{
		Subject = "$PROCESSACTION Reminder Notification"
		Body = "<html><body>
				Hello Team,
				</br>Following request which you have scheduled is going to be executed in $REMINDERMINS Minutes.<br/></body></html>
				
				<br/>$TABLEdata<br/>
				Note: You will be receiving another notification when the process completed.<br/>
				
				<br/>You may reach out to SQL Operations team (EY_DB_Operations_MSSQL.GID) for any help.<br/>

				<br/><strong>
				Thanks<br/>
				SQL Database Team.
				<strong>
				"
		From = "$FROM"
		To = $TO.split(',')
		SmtpServer = "$SMTP"
    }
    log("EMAIL TO ADDR:	$TO")
	log("EMAIL FROM ADDR:	$FROM")
	LOG("BODY:	$TABLEdata")
	LOG("SMTP SERVER:	$SMTP")
	try {
		Send-MailMessage @messageParameters -BodyAsHtml -Encoding utf8
		log("----> Email sent From: $From To: $To  using smtp server: $SMTP")
		return $true
	} catch {
		log("Failed to send Email:	$_.Exception.Message")
		return $false
	}
}

function createReminderScheduledTask {
	$InventAccount = "EY\P.SMOO.SQL"
	$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
    
	[Byte[]] $key = (1..16)  	
    $SecurePassword = ConvertTo-SecureString $DBAToolPassword -Key $key
	$InventCredential = New-Object System.Management.Automation.PSCredential ($InventAccount, $SecurePassword)
	$Password = $InventCredential.GetNetworkCredential().Password
	
	$action = New-ScheduledTaskAction -Execute 'Powershell.exe' -Argument '"C:\DBSRE\jobs\scheduledTaskReminder.ps1"'
	$trigger = New-ScheduledTaskTrigger `
		-Once `
		-At (Get-Date) `
		-RepetitionInterval (New-TimeSpan -Minutes 15) `
		-RepetitionDuration (New-TimeSpan -Days (365 * 23))
	Register-ScheduledTask -Action $action -Trigger $trigger -TaskPath "\DBSRE" -TaskName "DBSRE-ScheduledTask-Reminder" -Description "DBSRE-ScheduledTask-Reminder" -User "$InventAccount" -Password "$Password" -RunLevel Highest
}

function tableData($data) {
	$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
	$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
	$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
	$style = $style + "TD{border: 1px solid black; padding: 5px; }"
	$style = $style + "</style>"
	
	$new_data = $data | ConvertFrom-Json
	if ($new_data.PSobject.Properties.Name -contains "DBName") {
		$tableOutput = $new_data | select-object -property @{N='ServiceNow Ticket';E={$_.SNRefNo}}, @{N='Server Name';E={$_.ServerName}}, @{N='Instance Name';E={$_.InstanceName}}, DBName, @{N='Scheduled Time(in GMT)';E={$_.ScheduledTime}} | ConvertTo-Html -Head $style
	} else {
		$tableOutput = $new_data | select-object -property @{N='ServiceNow Ticket';E={$_.SNRefNo}}, @{N='Server Name';E={$_.ServerName}}, @{N='Instance Name';E={$_.InstanceName}}, @{N='Scheduled Time(in GMT)';E={$_.ScheduledTime}} | ConvertTo-Html -Head $style
	}
	return $tableOutput
}

# About the query
# For Example,
# If schedule time at 7:50 - (ReminderMins 15 + 15) = 7:20 <= 7:10 (if trigger at 7:10) - No Action
# If schedule time at 7:50 - (ReminderMins 15 + 15) = 7:20 <= 7:25 (if trigger at 7:25) - Trigger Action
function fetchReminderScheduledTaskInfo {
	log("======== START FETCHING TASK LIST FOR EMAIL REMINDER ========")
    try {
		$taskListForEmailReminders = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "select Id, ProcessId, ServerName, InstanceName, DBName, ReminderEmail, ScheduledTime, ReminderMins, ReminderCount, SNRefNo from [automation].[ScheduledTasks] where StatusId = 'SCHEDULED' And ReminderCount = 0 And DATEADD(MINUTE, -(ReminderMins + 15) ,ScheduledTime) <= (CONVERT(datetime, DATEADD(minute, DATEDIFF(minute, GETDATE(), GETUTCDATE()), GETDATE())))" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
	} catch {
		log("ERROR:	Failed to Fetch the List of scheduledTasks to send Reminder with $_.Exception.Message")
	}
	
	if ([String]::IsNullOrEmpty($taskListForEmailReminders) -ne $true) {
		foreach ($taskItem in $taskListForEmailReminders) {
			log("PROCESSING TASK LIST ITEM")
			# ---------------------------------------------------------------------------------
			$PROCESSID = $taskItem.ProcessId
			$TASKID = $taskItem.Id
			$SERVERNAME = $taskItem.ServerName
			$INSTANCENAME = $taskItem.InstanceName
			$SCHEDULETIME = $taskItem.ScheduledTime
			$SNREFNO = $taskItem.SNRefNo
			$REMINDERMINS = $taskItem.ReminderMins
			$INC_REMINDERCOUNT = $taskItem.ReminderCount + 1
			# ---------------------------------------------------------------------------------
			$PROCESS_ACTION = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "select Action from [automation].[Process] where Id = $PROCESSID" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
			
			$PROCESSACTION = $PROCESS_ACTION.Action
			if ($PROCESSACTION -eq "BACKUP_NOW" -Or $PROCESSACTION -eq "SCHEDULE_BACKUP") {
				$DBNAME = $taskItem.DBName
				$PROCESSACTION = "Database Backup"
				$data = @{SNRefNo = $SNREFNO; ServerName= $SERVERNAME; InstanceName= $INSTANCENAME; DBName= $DBNAME; ScheduledTime= $SCHEDULETIME | Out-String; ProcessAction= $PROCESSACTION} | ConvertTo-Json
			} elseif ($PROCESSACTION -eq "SCHEDULE_REBALANCE" -Or $PROCESSACTION -eq "REBALANCE_NOW") {
				$PROCESSACTION = "Cluster Rebalance"
				$data = @{SNRefNo = $SNREFNO; ServerName= $SERVERNAME; InstanceName= $INSTANCENAME; ScheduledTime= $SCHEDULETIME | Out-String; ProcessAction= $PROCESSACTION} | ConvertTo-Json
			}
			
			log("SNREFNO:	$SNREFNO")
			log("PROCESSID:	$PROCESSID")
			log("TASKID:	$TASKID")
			log("SERVERNAME:	$SERVERNAME")
			log("INSTANCENAME:	$INSTANCENAME")
			log("DBNAME:	$DBNAME")
			log("SCHEDULEDTIME:	$SCHEDULETIME")
			log("REMINDERMINS:	$REMINDERMINS")
			log("PROCESSACTION:	$PROCESSACTION")
			# ---------------------------------------------------------------------------------
			$TABLE_DATA = tableData($data)
			# ---------------------------------------------------------------------------------
			log("======== START SENDING EMAIL ========")
			$TO_ADDR = $taskItem.ReminderEmail
			$Email_Sent_Result = sendReminderNotification -TO $TO_ADDR -TABLEdata $TABLE_DATA -REMINDERMINS $REMINDERMINS -PROCESSACTION $PROCESSACTION
			log("======== END SENDING EMAIL ========")
			# ---------------------------------------------------------------------------------
			if ($Email_Sent_Result -eq $true) {
				log("======== UPDATING SCHEDULED TASK TABLE WITH REMINDERCOUNT TO 1 ========")
				try {
					invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "UPDATE [automation].[ScheduledTasks] SET ReminderCount = $INC_REMINDERCOUNT WHERE Id = $TASKID" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
				} catch {
					log("ERROR:	Updating scheduledTask Entry Failed with $_.Exception.Message")
				}
			} elseif ($Email_Sent_Result -eq $false) {
				log("ERROR:	Failed Sending an Email.")
			}
		}
	} else {
		log("TASK LIST FOR EMAIL REMINDER --> [EMPTY]")
	}
}

# -----------------------------------------------------------------------------
function log ($Message)
{
	#$scriptDir = Get-Location
	$scriptDir = "C:"
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir/reminder-task-output.log
}

# Main Function
log("+++++++++++++ START SCHEDULED TASK REMINDER ++++++++++++++")
fetchReminderScheduledTaskInfo
log("+++++++++++++ END SCHEDULED TASK REMINDER ++++++++++++++")