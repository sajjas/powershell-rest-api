param(
	[String]$JOB_INPUT,
    [String]$SERVERNAME,
	[String]$INSTANCENAME,
	[String]$DBNAME
)
# -----------------------------------------------------------------------------
#          Global Definition
# -----------------------------------------------------------------------------
$SMTP = "smtp.ey.net"
$FROM = "dbbackup.scheduler@ey.com"

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"


# -----------------------------------------------------------------------------
function sendDBBackupNotification 
{
    Param (
		[array]$TO,
		[string]$TABLEdata,
		[string]$Message,
		[string]$Error
	)
    
	
	if ($Message -eq 'FAILED') {
		$Body = "Scheduled Job DB Backup $Message with following Details. 
				
				$Error"
	} else {
		$Body = "Scheduled Job DB Backup $Message with following Details. 
				
				$TABLEdata"
	}
	$messageParameters = @{
		Subject = "Scheduled Job DBBackup $Message"
		Body = "$Body"
		From = "$FROM"
		To = $TO.split(',')
		SmtpServer = "$SMTP"
	}
    log("EMAIL TO ADDR:	$TO")
	log("EMAIL FROM ADDR:	$FROM")
	LOG("BODY:	$Body")
	LOG("SMTP SERVER:	$SMTP")
	try {
		Send-MailMessage @messageParameters -BodyAsHtml -Encoding utf8
		log("----> Email sent From: $From To: $To  using smtp server: $SMTP")
		return $true
	} catch {
		log("Failed to send Email:	$_.Exception.Message")
		return $false
	}
}

# -----------------------------------------------------------------------------
function tableData($data) 
{
	$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
	$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
	$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
	$style = $style + "TD{border: 1px solid black; padding: 5px; }"
	$style = $style + "</style>"
	
	$new_data = $data | ConvertFrom-Json
	$tableOutput = $new_data | select-object -property @{N='ServiceNow Ticket';E={$_.SNRefNo}}, @{N='Server Name';E={$_.ServerName}}, @{N='Instance Name';E={$_.InstanceName}}, DBName, @{N='Backup Type';E={$_.BackupType}}, @{N='NasDrive Region';E={$_.NasDriveRegion}}, @{N='UserId';E={$_.UserId}}	| ConvertTo-Html -Head $style
	
	return $tableOutput
}

# -----------------------------------------------------------------------------
Function ScheduleDbBackup 
{
	# -------------------------------------------------------------------------
	# ----------- Input Parameters --------------
    $SERVERNAME = $SERVERNAME
    $INSTANCENAME = $INSTANCENAME
    $DBNAME = $DBNAME
	
	$json_data = $JOB_INPUT | ConvertFrom-Json
    $NASDRIVEREGION = ($SERVERNAME.ToCharArray() | select-object -First 2) -join ''
    $ALERTID = $json_data.alertId
	$SHEDULEDTASKID = $json_data.scheduleTaskId
	$type_id =  "DB_BACKUP"

	# ---------------------------------------------------------------------------------
	# fetching Alert details
    $ALERT_DATA = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "select Info from [automation].[Alert] where Id = '$ALERTID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
    $ALERTINFO = $ALERT_DATA.Info | ConvertFrom-Json
	$BACKUPTYPE = $ALERTINFO.BackupType
    
    # ---------------------------------------------------------------------------------
    # fetching scheduledtasks details
    $SCHEDULE_TASK_DATA = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "select SNRefNo, ScheduledUser from [automation].[ScheduledTasks] where Id = '$SHEDULEDTASKID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD

    $SCHEDULED_USER = $SCHEDULE_TASK_DATA.ScheduledUser
	$USERID = $SCHEDULE_TASK_DATA.ScheduledUser
	$SNREFNO = $SCHEDULE_TASK_DATA.SNRefNo
	
	log("SERVERNAME:	$SERVERNAME")
	log("INSTANCENAME:	$INSTANCENAME")
	log("DBNAME:	$DBNAME")
	log("BACKUPTYPE:	$BACKUPTYPE")
	log("NASDRIVEREGION:	$NASDRIVEREGION")
	log("ALERTID:	$ALERTID")
    log("SHEDULEDTASKID:	$SHEDULEDTASKID")
	log("SCHEDULED_USER:	$SCHEDULED_USER")
	log("SNREFNO: $SNREFNO")

	
	# ---------------------------------------------------------------------------------
	# Format Table data
	$data = @{SNRefNo = $SNREFNO; ServerName= $SERVERNAME; InstanceName= $INSTANCENAME; DBName= $DBNAME; BackupType= $BACKUPTYPE; NasDriveRegion= $NASDRIVEREGION; UserId= $USERID; Status= "STARTED"} | ConvertTo-Json
	$TABLE_DATA = tableData($data)
	
	# ---------------------------------------------------------------------------------
	# Sending Email Notification to Scheduled User before DB Backup Start.
	$Email_Sent_Result = sendDBBackupNotification -TO $SCHEDULED_USER -TABLEdata $TABLE_DATA -Message "STARTED"
	
	# ---------------------------------------------------------------------------------
	# Trigger Fetching NAS Drive Path from NASShare Table 
	log ("Request : Derive NAS Drive Path from NASShare Table")
    $nas_drive_path_sql_output = SelectNASDrivePath $NASDRIVEREGION $VAR_SERVERINSTANCE $VAR_DATABASE $VAR_USERNAME $VAR_PASSWORD
    $nas_path_output = $nas_drive_path_sql_output | ConvertFrom-Json
	$nas_drive_path_json_output = $nas_path_output.backupPath | ConvertTo-Json
	$nas_drive_path_serialized = $nas_drive_path_json_output.Trim('"').Replace('\\', '\')	
	# ----------- Derived Input Parameter DRIVE_PATH --------------
	$nas_drive_path = $nas_drive_path_serialized
	log("NAS Drive Path : $nas_drive_path")
	
    # ---------------------------------------------------------------------------------
	# Insert ServiceRequest Table with Request STARTED Status
	$info = @{BACKUPTYPE= $BACKUPTYPE; NASDRIVEREGION= $NASDRIVEREGION; nas_drive_path= $nas_drive_path} | ConvertTo-Json
	$status_id = "STARTED"
	$ServiceRequest_Id =  InsertOrUpdateServiceRequestTable "INSERT" 0 $VAR_SERVERINSTANCE $VAR_DATABASE $VAR_USERNAME $VAR_PASSWORD $type_id $status_id $ALERTID $SERVERNAME $INSTANCENAME $DBNAME $SNREFNO $info $USERID "NULL"
	log("ServiceRequest Table Id  : $ServiceRequest_Id")
	
    # ---------------------------------------------------------------------------------
	# Trigger Get Available Drive Letter 
	log("Request : Trigger Get Drive Letter")
	$drive_letter_output = (68..90 | %{$L=[char]$_; if ((gdr).Name -notContains $L) {$L}})[0]
	
	if ([String]::IsNullOrEmpty($drive_letter_output) -eq $true) {
		# Update ServiceRequest Table with Request ERROR Status 
		$status_id = "FAILED"
		$ERROR_MSG = "Empty Drive Letter"
		$ServiceRequest_update_output = InsertOrUpdateServiceRequestTable "UPDATE" $ServiceRequest_Id $VAR_SERVERINSTANCE $VAR_DATABASE $VAR_USERNAME $VAR_PASSWORD $type_id $status_id $ALERTID $SERVERNAME $INSTANCENAME $DBNAME $SNREFNO $info $USERID $ERROR_MSG
		log("Update ServiceRequest Table With Error: $ServiceRequest_update_output")
		# send email and return incase of Failure
		BackupFailedEmail($ERROR_MSG)
	} else {
		#  Derived Input Parameter DRIVE_LETTER
		$drive_letter = $drive_letter_output + ':'
	}
	log("Drive Letter : $drive_letter")
	
    # ---------------------------------------------------------------------------------
	# Trigger Create NAS Drive
	log("Request : Trigger Create NAS Drive")
	$create_nas_drive_output = CreateNASDrive $INSTANCENAME $drive_letter $nas_drive_path
	$create_nas_drive_output_parsed = $create_nas_drive_output | ConvertFrom-Json
	if ($create_nas_drive_output_parsed.output -eq $false) { 
		# Update ServiceRequest Table with Request ERROR Status
		$status_id = "FAILED"
		$ServiceRequest_update_output = InsertOrUpdateServiceRequestTable "UPDATE" $ServiceRequest_Id $VAR_SERVERINSTANCE $VAR_DATABASE $VAR_USERNAME $VAR_PASSWORD $type_id $status_id $ALERTID $SERVERNAME $INSTANCENAME $DBNAME $SNREFNO $info $USERID $create_nas_drive_output
		log("Update ServiceRequest With Error Output  : $ServiceRequest_update_output")
		# send email and return incase of Failure
		BackupFailedEmail($create_nas_drive_output)
	}
	log("Create NAS Drive Output : $create_nas_drive_output")
	
    # ---------------------------------------------------------------------------------
	# Trigger Split count for backup incase of FULL Backup Request 
	if ($BACKUPTYPE -eq "FULL"){
		log("Request : Trigger Split count for backup")
		$last_backup_file_size_result = GetLastBackupFileSize $INSTANCENAME $DBNAME
		$output = $last_backup_file_size_result | ConvertFrom-Json
		$LastBackupSize_Data = $output.Success_Message.actual_output | Select-Object -first 1
		$converted_data = $LastBackupSize_Data | ConvertFrom-Json
		log("Last Backup Data : $LastBackupSize_Data")
		
		try {
			if ( $converted_data.BackupSize_GB -le 10 ) {
				$final_splits_count = 1
			} else {
				$splits_count = $converted_data.BackupSize_GB / 10
				if ( $splits_count -gt 10 ) {
					$final_splits_count =  10
				} else {
					$final_splits_count = [math]::floor($splits_count)
				}
			}
		} catch {
			# Update ServiceRequest Table with Request ERROR Status
			$error_output = $_.Exception.Message
			$status_id = "FAILED"
			$ServiceRequest_update_output = InsertOrUpdateServiceRequestTable "UPDATE" $ServiceRequest_Id $VAR_SERVERINSTANCE $VAR_DATABASE $VAR_USERNAME $VAR_PASSWORD $type_id $status_id $ALERTID $SERVERNAME $INSTANCENAME $DBNAME $SNREFNO $info $USERID $error_output
			log("Update ServiceRequest Table With Error Output  : $ServiceRequest_update_output")
			# send email and return incase of Failure
			BackupFailedEmail($error_output)
		}
	} else {
		$final_splits_count = 1
	}
	# Derived Input Parameter DB-SPLITS 
	$db_splits = $final_splits_count | ConvertTo-Json
	log("Backup Files Split Count : $db_splits")
	
    # ---------------------------------------------------------------------------------
	# Trigger DB Backup 
	log("Request : Trigger DB Backup")
	try {
		# Update ServiceRequest Table with Request INPROGRESS Status 
		$status_id = "INPROGRESS"
		$ServiceRequest_update_output = InsertOrUpdateServiceRequestTable "UPDATE" $ServiceRequest_Id $VAR_SERVERINSTANCE $VAR_DATABASE $VAR_USERNAME $VAR_PASSWORD $type_id $status_id $ALERTID $SERVERNAME $INSTANCENAME $DBNAME $SNREFNO $info $USERID "NULL"
		log("Update ServiceRequest Table With Inprogress  : $ServiceRequest_update_output")
		$db_backup_result = TriggerDatabaseBackup $INSTANCENAME $DBNAME $BACKUPTYPE $drive_letter $db_splits
		
		log("DB Backup Request Output : $db_backup_result")
		$db_backup_result = $db_backup_result | ConvertFrom-Json
		
		if ($db_backup_result.output -eq $false) {
			$error_output = $db_backup_result.Fail_Message | ConvertTo-Json
			$status_id = "FAILED"
			# Update ServiceRequest Table with Request ERROR Status
			$ServiceRequest_update_output = InsertOrUpdateServiceRequestTable "UPDATE" $ServiceRequest_Id $VAR_SERVERINSTANCE $VAR_DATABASE $VAR_USERNAME $VAR_PASSWORD $type_id $status_id $ALERTID $SERVERNAME $INSTANCENAME $DBNAME $SNREFNO $info $USERID $error_output
			log("Update ServiceRequest Table With Error Output  : $ServiceRequest_update_output")
			# send email and return incase of Failure
			BackupFailedEmail($error_output)
		} else {
			$status_id = "COMPLETED"
			# Update ServiceRequest Table with Request COMPLETED Status
			$ServiceRequest_update_output = InsertOrUpdateServiceRequestTable "UPDATE" $ServiceRequest_Id $VAR_SERVERINSTANCE $VAR_DATABASE $VAR_USERNAME $VAR_PASSWORD $type_id $status_id $ALERTID $SERVERNAME $INSTANCENAME $DBNAME $SNREFNO $info $USERID "NULL"
			log("Update ServiceRequest Table Completed Status  : $ServiceRequest_update_output")
            
            log("Create Backup Request Completed Successfully")
	
            # ---------------------------------------------------------------------------------
	        $UPDATE_ALERT_DATA = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "UPDATE [automation].[Alert] SET StatusId='COMPLETED' WHERE Id='$ALERTID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD

	        # ---------------------------------------------------------------------------------
	        $UPDATE_SCHEDULE_TASK_DATA = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "UPDATE [automation].[ScheduledTasks] SET StatusId='COMPLETED', ReturnInfo='Create Backup Completed Successfully' WHERE Id='$SHEDULEDTASKID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
            
	        # ---------------------------------------------------------------------------------
	        # Sending Email Notification to Scheduled User after DB Backup Finish.
	        $Email_Sent_Result = sendDBBackupNotification -TO $SCHEDULED_USER -TABLEdata $TABLE_DATA -Message "FINISHED"
		}
	} catch {
		$error_output = $_.Exception.Message
		$status_id = "FAILED"
		# Update ServiceRequest Table with Request ERROR Status
		$ServiceRequest_update_output = InsertOrUpdateServiceRequestTable "UPDATE" $ServiceRequest_Id, $VAR_SERVERINSTANCE $VAR_DATABASE $VAR_USERNAME $VAR_PASSWORD $type_id $status_id $ALERTID $SERVERNAME $INSTANCENAME $DBNAME $SNREFNO $info $USERID $error_output
		
        log("Update ServiceRequest Table With Error Output  : $ServiceRequest_update_output")
		# send email and return incase of Failure
		BackupFailedEmail($error_output)
	}
	
    # ---------------------------------------------------------------------------------
	# Trigger Delete NAS Drive
	log("Request : Trigger Delete NAS Drive")
	$delete_nas_drive = DeleteNASDrive $INSTANCENAME $drive_letter
	
	log("Delete NAS Drive Output  : $delete_nas_drive")
}

# -----------------------------------------------------------------------------
Function BackupFailedEmail($ERROR)
{
	# ---------------------------------------------------------------------------------
	$UPDATE_ALERT_DATA = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "UPDATE [automation].[Alert] SET StatusId='FAILED' WHERE Id='$ALERTID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD  
	
    # ---------------------------------------------------------------------------------
	$UPDATE_SCHEDULE_TASK_DATA = invoke-sqlcmd -serverInstance $VAR_SERVERINSTANCE -query "UPDATE [automation].[ScheduledTasks] SET StatusId='FAILED', ReturnInfo='$ERROR' WHERE Id='$SHEDULEDTASKID'" -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD
	 
	# ---------------------------------------------------------------------------------
	# Sending Email Notification to Scheduled User If DB Backup Failed.
	$Email_Sent_Result = sendDBBackupNotification -TO $SCHEDULED_USER -TABLEdata $TABLE_DATA -Message "FAILED" -Error $ERROR
}

# -----------------------------------------------------------------------------
Function TriggerDatabaseBackup 
{
	[cmdletbinding()]
	param (
		[string]$SQLINSTANCE,
		[string]$DBNAME,
		[string]$BACKUPTYPE,
		[string]$NASDRIVELETTER,
		[string]$DBSPLITS
	)

	$Date = Get-Date -format yyyy-MM-dd
	$sql_srv_inst = $SQLINSTANCE -replace '\\','-'
	$backupfiles = @()
	
	if ($DBSPLITS -gt 1){
		foreach ($split in 1..$DBSPLITS) {
			$backupfiles += "$NASDRIVELETTER\$sql_srv_inst-$DBNAME-DOU-$BACKUPTYPE-$date-split-$split.bak"
		}
	} else {
		$backupfiles = "$NASDRIVELETTER\$sql_srv_inst-$DBNAME-DOU-$BACKUPTYPE-$date.bak"
	}

	If ($BACKUPTYPE -eq 'FULL')  {
		try {
			# Backup a complete database
			Backup-SqlDatabase -ServerInstance $SQLINSTANCE `
								-Database $DBNAME `
								-CopyOnly `
								-BackupFile $backupfiles `
								-CompressionOption On `
								-BackupAction Database `
								-Initialize `
								-checksum
			$output_data = @{output = $true; Success_Message = @{BACKUPTYPE= $BACKUPTYPE; database= $DBNAME; Location= $backupfiles; Server= $SQLINSTANCE}} | ConvertTo-Json
			return $output_data
		} catch {
			$error_data = "$_".replace('\', '\\').replace('"', ' ') 
			$output_data = @{output= $false; Fail_Message= $error_data} | ConvertTo-Json
			return $output_data
		}
	}  ElseIf ($BACKUPTYPE -eq 'DIFF')  {
		try {
			# Create a differential backup
			Backup-SqlDatabase -ServerInstance $SQLINSTANCE `
								-Database $DBNAME `
								-BackupFile $backupfiles `
								-CompressionOption On `
								-BackupAction Database `
								-Incremental `
								-Initialize `
								-checksum
			$output_data = @{output = $true; Success_Message = @{BACKUPTYPE= $BACKUPTYPE; database= $DBNAME; Location= $backupfiles; Server= $SQLINSTANCE}} | ConvertTo-Json
			return $output_data
		} catch {
			$error_data = "$_".replace('\', '\\').replace('"', ' ') 
			$output_data = @{output= $false; Fail_Message= $error_data} | ConvertTo-Json
			return $output_data
		}

	}  Else {
		'Cannot determine Backup Type Selected'
	} 
}

# -----------------------------------------------------------------------------
Function SelectNASDrivePath 
{
	[cmdletbinding()]
    Param
    (
        [string]$VAR_NASREGION,
		[string]$VAR_DBSERVER,
		[string]$VAR_DATABASE,
		[string]$VAR_USERNAME,
		[string]$VAR_PASSWORD
    )
	$select_nas_path_query = "SELECT BackupPath FROM automation.NASShare where RegionId = '$VAR_NASREGION'"
    $select_nas_drive_path = Invoke-SqlCmd -ServerInstance "$VAR_DBSERVER" -Query "$select_nas_path_query" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"

	return $select_nas_drive_path | ConvertTo-Json
}

# -----------------------------------------------------------------------------
Function InsertOrUpdateServiceRequestTable
{
	param (
		$type,
		$insert_id,
		$connString_db_server,
		$connString_DBNAME,
		$connString_username,
		$connString_password,
		$type_id,
		$status_id,
        $alert_id,
		$SERVERNAME,
		$INSTANCENAME,
		$DBNAME,
		$SNREFNO,
		$info,
		$USERID,
		$comments
	)

	if ($type -eq "INSERT") {
		$queryString=" 
		INSERT INTO [automation].[ServiceRequest] 
				   ([TypeId]
				   ,[StatusId]
                   ,[AlertId]
				   ,[ServerName] 
				   ,[InstanceName]
				   ,[DBName]
				   ,[SNRefNo]
				   ,[Info] 
				   ,[Comments] 
				   ,[RequestedUser]
				   ,[CreationDate]) 
			 VALUES 
				   ('$type_id'
				   ,'$status_id'
                   ,'$alert_id'
				   ,'$SERVERNAME' 
				   ,'$INSTANCENAME' 
				   ,'$DBNAME'
				   ,'$SNREFNO'
				   ,'$info'
				   ,'$comments'
				   ,'$USERID'
				   ,getdate());
		SELECT SCOPE_IDENTITY();
		"	
	} elseif ($type -eq "UPDATE") {
		$queryString=" 
		UPDATE [automation].[ServiceRequest] SET
			TypeId='$type_id', 
			StatusId='$status_id',
            AlertId='$alert_id',
			ServerName='$SERVERNAME',
			InstanceName='$INSTANCENAME', 
			DBName='$DBNAME', 
			SNRefNo='$SNREFNO', 
			Info='$info', 
			Comments='$comments', 
			RequestedUser='$USERID',
			CreationDate=getdate()
		WHERE Id='$insert_id'; 
		SELECT SCOPE_IDENTITY();
		"
	}
    
    $sql_cmd_output_data = Invoke-SQLcmd -ServerInstance "$connString_db_server" -Query "$queryString" -Database "$connString_DBNAME" -Username "$connString_username" -Password "$connString_password"

	return $sql_cmd_output_data.Column1
}

# -----------------------------------------------------------------------------
Function CreateNASDrive
{
	param (
		[string]$SQLInstance,
		[string]$NASDriveLetter,
		[string]$NASDrivePath
	)

	try {
		# ----------------------------Enable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 1; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 1; RECONFIGURE;"

		$sp_configure_enable_output = @{output= $true} | ConvertTo-Json
	}catch {
		$sp_configure_enable_output = @{output= $false; Fail_Message= $_.Exception.Message} | ConvertTo-Json
	}

	$result = $sp_configure_enable_output | ConvertFrom-Json

	if ($result.output -eq 'True') {
		# --------------------Share Mapping is here, use appropriate value-----------------
		try {
			$InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
			# retrieve the password.
			[Byte[]] $key = (1..16)  	
			$SecurePassword = ConvertTo-SecureString $DBAToolPassword -Key $key
			$DBAToolPassword = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword))
			$sql_cmd_output_data = (Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC xp_cmdshell 'net use $NASDriveLetter $NASDrivePath /User:$InventAccount $DBAToolPassword'").output | Out-String
			
			if ($sql_cmd_output_data -NotMatch "The local device name is already in use" -And $sql_cmd_output_data -Match "error") {
				$output_data = @{output= $false; Fail_Message= @{NAS_Drive_Letter= $NASDriveLetter; actual_output= $sql_cmd_output_data}} | ConvertTo-Json
			} else {
				$output_data = @{output= $true; Success_Message= @{NAS_Drive_Letter= $NASDriveLetter; actual_output= $sql_cmd_output_data}} | ConvertTo-Json
			}      
		} catch {
			$error_data = "$_.Exception.Message"
			$output_data = @{output= $false; Fail_Message= $error_data} | ConvertTo-Json
		}
		
		# ----------------------------Disable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 0; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 0; RECONFIGURE;"

		return $output_data

	} elseif ($result.output -eq "False") {
		$error_data = "$_.Exception.Message"
		$output_data = @{output= $false; Fail_Message= $error_data} | ConvertTo-Json
		
		# ----------------------------Disable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 0; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 0; RECONFIGURE;"
		return $output_data
	}  
}

# -----------------------------------------------------------------------------
Function GetLastBackupFileSize 
{
	param (
		[string]$db_server,
		[string]$DBNAME
	)

	$queryString = "declare @BACKUPTYPE CHAR(1) = 'D' --'D' full, 'L' log
			;with Radhe as (
				SELECT  @@Servername as [Server_Name],
				B.name as Database_Name, 
				ISNULL(STR(ABS(DATEDIFF(day, GetDate(), MAX(Backup_finish_date)))), 'NEVER') as DaysSinceLastBackup,
				ISNULL(Convert(char(11), MAX(backup_finish_date), 113)+ ' ' + CONVERT(VARCHAR(8),MAX(backup_finish_date),108), 'NEVER') as LastBackupDate
				,BackupSize_GB=CAST(COALESCE(MAX(A.BACKUP_SIZE),0)/1024.00/1024.00/1024.00 AS NUMERIC(18,2))
				,BackupSize_MB=CAST(COALESCE(MAX(A.BACKUP_SIZE),0)/1024.00/1024.00 AS NUMERIC(18,2))
				,media_set_id = MAX(A.media_set_id)
				,[AVG Backup Duration]= AVG(CAST(DATEDIFF(s, A.backup_start_date, A.backup_finish_date) AS int))
				,[Longest Backup Duration]= MAX(CAST(DATEDIFF(s, A.backup_start_date, A.backup_finish_date) AS int))
				,A.type
				FROM sys.databases B

				LEFT OUTER JOIN msdb.dbo.backupset A 
							 ON A.database_name = B.name 
							AND A.is_copy_only = 1
							AND (@BACKUPTYPE IS NULL OR A.type = @BACKUPTYPE) 
				where A.Database_Name = '$DBNAME'
				GROUP BY B.Name, A.type

			)

			 SELECT r.[Server_Name]
				   ,r.Database_Name
				   ,[Backup Type] = r.type 
				   ,r.DaysSinceLastBackup
				   ,r.LastBackupDate
				   ,r.BackupSize_GB
				   ,r.BackupSize_MB
				   ,F.physical_device_name
				   ,r.[AVG Backup Duration]
				   ,r.[Longest Backup Duration]

			   FROM Radhe r

				LEFT OUTER JOIN msdb.dbo.backupmediafamily F
							 ON R.media_set_id = F.media_set_id
				where r.Database_Name = '$DBNAME'
				ORDER BY r.Server_Name, r.Database_Name"

	try {
	   $sql_cmd_output_data = Invoke-Sqlcmd -Query $queryString -ServerInstance "$db_server" | Select-Object -Property @{Name="LastBackupDate"; Expression={$_.LastBackupDate}}, @{Name="physical_device_name"; Expression={$_.physical_device_name}}, @{label="BackupSize_GB";expression={[math]::round($_.BackupSize_GB)}}, * -ExcludeProperty LastBackupDate, physical_device_name, BackupSize_GB, ItemArray, Table, RowError, RowState, HasErrors | ConvertTo-Json

	   $output_data = @{output= $true; Success_Message= @{actual_output= $sql_cmd_output_data}} | ConvertTo-Json
	}catch {
	   $output_data = @{output= $false; Fail_Message= $_.Exception.Message} | ConvertTo-Json
	}
	return $output_data
}

# -----------------------------------------------------------------------------
Function DeleteNASDrive
{
	param (
		$SQLInstance,
		$NASDriveLetter
	)

	try {
		# ----------------------------Enable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 1; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 1; RECONFIGURE;"

		$sp_configure_enable_output = @{output= $true} | ConvertTo-Json
	}catch {
		$sp_configure_enable_output = @{output= $false; Fail_Message= $_.Exception.Message} | ConvertTo-Json
	}

	$result = $sp_configure_enable_output | ConvertFrom-Json

	if ($result.output -eq 'True') {
		try {
			# --------------Delete Mapped Drive---------------- 
			$response = (Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC xp_cmdshell 'net use $NASDriveLetter /delete'").output

			$delete_drive = @{outpu= $true; Success_Message= $response} | ConvertTo-Json
		}catch {
			$delete_drive = @{output= $false; Fail_Message= $_.Exception.Message} | ConvertTo-Json
		}
		# ----------------------------Disable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 0; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 0; RECONFIGURE;"
		return $delete_drive

	} elseif ($result.output -eq "False") {
		$output_data = @{output= $false; Fail_Message= $_.Exception.Message}
		# ----------------------------Disable cmdshell----------------------
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 0; RECONFIGURE;"
		Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 0; RECONFIGURE;"
		return $output_data
	}  
}

# -----------------------------------------------------------------------------
function log ($Message)
{
	#$scriptDir = Get-Location
	$scriptDir = "C:"
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir/schdule-dbbackup-output.log
}

# Main Function
log("+++++++++++++ START SCHEDULED DB BACKUP TASK ++++++++++++++")
ScheduleDbBackup
log("+++++++++++++ END SCHEDULED DB BACKUP TASK ++++++++++++++")