. lib\remoteSessionHelper.ps1
. lib\helper\SRE_Log_Helper.ps1
. lib\helper\Join.ps1
. SRE-Scripts\SRE_DB_Memory.ps1
. SRE-Scripts\SRE_Trigger_Database_Backup.ps1
. SRE-Scripts\SRE_Insert_Scheduled_Job.ps1

Import-Module UniversalDashboard
#Enable-UDLogging

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"
$Hostname = $env:COMPUTERNAME

# -------------------------------------------------------------------------------------------
# 								Get Backup Request Progress 
#	EndPoint	: get-db-backup-progress
#	Method		: POST
# -------------------------------------------------------------------------------------------
$GET_BACKUP_REQUEST_PROGRESS = New-UDEndpoint -Url "/get-db-backup-progress" -Method "POST" -Endpoint {
    
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
	$sql_srv_instance = $NewBody.sql_srv_instance
    $db_name = $NewBody.db_name
	
	# --------------------- LOGGING --------------------
	$file_name = "SRE_AUTOMATION_DATABASE_BACKUP.log"
	log -Filename "$file_name" -Message "***********************************	Get DB backup Progress Requst Started For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"

    # ----------- Create Remote Session to DB Server/Jump Server -----------
	$remote_session = Create-PSSession -ServerName $db_srv_instance
	
	# ----------- Trigger Checking DB Backup Progress -----------
	log -Filename "$file_name" -Message "Request : Trigger DB Backup Progress"
 
	if ($remote_session.Count -eq 1) {
		$db_backup_progress = Invoke-Command -Session $remote_session -ScriptBlock ${function:GetDBBackupProgress} -ArgumentList ($args + @($sql_srv_instance, $db_name, "backup_create_progress"))
	} elseif ($remote_session.Count -eq 2) {
		$Jumpy_session = $remote_session[0]
		$db_backup_progress = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:GetDBBackupProgress});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @($using:sql_srv_instance, $using:db_name, "backup_create_progress"))}
	}
    log -Filename "$file_name" -Message "db_backup_progress : $db_backup_progress"
	
	# ----------- Remove Remote Session/s -----------
	if ($remote_session.Count -eq 1) {
		Remove-PSSession $remote_session
	} elseif ($remote_session.Count -eq 2) {
		Invoke-Command -Session $Jumpy_session -ScriptBlock {Remove-PSSession $(Get-PSSession)}
		Remove-PSSession $Jumpy_session
	}
	log -Filename "$file_name" -Message "***********************************	Get DB backup Progress Requst Completed For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
	
	$output = $db_backup_progress | Out-String
	if ([String]::IsNullOrEmpty($output) -eq $true) {
		return 0 | Out-String
	} else {
		return $db_backup_progress
	}
}

# -------------------------------------------------------------------------------------
# ---------------------------- Create Database Backup Request ------------------
#	EndPoint	: create-backup
#	Method		: POST
# -------------------------------------------------------------------------------------
$CREATE_DATABASE_BACKUP = New-UDEndpoint -Url "/create-backup" -Method "POST" -Endpoint {

    param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
    $sql_srv_instance = $NewBody.sql_srv_instance
    $db_name = $NewBody.db_name
    $backup_type = $NewBody.backup_type
    $nas_drive_region = $NewBody.nas_drive_region
	$user_id = $NewBody.user_id
	$service_now_incident_id = $NewBody.service_now_incident_id
	$type_id =  "DB_BACKUP"
	
	# --------------------- LOGGING --------------------
	$file_name = "SRE_AUTOMATION_DATABASE_BACKUP.log"
	log -Filename "$file_name" -Message "***********************************	Create Backup Requst Started For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	
	# ----------- Trigger Fetching NAS Drive Path from NASShare Table --------------
	log -Filename "$file_name" -Message "Request : Derive NAS Drive Path from NASShare Table"
	$nas_drive_path_sql_output = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:SelectNASDrivePath} -ArgumentList ($args + @($nas_drive_region, $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD))
	$nas_path_output = $nas_drive_path_sql_output | ConvertFrom-Json
	$nas_drive_path_json_output = $nas_path_output.backupPath | ConvertTo-Json
	$nas_drive_path_serialized = $nas_drive_path_json_output.Trim('"').Replace('\\', '\')	
	# ----------- Derived Input Parameter DRIVE_PATH --------------
	$nas_drive_path = $nas_drive_path_serialized
	log -Filename "$file_name" -Message "NAS Drive Path : $nas_drive_path"
	
	# ----------- Insert ServiceRequest Table with Request STARTED Status -----------
	$info = @{backup_type= $backup_type; nas_drive_region= $nas_drive_region; nas_drive_path= $nas_drive_path} | ConvertTo-Json
	$status_id = "STARTED"
	$ServiceRequest_Id = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:InsertOrUpdateServiceRequestTable} -ArgumentList ($args + @("INSERT", 0, $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, "NULL"))
	log -Filename "$file_name" -Message "Insert ServiceRequest Table Output  : $ServiceRequest_Id"
	
	# ----------- Create Remote Session to DB Server/Jump Server -----------
	$remote_session = Create-PSSession -ServerName $db_srv_instance
	
	# ----------- Trigger Get Available Drive Letter -----------
	log -Filename "$file_name" -Message "Request : Trigger Get Drive Letter <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	if ($remote_session.Count -eq 1) {
		$drive_letter_output = Invoke-Command -Session $remote_session -ScriptBlock {(68..90 | %{$L=[char]$_; if ((gdr).Name -notContains $L) {$L}})[0]}
	} elseif ($remote_session.Count -eq 2) {
		$Jumpy_session = $remote_session[0]
		$drive_letter_output = Invoke-Command -Session $Jumpy_session -ScriptBlock {Invoke-Command -Session $(Get-PSSession) -ScriptBlock {(68..90 | %{$L=[char]$_; if ((gdr).Name -notContains $L) {$L}})[0]}}
	}
	if ([String]::IsNullOrEmpty($drive_letter_output) -eq $true) {
		# ----------- Update ServiceRequest Table with Request ERROR Status -----------
		$status_id = "ERROR"
		$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:InsertOrUpdateServiceRequestTable} -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, "Empty Drive Letter"))
		log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
		return
	} else {
		# ----------- Derived Input Parameter DRIVE_LETTER --------------
		$drive_letter = $drive_letter_output + ':'
	}
	log -Filename "$file_name" -Message "Drive Letter : $drive_letter"
	
	# ----------- Trigger Create NAS Drive --------------
	log -Filename "$file_name" -Message "Request : Trigger Create NAS Drive <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	if ($remote_session.Count -eq 1) {
		$create_nas_drive_output = Invoke-Command -Session $remote_session -ScriptBlock ${function:CreateNASDrive} -ArgumentList ($args + @($sql_srv_instance, $drive_letter, $nas_drive_path))
	} elseif ($remote_session.Count -eq 2) {
		$Jumpy_session = $remote_session[0]
		$create_nas_drive_output = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:CreateNASDrive});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @($using:sql_srv_instance, $using:drive_letter, $using:nas_drive_path))}
	}
	$create_nas_drive_output_parsed = $create_nas_drive_output | ConvertFrom-Json
	if ($create_nas_drive_output_parsed.output -eq $false) { 
		# ----------- Update ServiceRequest Table with Request ERROR Status -----------
		$status_id = "ERROR"
		$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:InsertOrUpdateServiceRequestTable} -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, $create_nas_drive_output))
		log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
		return
	}
	log -Filename "$file_name" -Message "Create NAS Drive Output : $create_nas_drive_output"
	
	# ----------- Trigger Split count for backup incase of FULL Backup Request -----------
	if ($backup_type -eq "FULL"){
		log -Filename "$file_name" -Message "Request : Trigger Split count for backup <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
		if ($remote_session.Count -eq 1) {
			$last_backup_file_size_result = Invoke-Command -Session $remote_session -ScriptBlock ${function:GetLastBackupFileSize} -ArgumentList ($args + @($sql_srv_instance, $db_name))
		} elseif ($remote_session.Count -eq 2) {
			$Jumpy_session = $remote_session[0]
			$last_backup_file_size_result = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:GetLastBackupFileSize});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @($using:sql_srv_instance, $using:db_name))}
		}
		$output = $last_backup_file_size_result | ConvertFrom-Json
		$LastBackupSize_Data = $output.Success_Message.actual_output | Select-Object -first 1
		$converted_data = $LastBackupSize_Data | ConvertFrom-Json
		log -Filename "$file_name" -Message "Last Backup Data : $LastBackupSize_Data"
		
		try {
			if ( $converted_data.BackupSize_GB -le 10 ) {
				$final_splits_count = 1
			} else {
				$splits_count = $converted_data.BackupSize_GB / 10
				if ( $splits_count -gt 10 ) {
					$final_splits_count =  10
				} else {
					$final_splits_count = [math]::floor($splits_count)
				}
			}
		} catch {
			# ----------- Update ServiceRequest Table with Request ERROR Status -----------
			$status_id = "ERROR"
			$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:InsertOrUpdateServiceRequestTable} -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, $_.Exception.Message))
			log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
			return
		}
	} else {
		$final_splits_count = 1
	}
	# ----------- Derived Input Parameter DB-SPLITS --------------
	$db_splits = $final_splits_count | ConvertTo-Json
	log -Filename "$file_name" -Message "Backup Files Split Count : $db_splits"
	
	# ----------- Trigger DB Backup --------------
	log -Filename "$file_name" -Message "Request : Trigger DB Backup <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	try {
		# ----------- Update ServiceRequest Table with Request INPROGRESS Status -----------
		$status_id = "INPROGRESS"
		$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:InsertOrUpdateServiceRequestTable} -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, "NULL"))
		log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
		if ($remote_session.Count -eq 1) {
			$db_backup_result = Invoke-Command -Session $remote_session -ScriptBlock ${function:TriggerDatabaseBackup} -ArgumentList ($args + @($sql_srv_instance, $db_name, $backup_type, $drive_letter, $db_splits))
		} elseif ($remote_session.Count -eq 2) {
			$Jumpy_session = $remote_session[0]
			$db_backup_result = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:TriggerDatabaseBackup});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @($using:sql_srv_instance, $using:db_name, $using:backup_type, $using:drive_letter, $using:db_splits))}
		}
		
		log -Filename "$file_name" -Message "DB Backup Request Output : $db_backup_result"
		$db_backup_result = $db_backup_result | ConvertFrom-Json
		
		if ($db_backup_result.output -eq $false) {
			$error_output = $db_backup_result.Fail_Message | ConvertTo-Json
			$status_id = "ERROR"
			# ----------- Update ServiceRequest Table with Request ERROR Status -----------
			$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:InsertOrUpdateServiceRequestTable} -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, $error_output))
			log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
			return
		} else {
			$status_id = "COMPLETED"
			# ----------- Update ServiceRequest Table with Request COMPLETED Status -----------
			$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:InsertOrUpdateServiceRequestTable} -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, "NULL"))
			log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
		}
	} catch {
		$error_output = $_.Exception.Message
		$status_id = "ERROR"
		# ----------- Update ServiceRequest Table with Request ERROR Status -----------
		$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:InsertOrUpdateServiceRequestTable} -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, $error_output))
		log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
		return
	}
	
	######### Trigger Delete NAS Drive #########
	log -Filename "$file_name" -Message "Request : Trigger Delete NAS Drive <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	if ($remote_session.Count -eq 1) {
		$delete_nas_drive = Invoke-Command -Session $remote_session -ScriptBlock ${function:DeleteNASDrive} -ArgumentList ($args + @($sql_srv_instance, $drive_letter))
	} elseif ($remote_session.Count -eq 2) {
		$Jumpy_session = $remote_session[0]
		$delete_nas_drive = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:DeleteNASDrive});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @($using:sql_srv_instance, $using:drive_letter))}
	}
	log -Filename "$file_name" -Message "Delete NAS Drive Output  : $delete_nas_drive"
	
	# ----------- Remove Remote Session/s -----------
	if ($remote_session.Count -eq 1) {
		Remove-PSSession $remote_session
	} elseif ($remote_session.Count -eq 2) {
		Invoke-Command -Session $Jumpy_session -ScriptBlock {Remove-PSSession $(Get-PSSession)}
		Remove-PSSession $Jumpy_session
	}
	log -Filename "$file_name" -Message "***********************************	Create Backups Request Completed Successfully For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
}

# ---------------------------------------------------------------------------------------
# 								Get DBs, Size and LastBackupDate Request
#	EndPoint	: get-dbs-size-lastbkp-data
#	Method		: POST 
# ---------------------------------------------------------------------------------------
$GET_DBS_SIZE_LASTBKP_DATA = New-UDEndpoint -Url "/get-dbs-size-lastbkp-data" -Method "POST" -Endpoint {
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
	$sql_srv_instance = $NewBody.sql_srv_instance
	
	# --------------------- LOGGING --------------------
	$file_name = "SRE_AUTOMATION_DATABASE_BACKUP.log"
	log -Filename "$file_name" -Message "***********************************	Get DBs, Size and LastBackupDate Request Started	For -> Remote Server : $db_srv_instance  ***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	
	# ----------- Create Remote Session to DB Server/Jump Server -----------
	$remote_session = Create-PSSession -ServerName $db_srv_instance

    # ----------- Trigger Getting DBs, Size and LastBackupDate -----------
	log -Filename "$file_name" -Message "Request : Trigger Getting DBs, Size and LastBackupDate"
	
	if ($remote_session.Count -eq 1) {
		$dbs_size_lastbkp_data = Invoke-Command -Session $remote_session -ScriptBlock ${function:GetDBsSizeLastBackupData} -ArgumentList ($args + @($sql_srv_instance))
	} elseif ($remote_session.Count -eq 2) {
		$Jumpy_session = $remote_session[0]
		$dbs_size_lastbkp_data = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:GetDBsSizeLastBackupData});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @($using:sql_srv_instance))}
	}
	
	# ----------- Remove Remote Session/s -----------
	if ($remote_session.Count -eq 1) {
		Remove-PSSession $remote_session
	} elseif ($remote_session.Count -eq 2) {
		Invoke-Command -Session $Jumpy_session -ScriptBlock {Remove-PSSession $(Get-PSSession)}
		Remove-PSSession $Jumpy_session
	}
	log -Filename "$file_name" -Message "***********************************	Get DBs, Size and LastBackupDate Request Completed	***********************************"
	
	$processes_data = $dbs_size_lastbkp_data | ConvertFrom-Json
	if ($processes_data.output -eq $true) {
		$backup_histrory = $processes_data.data | ConvertTo-Json
		log -Filename "$file_name" -Message "Response : $backup_histrory"
		return $backup_histrory
	} else {
		log -Filename "$file_name" -Message "Response : $dbs_size_lastbkp_data"
		return $dbs_size_lastbkp_data
	}
}

# -------------------------------------------------------------------------------------------
# 								DB Backup Request cancel 
#	EndPoint	: db-backup-request-cancel
#	Method		: POST
# -------------------------------------------------------------------------------------------
$GET_BACKUP_REQUEST_CANCEL = New-UDEndpoint -Url "/db-backup-request-cancel" -Method "POST" -Endpoint {
    
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
	$sql_srv_instance = $NewBody.sql_srv_instance
    $db_name = $NewBody.db_name
	
	# --------------------- LOGGING --------------------
	$file_name = "SRE_AUTOMATION_DATABASE_BACKUP.log"
	log -Filename "$file_name" -Message "***********************************	DB backup Request Cancel Started For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	
	# ----------- Create Remote Session to DB Server/Jump Server -----------
	$remote_session = Create-PSSession -ServerName $db_srv_instance
	
	# ----------- Trigger DB Backup Request Cancel -----------
	log -Filename "$file_name" -Message "Request : Trigger DB Backup Request Cancel"
    
	if ($remote_session.Count -eq 1) {
		$db_backup_request_cancel = Invoke-Command -Session $remote_session -ScriptBlock ${function:GetDBBackupProgress} -ArgumentList ($args + @($sql_srv_instance, $db_name, "backup_create_cancel"))
	} elseif ($remote_session.Count -eq 2) {
		$Jumpy_session = $remote_session[0]
		$db_backup_progress = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:GetDBBackupProgress});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @($using:sql_srv_instance, $using:db_name, "backup_create_cancel"))}
	}
    log -Filename "$file_name" -Message "db_backup_request_cancel_output : $db_backup_request_cancel"
    
	# ----------- Remove Remote Session/s -----------
	if ($remote_session.Count -eq 1) {
		Remove-PSSession $remote_session
	} elseif ($remote_session.Count -eq 2) {
		Invoke-Command -Session $Jumpy_session -ScriptBlock {Remove-PSSession $(Get-PSSession)}
		Remove-PSSession $Jumpy_session
	}
	log -Filename "$file_name" -Message "***********************************	DB backup Requst Cancel Completed For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
	
	return $db_backup_request_cancel
}

# ---------------------------------------------------------------------------------------
# 								Scheduled Job data Insert into Table
#	EndPoint	: schedule-job-table-insert-api
#	Method		: POST
# ---------------------------------------------------------------------------------------
$SCHEDULE_JOB_INSERT_API = New-UDEndpoint -Url "/schedule-job-table-insert-api" -Method "POST" -Endpoint {

	param($Body)

    # Validate all JSON fields are containing valid JSON
    try {
        $NewBody = ConvertFrom-Json $Body -ErrorAction Stop;
        $validJson = $true;
    } catch {
        $validJson = $false;
    }

    if ($validJson -eq $false) {
        return "Provided text is not a valid JSON string"
    }
	
	# ----------- Input Parameters --------------
    $Job_Name = $NewBody.Job_Name
	$Alert_Type = $NewBody.Alert_Type
	$Created_By = $NewBody.Created_By
    $Target_Servers = $NewBody.Target_Servers | convertTo-Json
    $Job_Inputs = $NewBody.Job_Inputs
	$Job_Frequency = $NewBody.Job_Frequency
    $Job_Time = $NewBody.Job_Time
	
	if ($Job_Frequency -eq "MONTHLY") {
		$Job_Time = $Job_Time.Monthly | ConvertTo-Json
	} elseif ($Job_Frequency -eq "WEEKLY") {
		$Job_Time = $Job_Time.Weekly | ConvertTo-Json
	} elseif ($Job_Frequency -eq "DAILY") {
		$Job_Time = $Job_Time.Daily | ConvertTo-Json
	} elseif ($Job_Frequency -eq "HOURLY") {
		$Job_Time = $Job_Time.Hourly | ConvertTo-Json
	}
    
	# --------------------- LOGGING --------------------
	$file_name = "SRE_AUTOMATION_INSERT_SCHEDULED_JOB.log"
	log -Filename "$file_name" -Message "***********************************	Scheduled Job Data Insert/Update into Table Started For -> Job : $Job_Name	***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"

	# ----------- Trigger Fetching Powershell script file name from LookupMaster Table -----------
	$script_name_output = Invoke-Command -ComputerName $Hostname -ScriptBlock {invoke-sqlcmd -serverInstance $using:VAR_SERVERINSTANCE -query "select LookupName from [automation].[LookupMaster] where LookupType = 'JOB_SCRIPT_NAME' And LookupCode = '$using:Alert_Type'" -Database $using:VAR_DATABASE -Username $using:VAR_USERNAME -Password $using:VAR_PASSWORD} 
	$Job_Script_File = $script_name_output.LookupName
	
	log -Filename "$file_name" -Message "Request Arguments:	$Job_Name, $Alert_Type, $Created_By, $Target_Servers, $Job_Script_File, $Job_Inputs, $Job_Frequency, $Job_Time"
	if ($Job_Inputs.PSobject.Properties.Name -contains "scheduled_job_id") {
		# ----------- Trigger Scheduled Job Data Update into Table -----------
		$update_row_id = $Job_Inputs.scheduled_job_id
		$Job_Inputs = $NewBody.Job_Inputs | ConvertTo-Json
		log -Filename "$file_name" -Message "Request : Trigger Scheduled Job Data Update into Table"
		$update_scheduled_job_table_output = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:InsertScheduledJob} -ArgumentList ($args + @($Job_Name, $Alert_Type, $Created_By, $Target_Servers, $Job_Script_File, $Job_Inputs, $Job_Frequency, $Job_Time, "CREATED", "UPDATE", $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD, $update_row_id))
        log -Filename "$file_name" -Message "Response : $update_scheduled_job_table_output"
		if ([String]::IsNullOrEmpty($update_scheduled_job_table_output) -eq $true) {
			return @{output = $true; Success_Message = $update_row_id} | ConvertTo-Json
		} else {
			return @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
		}
	} else {
		# ----------- Trigger Scheduled Job Data Insert and update into Table -----------
		log -Filename "$file_name" -Message "Request : Trigger Scheduled Job Data Insert into Table"
		$insert_scheduled_job_table_output = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:InsertScheduledJob} -ArgumentList ($args + @($Job_Name, $Alert_Type, $Created_By, $Target_Servers, $Job_Script_File, $Job_Inputs, $Job_Frequency, $Job_Time, "CREATED", "INSERT", $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD, "NULL"))
		$insert_scheduled_job_table_output_parsed = $insert_scheduled_job_table_output.Column1
		log -Filename "$file_name" -Message "Response : $insert_scheduled_job_table_output_parsed"
		if ([String]::IsNullOrEmpty($insert_scheduled_job_table_output_parsed) -eq $true) {
			return @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
		} else {
			$NewBody.Job_Inputs | add-member -Name "scheduled_job_id" -value $insert_scheduled_job_table_output_parsed -MemberType NoteProperty
			$Job_Inputs = $NewBody.Job_Inputs | ConvertTo-Json
			$update_scheduled_job_table_output = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:InsertScheduledJob} -ArgumentList ($args + @($Job_Name, $Alert_Type, $Created_By, $Target_Servers, $Job_Script_File, $Job_Inputs, $Job_Frequency, $Job_Time, "CREATED", "UPDATE", $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD, $insert_scheduled_job_table_output_parsed))
			log -Filename "$file_name" -Message "Response : $update_scheduled_job_table_output"
			if ([String]::IsNullOrEmpty($update_scheduled_job_table_output) -eq $true) {
				return @{output = $true; Success_Message = $insert_scheduled_job_table_output_parsed} | ConvertTo-Json
			} else {
				return @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
			}
		}
	}
    log -Filename "$file_name" -Message "***********************************	Scheduled Job Data Insert/Update into Table Completed For -> Job : $Job_Name	***********************************"
}

# ---------------------------------------------------------------------------------------
# 								DB Backup History per date range Check Request
#	EndPoint	: db-backup-history
#	Method		: POST 
# ---------------------------------------------------------------------------------------
$GET_DB_BACKUP_HISTORY = New-UDEndpoint -Url "/db-backup-history-with-range" -Method "POST" -Endpoint {
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
	$sql_srv_instance = $NewBody.sql_srv_instance
	$db_name = $NewBody.db_name
	$start_date = $NewBody.start_date
	$end_date = $NewBody.end_date
	
	# --------------------- LOGGING --------------------
	$file_name = "SRE_AUTOMATION_DATABASE_BACKUP.log"
	log -Filename "$file_name" -Message "***********************************	DB Backup Request History Check Request Started	For -> Remote Server : $db_srv_instance and -> DB Name : $db_name  ***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"

	# ----------- Create Remote Session to DB Server/Jump Server -----------
	$remote_session = Create-PSSession -ServerName $db_srv_instance
	
    # ----------- Trigger Checking DB Backup Request History -----------
	log -Filename "$file_name" -Message "Request : Trigger Checking DB Backup Request History"
	
	if ($remote_session.Count -eq 1) {
		$db_backup_history = Invoke-Command -Session $remote_session -ScriptBlock ${function:GetDBBackupHistoryWithDateRange} -ArgumentList ($args + @($sql_srv_instance, $db_name, $start_date, $end_date))
	} elseif ($remote_session.Count -eq 2) {
		$Jumpy_session = $remote_session[0]
		$db_backup_history = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:GetDBBackupHistoryWithDateRange});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @($using:sql_srv_instance, $using:db_name, $using:start_date, $using:end_date))}
	}
	
	# ----------- Remove Remote Session/s -----------
	if ($remote_session.Count -eq 1) {
		Remove-PSSession $remote_session
	} elseif ($remote_session.Count -eq 2) {
		Invoke-Command -Session $Jumpy_session -ScriptBlock {Remove-PSSession $(Get-PSSession)}
		Remove-PSSession $Jumpy_session
	}
	
	log -Filename "$file_name" -Message "***********************************	DB Backup Request History Check Request Completed For -> Remote Server : $db_srv_instance and -> DB Name : $db_name  ***********************************"
	
	$processes_data = $db_backup_history | ConvertFrom-Json
	if ($processes_data.output -eq $true) {
		$backup_histrory = $processes_data.data | ConvertTo-Json
		log -Filename "$file_name" -Message "Response : $backup_histrory"
		return $backup_histrory
	} else {
		log -Filename "$file_name" -Message "Response : $db_backup_history"
		return $db_backup_history
	}
}

# ---------------------------------------------------------------------------------------
# 								SQL Instance Standard Config
#	EndPoint	: std-config
#	Method		: POST 
# ---------------------------------------------------------------------------------------
$SQL_INST_STD_CONFIG = New-UDEndpoint -Url "/std-config" -Method "POST" -Endpoint {
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
	$sql_srv_instance = $NewBody.sql_srv_instance
	
	# --------------------- LOGGING --------------------
	$file_name = "SRE_AUTOMATION_SQL_INSTANCE_STD_CONFIG.log"
	log -Filename "$file_name" -Message "***********************************	SQL Instance Standard Config Request Started For -> Remote Server : $db_srv_instance  ***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"

	# ----------- Create Remote Session to DB Server/Jump Server -----------
	$remote_session = Create-PSSession -ServerName $db_srv_instance
	
    # ----------- Trigger Checking DB Backup Request History -----------
	log -Filename "$file_name" -Message "Request : SQL Instance Standard Config"
	
	if ($remote_session.Count -eq 1) {
		$r1 = Invoke-Command -Session $remote_session -ScriptBlock ${function:DBMemory} -ArgumentList ($args + @("R1", $sql_srv_instance))
	} elseif ($remote_session.Count -eq 2) {
		$Jumpy_session = $remote_session[0]
		$r1 = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:DBMemory});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @("R1", $using:sql_srv_instance))}
	}
	$r1_display = $r1 | out-string
	log -Filename "$file_name" -Message "R1 Output:	$r1_display"
	# ----------- Remove Remote Session/s -----------
	if ($remote_session.Count -eq 1) {
		Remove-PSSession $remote_session
	} elseif ($remote_session.Count -eq 2) {
		Invoke-Command -Session $Jumpy_session -ScriptBlock {Remove-PSSession $(Get-PSSession)}
		Remove-PSSession $Jumpy_session
	}
	
	$r2 = Invoke-Command -ComputerName $Hostname -ScriptBlock ${function:DBMemory} -ArgumentList ($args + @("R2", "NULL", $VAR_SERVERINSTANCE, $VAR_DATABASE, $VAR_USERNAME, $VAR_PASSWORD))
	$r2_display = $r2 | Out-string
	log -Filename "$file_name" -Message "R2 Output:	$r2_display"
	
	#Refer this: https://devblogs.microsoft.com/powershell/joining-multiple-tables-grouping-and-evaluating-totals/
	#https://stackoverflow.com/questions/1848821/in-powershell-whats-the-best-way-to-join-two-tables-into-one
	#$FinalContent = $r1 | Join-Object Inner $r2 -On configuration_id -Equals configid | Select-Object configuration_id, name, value, maximum, value_in_use, description, configid, ConfigClass, IsShown, IsChangeable
	$FinalContent = $r1 | Join-Object $r2 -On configuration_id -Equals ConfigId | Select-Object configuration_id, name, value, maximum, value_in_use, description, ConfigId, ConfigClass, IsShown, IsChangeable
	
	log -Filename "$file_name" -Message "$FinalContent"
	log -Filename "$file_name" -Message "***********************************	SQL Instance Standard Config Request Completed For -> Remote Server : $db_srv_instance  ***********************************"
	
	return @{StandardConfigData=$FinalContent} | ConvertTo-Json
}

# -------------------------------------------------------------------------------------
# 								Trigger Rest Api Server 
# -------------------------------------------------------------------------------------
$Endpoints = $GET_BACKUP_REQUEST_PROGRESS, $CREATE_DATABASE_BACKUP, $GET_DBS_SIZE_LASTBKP_DATA, $GET_BACKUP_REQUEST_CANCEL, $SCHEDULE_JOB_INSERT_API, $GET_DB_BACKUP_HISTORY, $SQL_INST_STD_CONFIG
Start-UDRestApi -Name Powershell-RestCalls-DB-Backup -Port 8809 -Endpoint $Endpoints -Wait

# Get-UDRestApi -Name Powershell-RestCalls-DB-Backup | Stop-UDRestApi