
UPDATE [automation].[Server]
set [automation].[Server].IsClustered = M.[IsClustered],
[automation].[Server].PhysicalServer = M.[PhysicalServer] 
FROM [automation].[Server] S
INNER JOIN [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].[vInstancesList] M
ON S.ServerName  = M.[ServerName];

