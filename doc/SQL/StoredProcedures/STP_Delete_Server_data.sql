CREATE PROCEDURE automation.Delete_server_Data
AS
    SET NOCOUNT ON;
DECLARE
 	@DELETED_STATUS int

 	SELECT @DELETED_STATUS = s.id  from automation.GeneralStatus s WHERE s.StatusName = 'Deleted'

	UPDATE automation.[server]
	SET StatusId = @DELETED_STATUS
	WHERE Id in (
		SELECT s.id
		FROM automation.[server] s
		WHERE s.ServerName not in (
			SELECT DISTINCT ServerName
			FROM [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].vInstancesList
		)
	)
