CREATE PROCEDURE automation.Import_Server_Data
AS
    SET NOCOUNT ON;

   DECLARE
 	@ACTIVE_STATUS VARCHAR(64)

 	SELECT @ACTIVE_STATUS = s.LookupCode  from automation.LookupMaster s WHERE s.LookupType =  'GENERAL_STATUS' AND s.LookupCode = 'ACTIVE'

-- step Insert new servers


	INSERT INTO [automation].[server] (ServerName, DomainName , RegionName , Description, StatusId)
    SELECT DISTINCT SRC.ServerName, [Domain], [Location] , SRC.Description, @ACTIVE_STATUS
    FROM [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].[vInstancesList] as SRC
	WHERE SRC.[ServerName] not in (
		SELECT [automation].[server].[servername]
		FROM [automation].[server] )
	AND SRC.[Domain] IS NOT NULL
	AND SRC.[Location] IS NOT NULL
	AND SRC.AlwaysOnRole IS NOT NULL
	ORDER BY 1 DESC
