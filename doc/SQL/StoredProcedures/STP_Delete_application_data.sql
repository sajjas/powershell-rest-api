CREATE PROCEDURE automation.Delete_Application_Data
AS
    SET NOCOUNT ON;
DECLARE
 	@DELETED_STATUS int

 	SELECT @DELETED_STATUS = s.id  from automation.GeneralStatus s WHERE s.StatusName = 'Deleted'

	UPDATE automation.[application]
	SET StatusId = @DELETED_STATUS
	WHERE Id in (
		SELECT app.id
		FROM automation.[application] app
		WHERE app.ApplicationName not in (
			SELECT DISTINCT AppName
			FROM [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].vInstancesList
		)
	)