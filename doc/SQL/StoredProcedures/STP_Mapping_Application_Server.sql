CREATE PROCEDURE automation.Mapping_Application_server
AS
    SET NOCOUNT ON;

   DECLARE
 	@ACTIVE_STATUS int

 	SELECT @ACTIVE_STATUS = s.id from automation.LookupMaster s WHERE s.LookupCode = 'ACTIVE' AND s.LookupType = 'GENERAL_STATUS'

    -- step 1 Applications and Server Mapping
    INSERT INTO [automation].[ServerMapping] (ApplicationId, ServerId,  Description,CreationDate)
    SELECT distinct [automation].[application].[id] as ApplicationId, [automation].[server].[id] as ServerId, INV.AppName + ' have ' + INV.ServerName, getdate()
    FROM [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].[vInstancesList]  as INV
    INNER JOIN [automation].[application] ON  [automation].[application].[ApplicationName] = INV.AppName
    INNER JOIN [automation].[server] ON  [automation].[server].[ServerName] = INV.ServerName
