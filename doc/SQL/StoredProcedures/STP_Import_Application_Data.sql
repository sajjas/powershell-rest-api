CREATE PROCEDURE automation.Import_Application_Data
AS
    SET NOCOUNT ON;
	-- step 1 Insert new Applications
 DECLARE
 	@ACTIVE_STATUS int

 	SELECT @ACTIVE_STATUS = s.id from automation.LookupMaster s WHERE s.LookupCode = 'ACTIVE' AND s.LookupType = 'GENERAL_STATUS'

    INSERT INTO automation.application (ApplicationName, Manageable, Description, CreationDate, StatusId  )
        SELECT   DISTINCT AppName, 1, 'Description for '+AppName, getdate(), @ACTIVE_STATUS
        FROM [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].[vInstancesList] as SRC
        WHERE SRC.[AppName] not in (
            SELECT [automation].[application].[ApplicationName]
            FROM [automation].[application]
        ) ORDER BY 1 DESC

