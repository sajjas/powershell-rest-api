CREATE PROCEDURE automation.Import_Instance_Data
AS
    SET NOCOUNT ON;

   DECLARE
 	@ACTIVE_STATUS VARCHAR(64)

 	SELECT @ACTIVE_STATUS = s.LookupCode  from automation.LookupMaster s WHERE s.LookupType =  'GENERAL_STATUS' AND s.LookupCode = 'ACTIVE'
	INSERT INTO [automation].[instance] (InstanceName, Description, ServerId, StatusId, CreationDate)
    SELECT DISTINCT InstanceName, 'Description for' +InstanceName, SRV.Id , @ACTIVE_STATUS , getdate()
    FROM [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].[vInstancesList] as SRC,
    	 [automation].[server] SRV
	WHERE SRC.[instanceName] not in (
		SELECT [automation].[instance].[instanceName]
		FROM [automation].[instance]
	)
	AND SRC.ServerName = SRV.ServerName
	AND SRC.Type = 'SQL'
	ORDER BY 1 DESC

