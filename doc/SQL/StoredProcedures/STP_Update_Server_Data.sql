CREATE PROCEDURE automation.Update_Server_Data
AS
SET NOCOUNT ON;

DECLARE
 	 @server_name nvarchar(50), @message varchar(80),
 	 @DomainSource varchar(250),
 	 @DomainTarget nvarchar(250),
 	 @RegionSource varchar(250),
 	 @RegionTarget nvarchar(250)



DECLARE server_cursor CURSOR FOR
SELECT ServerName FROM target.server

OPEN server_cursor

FETCH NEXT FROM server_cursor
INTO @server_name

-- Iterar Cursor
WHILE @@FETCH_STATUS = 0
BEGIN
    SELECT @message = '----- Server Name: ' +  @server_name
    PRINT @message
-- ------------------------------------------------------------------
---   				 DOMAIN
-- ------------------------------------------------------------------
-- Traer domain del Source para el server
	SELECT  @DomainSource = [Domain]
	FROM backupinventory.[source].vInstancesList
	WHERE ServerName = @server_name
 -- Traer domain del target para el server
	SELECT  @DomainTarget = [DomainName]
	FROM backupinventory.[target].server
	WHERE ServerName = @server_name

    PRINT '----- Domain Name Source: ' + @DomainSource
    PRINT '----- Domain Name Target: ' + @DomainTarget
-- comparar domain source con domain target si son distintos then Update
    IF @DomainSource COLLATE Latin1_General_CS_AS != @DomainTarget COLLATE Latin1_General_CS_AS
    	UPDATE backupinventory.[target].server
    	SET DomainName = @DomainSource
    	WHERE ServerName = @server_name
-- ------------------------------------------------------------------
---    				REGION
-- ------------------------------------------------------------------
-- Traer domain del Source para el server
	SELECT  @RegionSource = [Location]
	FROM backupinventory.[source].vInstancesList
	WHERE ServerName = @server_name
 -- Traer domain del target para el server
	SELECT  @RegionTarget = [RegionName]
	FROM backupinventory.[target].server
	WHERE ServerName = @server_name

    PRINT '----- Region Name Source: ' + @RegionSource
    PRINT '----- Region Name Target: ' + @RegionTarget
-- comparar domain source con domain target si son distintos then Update
    IF @RegionSource COLLATE Latin1_General_CS_AS  != @RegionTarget COLLATE Latin1_General_CS_AS
    	UPDATE backupinventory.[target].server
    	SET RegionName = @RegionSource
    	WHERE ServerName = @server_name

-- Se sigue con el siguiente record
        -- Get the next vendor.
	FETCH NEXT FROM server_cursor
	INTO @server_name
END
CLOSE server_cursor;
DEALLOCATE server_cursor;