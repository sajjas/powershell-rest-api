--SELECT * FROM  sys.triggers WHERE type = 'TR';
--SELECT table_name FROM information_schema.tables WHERE table_type = 'base table' AND table_schema='dbo';
--SELECT @@VERSION AS 'Container_version';
/*


DROP TABLE automation.ApplicationMapping
DROP TABLE automation.DatabaseMapping
DROP TABLE automation.GroupMapping
DROP TABLE automation.Event
DROP TABLE automation.NASShare
DROP TABLE automation.Process
DROP TABLE automation.ProcessStatus
DROP TABLE automation.Entity
DROP TABLE automation.Application
DROP TABLE automation.Instance
DROP TABLE automation.Data_Base
DROP TABLE automation.Server
DROP TABLE automation.[Group]
DROP TABLE automation.[User]
DROP TABLE automation.GeneralStatus
DROP TABLE automation.Domain
DROP TABLE automation.Region


*/

----------------------------------------------------------------------------------------
--                               MASTER TABLES
----------------------------------------------------------------------------------------
create TABLE automation.[AlertType]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [TypeName] NVARCHAR(250) NOT NULL
)

GO

create TABLE automation.[Alert]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [TypeId] INT NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    [StatusId] INT NOT NULL,
    [Info] NVARCHAR(500),
    [Last_Modified_Date] DATETIME,
    CONSTRAINT FK_GENERAL_STATUS_ALERT FOREIGN KEY (StatusId) REFERENCES automation.ProcessStatus (Id),
    CONSTRAINT FK_TYPE_ALERT FOREIGN KEY (TypeId) REFERENCES automation.AlertType (Id)

)


GO

create TABLE automation.[ServiceRequestType]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [TypeName] NVARCHAR(250) NOT NULL
)

GO

create TABLE automation.[ServiceRequest]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [TypeId] INT NOT NULL,
    [StatusId] INT NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    [AlertId] INT,
    [Info] NVARCHAR(500),
    CONSTRAINT FK_GENERAL_STATUS_SERVICEREQUEST FOREIGN KEY (StatusId) REFERENCES automation.ProcessStatus (Id),
    CONSTRAINT FK_TYPE_SERVICEREQUEST FOREIGN KEY (TypeId) REFERENCES automation.ServiceRequestType (Id)
);

GO

create TABLE automation.Entity
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [EntityName] NVARCHAR(50) NOT NULL,
    [Description] NVARCHAR(250) NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (EntityName)
);

GO

create TABLE automation.GeneralStatus
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [StatusName] NVARCHAR(20) NOT NULL,
    [Description] NVARCHAR(250) NOT NULL,
    UNIQUE (StatusName)
);

GO

create TABLE automation.ProcessStatus
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [StatusName] NVARCHAR(20) NOT NULL,
    [Description] NVARCHAR(250) NOT NULL,
    UNIQUE (StatusName)
);

GO

create TABLE automation.[Domain]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [DomainName] NVARCHAR(50) NOT NULL,
    [Description] NVARCHAR(50) NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (DomainName)
)

GO

create TABLE automation.Region
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [RegionName] NVARCHAR(50) NOT NULL,
    [Description] NVARCHAR(50) NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (RegionName)
)

GO

create TABLE automation.Data_Base
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [DatabaseName] NVARCHAR(250) NOT NULL,
    [Description] NVARCHAR(250) NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (DatabaseName)
);

GO


----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
create TABLE automation.dbbkp
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [db_srv_instance] NVARCHAR(50) NOT NULL,
    [sql_srv_instance] NVARCHAR(50) NOT NULL,
    [db_name] INT NOT NULL,
    [backup_type_name] INT NOT NULL DEFAULT 1,
    [nas_drive_path] INT NOT NULL,
    [nas_drive_region] INT NOT NULL ,
    [process_id] INT NOT NULL ,
    [server_id] INT NOT NULL ,
    [user_id] INT NOT NULL ,
    [process_parameters] NVARCHAR(250)  NOT NULL ,
    [service_now_incident_id] NVARCHAR(250)  NOT NULL ,
    [process_status_id] INT NOT NULL ,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT FK_DBBKP_BACKUP_TYPE0 FOREIGN KEY (backup_type_name) REFERENCES automation.backup_type (Id),
    CONSTRAINT FK_DBBKP_BACKUP_TYPE2 FOREIGN KEY (nas_drive_path)   REFERENCES automation.NASShare (Id),
    CONSTRAINT FK_DBBKP_BACKUP_TYPE3 FOREIGN KEY (nas_drive_region) REFERENCES automation.Region (Id),
    CONSTRAINT FK_DBBKP_BACKUP_TYPE4 FOREIGN KEY (process_id) REFERENCES automation.Process (Id),
    CONSTRAINT FK_DBBKP_BACKUP_TYPE5 FOREIGN KEY (server_id) REFERENCES automation.Server (Id),
    CONSTRAINT FK_DBBKP_BACKUP_TYPE6 FOREIGN KEY (user_id) REFERENCES automation.[User] (Id),
    CONSTRAINT FK_DBBKP_BACKUP_TYPE7 FOREIGN KEY (process_status_id) REFERENCES automation.ProcessStatus (Id),
    CONSTRAINT FK_DBBKP_BACKUP_TYPE8 FOREIGN KEY (db_name) REFERENCES automation.[data_base] (Id)
);

GO

create TABLE automation.backup_type
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [backup_type_name] NVARCHAR(50) NOT NULL,
    UNIQUE(backup_type_name)
}

GO

----------------------------------------------------------------------------------------
--                               DEPENDENT TABLES
----------------------------------------------------------------------------------------

GO


create TABLE automation.Application
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [ApplicationName] NVARCHAR(250) NOT NULL,
    [Manageable] bit DEFAULT 1 NOT NULL,
    [StatusId] INT NOT NULL,
    [Description] NVARCHAR(250) NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (ApplicationName),
    CONSTRAINT FK_GENERAL_STATUS_APPLICATION FOREIGN KEY (StatusId) REFERENCES automation.GeneralStatus (Id)
);

GO

create TABLE automation.NASShare
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [NASShareName]  NVARCHAR(50) NOT NULL,
    [RegionId] INT NOT NULL,
    [BackupPath] NVARCHAR(500) NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (NASShareName),
    CONSTRAINT FK_REGION_NASSHARE FOREIGN KEY (RegionId) REFERENCES automation.Region (Id)
);

GO

create TABLE automation.Server
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [ServerName] NVARCHAR(50) NOT NULL,
    [Description] NVARCHAR(250) NOT NULL,
    [DomainId] INT NOT NULL,
    [RegionId] INT NOT NULL,
    [StatusId] INT NOT NULL,
    [CreationDate] DATETIME,
    [Last_Modified_Date] DATETIME,
    UNIQUE(ServerName),
    CONSTRAINT FK_GENERAL_STATUS_SERVER FOREIGN KEY (StatusId) REFERENCES automation.GeneralStatus (Id),
    CONSTRAINT FK_DOMAIN_SERVER FOREIGN KEY (DomainId) REFERENCES automation.[Domain] (Id),
    CONSTRAINT FK_REGION_SERVER FOREIGN KEY (RegionId) REFERENCES automation.Region (Id)
);

GO

create TABLE automation.Instance
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY ,
    [InstanceName] NVARCHAR(50) NOT NULL,
    [Description] NVARCHAR(250) NOT NULL,
    [ServerId] INT NOT NULL,
    [DatabaseId] INT NOT NULL,
    [StatusId] INT NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (InstanceName,ServerId,DatabaseId),
    CONSTRAINT FK_SERVER_NAME_INSTANCE FOREIGN KEY (ServerId) REFERENCES automation.server (Id),
    CONSTRAINT FK_DATA_BASE_NAME_INSTANCE FOREIGN KEY (DataBaseId) REFERENCES automation.Data_Base (Id),
    CONSTRAINT FK_GENERALSTATUS_INSTANCE FOREIGN KEY (StatusId) REFERENCES automation.GeneralStatus (Id)
);

GO

create TABLE automation.DatabaseMapping
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [ApplicationId] INT NOT NULL,
    [DatabaseId] INT NOT NULL,
    [Description] NVARCHAR(250) NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (ApplicationId,Dahttps://us04web.zoom.us/j/519767977tabaseId),
    CONSTRAINT FK_APPLICATION_NAME_DATABASEMAPPING FOREIGN KEY (ApplicationId) REFERENCES automation.Application (Id),
    CONSTRAINT FK_DATA_BASE_NAME_DATABASEMAPPING FOREIGN KEY (DatabaseId) REFERENCES automation.Data_Base (Id)
);

GO

create TABLE automation.[User]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [UserName] NVARCHAR(50) NOT NULL,
    [Description] NVARCHAR(250) NOT NULL,
    [Email] NVARCHAR(50) NOT NULL,
    [StatusId] INT NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (UserName),
    CONSTRAINT FK_GENERAL_STATUS_USER FOREIGN KEY (StatusId) REFERENCES automation.GeneralStatus (Id)
);

GO

create TABLE automation.[Group]
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [GroupName] NVARCHAR(50) NOT NULL,
    [Description] NVARCHAR(250) NOT NULL,
    [EmailGroup] NVARCHAR(50) NOT NULL,
    [StatusId] INT NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (GroupName),
    CONSTRAINT FK_GENERAL_STATUS_GROUP FOREIGN KEY (StatusId) REFERENCES automation.GeneralStatus (Id)
);

GO

create TABLE automation.GroupMapping
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [GroupId] INT NOT NULL,
    [UserId] INT NOT NULL,
    [StatusId] INT NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (GroupId,UserId),
    CONSTRAINT FK_GROUP_NAME_GROUPMAPPING FOREIGN KEY (GroupId) REFERENCES automation.[Group] (Id),
    CONSTRAINT FK_USER_NAME_GROUPMAPPING FOREIGN KEY (UserId) REFERENCES automation.[User] (Id),
    CONSTRAINT FK_GENERAL_STATUS_GROUPMAPPING FOREIGN KEY (StatusId) REFERENCES automation.GeneralStatus (Id)
);

GO
create TABLE automation.ApplicationMapping
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY ,
    [GroupId] INT NOT NULL,
    [ApplicationId] INT NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (GroupId,ApplicationId),
    CONSTRAINT FK_GROUP_NAME_APPLICATIONMAPPING FOREIGN KEY (GroupId) REFERENCES automation.[Group] (Id),
    CONSTRAINT FK_APPLICATION_NAME_APPLICATIONMAPPING FOREIGN KEY (ApplicationId) REFERENCES automation.Application (Id)
);

GO

create TABLE automation.Process
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY ,
    [ProcessName] NVARCHAR(50) NOT NULL,
    [EntityId] INT NOT NULL,
    [Action] NVARCHAR(250) NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT FK_PROCESS_ENTITY FOREIGN KEY (EntityId) REFERENCES automation.Entity (Id)
);

GO

create TABLE automation.Event
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [ProcessId] INT NOT NULL,
    [ServerId] INT NOT NULL,
    [UserId] INT NOT NULL,
    [DateTimeStarted] DateTime NOT NULL,
    [DateTimeCompleted] DateTime NOT NULL,
    [ProcessParameters] NVARCHAR(500) NOT NULL,
    [ServiceNowIncidentId] NVARCHAR(20) NOT NULL,
    [ProcessStatusId] INT NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT FK_PROCESS FOREIGN KEY (ProcessId) REFERENCES automation.process (Id),
    CONSTRAINT FK_USER_NAME_AUTH_EVENT FOREIGN KEY (UserId) REFERENCES automation.[User] (Id),
    CONSTRAINT FK_SERVER_NAME_EVENT FOREIGN KEY (ServerId) REFERENCES automation.server (Id),
    CONSTRAINT FK_GENERAL_STATUS_EVENT FOREIGN KEY (ProcessStatusId) REFERENCES automation.ProcessStatus (Id)
);

GO

create TABLE automation.ProcessGroupMapping
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY ,
    [GroupId] INT NOT NULL,
    [ProcessId] INT NOT NULL,
    [CreationDate] DATETIME DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (GroupId,ProcessId),
    CONSTRAINT FK_GROUP_NAME_ProcessGroupMapping FOREIGN KEY (GroupId) REFERENCES automation.[Group] (Id),
    CONSTRAINT FK_PROCESSID_ProcessGroupMapping FOREIGN KEY (ProcessId) REFERENCES automation.process (Id)
);

-- --------------------------------------------------------------------------------------------------------------

create trigger TRG_BU_Server
ON automation.Server
AFTER UPDATE
AS
  begin
    update automation.Server
    set Last_Modified_Date = GETDATE()
    where ServerName in (select ServerName from Inserted)
  end;

-- --------------------------------------------------------------------------------------------------------------

create trigger TRG_AI_Server
ON  automation.Server
AFTER INSERT
AS
begin
    set NOCOUNT ON;
    update automation.Server
    set CreationDate = GETDATE(),
        Last_Modified_Date = GETDATE()
    FROM inserted INNER JOIN automation.Server On inserted.ServerName = automation.Server.ServerName
end



