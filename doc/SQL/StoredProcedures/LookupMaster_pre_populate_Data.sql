INSERT INTO backupinventory.automation.LookupMaster (LookupType,LookupCode,LookupName,CreationDate,IsDeleted) VALUES 
('USER_STATUS','INACTIVE','Inactive User','2020-04-14 10:14:55.860',0)
,('USER_STATUS','ACTIVE','Active User','2020-04-14 10:15:07.393',0)
,('ALERT_STATUS','CREATED','Alert Created','2020-04-14 10:15:41.720',0)
,('ALERT_STATUS','SCHEDULED','Alert Scheduled','2020-04-14 10:15:58.330',0)
,('ALERT_STATUS','COMPLETED','Alert Completed','2020-04-14 10:16:19.080',0)
,('ALERT_STATUS','CLUSTER_UNBALENCE','Alert type Cluster Unbalence','2020-04-14 10:17:35.160',0)
,('DB_BACKUP','INITIATED','Db Backup Initiated','2020-04-14 10:17:57.190',0)
,('DB_BACKUP','CANCELLED','Db Backup Action Cancelled','2020-04-14 10:18:22.207',0)
,('DB_BACKUP','ERROR','Db Backup Error','2020-04-14 10:18:44.097',0)
,('DB_BACKUP','COMPLETED','Db Backup Completed','2020-04-14 10:18:53.610',0)
;
INSERT INTO backupinventory.automation.LookupMaster (LookupType,LookupCode,LookupName,CreationDate,IsDeleted) VALUES 
('DB_BACKUP','INPROGRESS','Db Backup InProgress','2020-04-14 10:39:41.743',0)
,('GENERAL_STATUS','ACTIVE','General Status Active State','2020-04-14 10:39:53.637',0)
,('GENERAL_STATUS','INACTIVE','General Status Inactive State','2020-04-14 11:32:25.980',0)
,('PROCESS_STATUS','PROCESS_COMPLETED','process completed state','2020-04-14 11:32:49.760',0)
,('PROCESS_STATUS','PROCESS_FAILED','process failed state','2020-04-14 11:33:45.917',0)
,('PROCESS_STATUS','PROCESS_NEW','process new state','2020-04-14 11:34:14.943',0)
,('PROCESS_STATUS','PROCESS_CLOSE','process close state','2020-04-14 11:34:46.210',0)
,('PROCESS_STATUS','PROCESS_OPEN','process open state','2020-04-14 11:35:23.027',0)
,('PROCESS_STATUS','PROCESS_RUNNING','process running state','2020-04-14 11:32:25.000',0)
,('ALERT_TYPE','CLUSTER','XXXXXXXXX','2020-04-14 11:32:25.000',0)
;
INSERT INTO backupinventory.automation.LookupMaster (LookupType,LookupCode,LookupName,CreationDate,IsDeleted) VALUES 
('SCH_FREQ','DAILY','Schedule for Daily','2020-04-14 11:32:25.000',0)
,('SCH_FREQ','WEEKLY','Schedule for Weekly','2020-04-14 11:32:25.000',0)
,('SCH_FREQ','MONTHLY','Schedule for Monthly','2020-04-14 11:32:25.000',0)
,('SCH_JOB_STATUS','CREATED','XXXXXXXXXX','2020-04-14 11:32:25.000',0)
,('SCH_JOB_STATUS','ACTIVE','XXXXXXXXXX','2020-04-14 11:32:25.000',0)
,('SCH_JOB_STATUS','INACTIVE','XXXXXXXXXX','2020-04-14 11:32:25.000',0)
;