CREATE PROCEDURE automation.Delete_instance_Data
AS
    SET NOCOUNT ON;
DECLARE
 	@DELETED_STATUS int

 	SELECT @DELETED_STATUS = s.id  from automation.GeneralStatus s WHERE s.StatusName = 'Deleted'

	UPDATE automation.[instance]
	SET StatusId = @DELETED_STATUS
	WHERE Id in (
		SELECT ins.id
		FROM automation.[instance] ins
		WHERE ins.InstanceName not in (
			SELECT DISTINCT InstanceName
			FROM [USSECVMPDBTSQ01.EY.NET].[DBA_CentralDB].[dbo].vInstancesList
		)
	)
