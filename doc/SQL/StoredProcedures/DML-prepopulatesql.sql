/*
DELETE FROM backupinventory.dbo.Application_Ey;
DELETE FROM backupinventory.dbo.Application_Ey_Db;
DELETE FROM backupinventory.dbo.Auth_Group_Application;
DELETE FROM backupinventory.dbo.Auth_Group_User;
DELETE FROM backupinventory.dbo.Auth_User;
DELETE FROM backupinventory.dbo.Data_Base;


DELETE FROM backupinventory.dbo.Server;
DELETE FROM backupinventory.dbo.Process_Status;
DELETE FROM backupinventory.dbo.Process;
DELETE FROM backupinventory.dbo.[Instance];
DELETE FROM backupinventory.dbo.Event;
DELETE FROM backupinventory.dbo.Auth_Group;
DELETE FROM backupinventory.dbo.General_Status;
*/


INSERT INTO backupinventory.dbo.Entity (EntityId, Description, CreationDate) VALUES('SERVER', 'Define Server Type', getdate());
INSERT INTO backupinventory.dbo.Entity (EntityId, Description, CreationDate) VALUES('DATABASE', 'Define Database Type', getdate());
INSERT INTO backupinventory.dbo.Entity (EntityId, Description, CreationDate) VALUES('INSTANCE', 'Define INSTANCE Type', getdate());


INSERT INTO backupinventory.dbo.Region (RegionName, Description) VALUES('Region 111', 'Some Region 111');
INSERT INTO backupinventory.dbo.Region (RegionName, Description) VALUES('Region 222', 'Some Region 222');
INSERT INTO backupinventory.dbo.Region (RegionName, Description) VALUES('Region 333', 'Some Region 333');
INSERT INTO backupinventory.dbo.Region (RegionName, Description) VALUES('Region 444', 'Some Region 444');
INSERT INTO backupinventory.dbo.Region (RegionName, Description) VALUES('Region 555', 'Some Region 555');

INSERT INTO backupinventory.dbo.[Domain] (DomainName, Description) VALUES('Domain 777', 'Domain Description 777');
INSERT INTO backupinventory.dbo.[Domain] (DomainName, Description) VALUES('Domain 888', 'Domain Description 888');
INSERT INTO backupinventory.dbo.[Domain] (DomainName, Description) VALUES('Domain 999', 'Domain Description 999');
INSERT INTO backupinventory.dbo.[Domain] (DomainName, Description) VALUES('Domain 000', 'Domain Description 000');
INSERT INTO backupinventory.dbo.[Domain] (DomainName, Description) VALUES('Domain 555', 'Domain Description 555');

INSERT INTO backupinventory.dbo.NASShare (RegionName, BackupPath, CreationDate) VALUES('Region 111', '/PATH/To/Some/Share1', getdate());
INSERT INTO backupinventory.dbo.NASShare (RegionName, BackupPath, CreationDate) VALUES('Region 222', '/PATH/To/Some/Share2', getdate());
INSERT INTO backupinventory.dbo.NASShare (RegionName, BackupPath, CreationDate) VALUES('Region 333', '/PATH/To/Some/Share3', getdate());
INSERT INTO backupinventory.dbo.NASShare (RegionName, BackupPath, CreationDate) VALUES('Region 444', '/PATH/To/Some/Share4', getdate());
INSERT INTO backupinventory.dbo.NASShare (RegionName, BackupPath, CreationDate) VALUES('Region 555', '/PATH/To/Some/Share5', getdate());





INSERT INTO backupinventory.dbo.GeneralStatus (StatusId, Description, CreationDate)
VALUES('ACTIVE', 'Define an ACTIVE Status', getdate());

INSERT INTO backupinventory.dbo.GeneralStatus (StatusId, Description, CreationDate)
VALUES('INACTIVE', 'Define an INACTIVE Status', getdate());

INSERT INTO backupinventory.dbo.GeneralStatus (StatusId, Description, CreationDate)
VALUES('RETIRED', 'Define a Retired Server Status for logical deletion', getdate());

INSERT INTO backupinventory.dbo.ProcessStatus (StatusId, Description, CreationDate)
VALUES('RUNNING', 'Define RUNNING Status for process', getdate());

INSERT INTO backupinventory.dbo.ProcessStatus (StatusId, Description, CreationDate)
VALUES('COMPLETED', 'Define COMPLETED Status for process', getdate());

INSERT INTO backupinventory.dbo.ProcessStatus (StatusId, Description, CreationDate)
VALUES('FAILED', 'Define FAILED Status for process', getdate());


INSERT INTO backupinventory.dbo.SERVER (ServerName,Description,StatusId,CreationDate,domainName,RegionName) VALUES
('fakeserver123.ey.com','fake test server','ACTIVE',getdate(),'Domain 555','Region 111');
INSERT INTO backupinventory.dbo.SERVER (ServerName,Description,StatusId,CreationDate,domainName,RegionName) VALUES
('fakeserver456.ey.com','fake test 456 server','ACTIVE',getdate(),'Domain 000','Region 333');
INSERT INTO backupinventory.dbo.SERVER (ServerName,Description,StatusId,CreationDate,domainName,RegionName) VALUES
('fakeserver789.ey.com','fake test 789 server','ACTIVE',getdate(),'Domain 999','Region 222');
INSERT INTO backupinventory.dbo.SERVER (ServerName,Description,StatusId,CreationDate,domainName,RegionName) VALUES
('fakeserverABC.ey.com','fake test ABC server','ACTIVE',getdate(),'Domain 888','Region 444');
INSERT INTO backupinventory.dbo.SERVER (ServerName,Description,StatusId,CreationDate,domainName,RegionName) VALUES
('fakeserverXYZ.ey.com','fake test XYZ server','ACTIVE',getdate(),'Domain 777','Region 555');


INSERT INTO backupinventory.dbo.DATA_BASE (DatabaseName,Description,CreationDate) VALUES 
('SQL INST 1','Instance 1 - node 1',getdate());
INSERT INTO backupinventory.dbo.DATA_BASE (DatabaseName,Description,CreationDate) VALUES 
('SQL INST 2','Instance 2 - node 2',getdate());
INSERT INTO backupinventory.dbo.DATA_BASE (DatabaseName,Description,CreationDate) VALUES 
('SQL INST 3','Instance 3 - node 1',getdate());
INSERT INTO backupinventory.dbo.DATA_BASE (DatabaseName,Description,CreationDate) VALUES 
('SQL INST 4','Instance 4 - node 2',getdate());
INSERT INTO backupinventory.dbo.DATA_BASE (DatabaseName,Description,CreationDate) VALUES 
('SQL INST 5','Instance 5 - node 1',getdate());


INSERT INTO backupinventory.dbo.APPLICATION (ApplicationName,Description,CreationDate) VALUES
('ServiceNow','Service Now application',getdate());
INSERT INTO backupinventory.dbo.APPLICATION (ApplicationName,Description,CreationDate) VALUES
('SQL DBA Tool','SQL DBA Tool',getdate());
INSERT INTO backupinventory.dbo.APPLICATION (ApplicationName,Description,CreationDate) VALUES
('Azure Automation','Azure Automation',getdate());
INSERT INTO backupinventory.dbo.APPLICATION (ApplicationName,Description,CreationDate) VALUES
('Human Resources Services','Human Resources Servicesn',getdate());
INSERT INTO backupinventory.dbo.APPLICATION (ApplicationName,Description,CreationDate) VALUES
('some amazon services','some amazon services',getdate());


INSERT INTO backupinventory.dbo.[USER] (UserName,Description,StatusId,CreationDate,email) VALUES
('fakeuser01','user testing','ACTIVE',getdate(),'fakeuser01@ey.com');
INSERT INTO backupinventory.dbo.[USER] (UserName,Description,StatusId,CreationDate,email) VALUES
('fakeuser02','user testing','ACTIVE',getdate(),'fakeuser02@ey.com');
INSERT INTO backupinventory.dbo.[USER] (UserName,Description,StatusId,CreationDate,email) VALUES
('fakeuser03','user testing','ACTIVE',getdate(),'fakeuser03@ey.com');
INSERT INTO backupinventory.dbo.[USER] (UserName,Description,StatusId,CreationDate,email) VALUES
('fakeuser04','user testing','ACTIVE',getdate(),'fakeuser04@ey.com');
INSERT INTO backupinventory.dbo.[USER] (UserName,Description,StatusId,CreationDate,email) VALUES
('fakeuser05','user testing','ACTIVE',getdate(),'fakeuser05@ey.com');


INSERT INTO backupinventory.dbo.[GROUP] (GroupName,Description,StatusId,CreationDate,EmailGroup) VALUES
('fakegroup01','TEsting group','ACTIVE',getdate(),'fakegroup01@ey.com');
INSERT INTO backupinventory.dbo.[GROUP] (GroupName,Description,StatusId,CreationDate,EmailGroup) VALUES
('fakegroup02','TEsting group','ACTIVE',getdate(),'fakegroup02@ey.com');
INSERT INTO backupinventory.dbo.[GROUP] (GroupName,Description,StatusId,CreationDate,EmailGroup) VALUES
('fakegroup03','TEsting group','ACTIVE',getdate(),'fakegroup03@ey.com');
INSERT INTO backupinventory.dbo.[GROUP] (GroupName,Description,StatusId,CreationDate,EmailGroup) VALUES
('fakegroup04','TEsting group','ACTIVE',getdate(),'fakegroup04@ey.com');
INSERT INTO backupinventory.dbo.[GROUP] (GroupName,Description,StatusId,CreationDate,EmailGroup) VALUES
('fakegroup05','TEsting group','ACTIVE',getdate(),'fakegroup05@ey.com');


INSERT INTO backupinventory.dbo.[INSTANCE] (InstanceName,ServerName,DatabaseName,Description,StatusId,CreationDate) VALUES 
('SQL INST 1 Node 1','fakeserver123.ey.com','SQL INST 1','Test data for views','ACTIVE',getdate());
INSERT INTO backupinventory.dbo.[INSTANCE] (InstanceName,ServerName,DatabaseName,Description,StatusId,CreationDate) VALUES 
('SQL INST 2 Node 1','fakeserver456.ey.com','SQL INST 2','Test data for views','ACTIVE',getdate());
INSERT INTO backupinventory.dbo.[INSTANCE] (InstanceName,ServerName,DatabaseName,Description,StatusId,CreationDate) VALUES 
('SQL INST 3 Node 1','fakeserver789.ey.com','SQL INST 3','Test data for views','ACTIVE',getdate());
INSERT INTO backupinventory.dbo.[INSTANCE] (InstanceName,ServerName,DatabaseName,Description,StatusId,CreationDate) VALUES 
('SQL INST 4 Node 1','fakeserverABC.ey.com','SQL INST 4','Test data for views','ACTIVE',getdate());
INSERT INTO backupinventory.dbo.[INSTANCE] (InstanceName,ServerName,DatabaseName,Description,StatusId,CreationDate) VALUES 
('SQL INST 5 Node 1','fakeserverXYZ.ey.com','SQL INST 5','Test data for views','ACTIVE',getdate());


INSERT INTO backupinventory.dbo.DatabaseMapping (ApplicationName,DatabaseName,Description,CreationDate) VALUES
('serviceNow','SQL INST 1','testing relation app + database',getdate());
INSERT INTO backupinventory.dbo.DatabaseMapping (ApplicationName,DatabaseName,Description,CreationDate) VALUES
('SQL DBA Tool','SQL INST 2','testing relation app + database',getdate());
INSERT INTO backupinventory.dbo.DatabaseMapping (ApplicationName,DatabaseName,Description,CreationDate) VALUES
('Azure Automation','SQL INST 3','testing relation app + database',getdate());
INSERT INTO backupinventory.dbo.DatabaseMapping (ApplicationName,DatabaseName,Description,CreationDate) VALUES
('Human Resources Services','SQL INST 4','testing relation app + database',getdate());
INSERT INTO backupinventory.dbo.DatabaseMapping (ApplicationName,DatabaseName,Description,CreationDate) VALUES
('some amazon services','SQL INST 5','testing relation app + database',getdate());


INSERT INTO backupinventory.dbo.ApplicationMapping (GroupName,ApplicationName,CreationDate) VALUES
('fakegroup01','serviceNow',getdate());
INSERT INTO backupinventory.dbo.ApplicationMapping (GroupName,ApplicationName,CreationDate) VALUES
('fakegroup02','SQL DBA Tool',getdate());
INSERT INTO backupinventory.dbo.ApplicationMapping (GroupName,ApplicationName,CreationDate) VALUES
('fakegroup03','Azure Automation',getdate());
INSERT INTO backupinventory.dbo.ApplicationMapping (GroupName,ApplicationName,CreationDate) VALUES
('fakegroup04','Human Resources Services',getdate());
INSERT INTO backupinventory.dbo.ApplicationMapping (GroupName,ApplicationName,CreationDate) VALUES
('fakegroup05','some amazon services',getdate());

INSERT INTO backupinventory.dbo.GroupMapping (GroupName,UserName,StatusId,CreationDate) VALUES
('fakegroup01','fakeuser01','ACTIVE',getdate());
INSERT INTO backupinventory.dbo.GroupMapping (GroupName,UserName,StatusId,CreationDate) VALUES
('fakegroup02','fakeuser02','ACTIVE',getdate());
INSERT INTO backupinventory.dbo.GroupMapping (GroupName,UserName,StatusId,CreationDate) VALUES
('fakegroup03','fakeuser03','ACTIVE',getdate());
INSERT INTO backupinventory.dbo.GroupMapping (GroupName,UserName,StatusId,CreationDate) VALUES
('fakegroup04','fakeuser04','ACTIVE',getdate());
INSERT INTO backupinventory.dbo.GroupMapping (GroupName,UserName,StatusId,CreationDate) VALUES
('fakegroup05','fakeuser05','ACTIVE',getdate());


INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('IIS-RESTART','SERVER','Restart IIS','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('MEMORY','SERVER','Check Memory Space','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('CPU','SERVER','Check CPU','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('TOP10-MEM','SERVER','TOP 10 Memory','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('DISK','SERVER','Check Space Disk','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('IISStatus','SERVER','IIS Service Status','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('TOP10-CPU','SERVER','TOP 10 CPU','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('IIS','SERVER','IIS Service Related Actions','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('UPTIME','SERVER','Server Uptime','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('SERVICES-LIST','SERVER','List Services','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('REBOOT','SERVER','Restart Server','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('APP-POL-RESET','INSTANCE','Application Pool Account Reset','Connect using WMI to perform actions',getdate())
INSERT INTO backupinventory.dbo.Process (Name, EntityId, [Action], Description, CreationDate) 
values ('WINDOWS-SERVICE','DATABASE','WINDOWS Service','Connect using WMI to perform actions',getdate())


INSERT INTO backupinventory.dbo.Event
(ProcessId, ServerName, UserName, DateTimeStarted, DateTimeCompleted, ProcessParameters, ServiceNowIncidentId, ProcessStatusId, CreationDate)
VALUES(1, 'fakeserver123.ey.com', 'fakeuser01', getdate(), getdate(), 'parameters AABC DB QWERTY 123', 'Change Request 123', 'COMPLETED', getdate());

INSERT INTO backupinventory.dbo.Event
(ProcessId, ServerName, UserName, DateTimeStarted, DateTimeCompleted, ProcessParameters, ServiceNowIncidentId, ProcessStatusId, CreationDate)
VALUES(2, 'fakeserver123.ey.com', 'fakeuser01', getdate(), getdate(), 'parameters AABC DB QWERTY 123', 'Change Request 456', 'FAILED', getdate());

INSERT INTO backupinventory.dbo.Event
(ProcessId, ServerName, UserName, DateTimeStarted, DateTimeCompleted, ProcessParameters, ServiceNowIncidentId, ProcessStatusId, CreationDate)
VALUES(3, 'fakeserver456.ey.com', 'fakeuser05', getdate(), getdate(), 'parameters AABC DB QWERTY 123', 'Change Request 999', 'RUNNING', getdate());

INSERT INTO backupinventory.dbo.Event
(ProcessId, ServerName, UserName, DateTimeStarted, DateTimeCompleted, ProcessParameters, ServiceNowIncidentId, ProcessStatusId, CreationDate)
VALUES(2, 'fakeserver123.ey.com', 'fakeuser03', getdate(), getdate(), 'parameters AABC DB QWERTY 123', 'Change Request 012', 'FAILED', getdate());

INSERT INTO backupinventory.dbo.Event
(ProcessId, ServerName, UserName, DateTimeStarted, DateTimeCompleted, ProcessParameters, ServiceNowIncidentId, ProcessStatusId, CreationDate)
VALUES(3, 'fakeserver456.ey.com', 'fakeuser04', getdate(), getdate(), 'parameters AABC DB QWERTY 123', 'Change Request 111', 'RUNNING', getdate());
