param (
	$sql_srv_instance
)

$queryString = "SELECT  
   A.[Server],  
   A.database_name,
   MAX(C.data_size_gb) as data_size_gb,
   MAX(C.log_size_gb) as log_size_gb,
   MAX(C.total_size_gb) as total_size_gb ,
   MAX(A.last_db_backup_date) as last_db_backup_date,  
   MAX(B.backup_start_date) as backup_start_date,  
   MAX(B.expiration_date)as expiration_date, 
   MAX(B.backup_size) as backup_size,  
   MAX(B.logical_device_name) as logical_device_name,  
   MAX(B.physical_device_name) as physical_device_name,   
   MAX(B.backupset_name) as backupset_name, 
   MAX(B.description)as description ,
   MAX(B.device_type) as device_type,
   device_type_string = CASE   
      WHEN  MAX(B.device_type) = 2  THEN  'Disk'
	   WHEN  MAX(B.device_type) = 5  THEN  'Tape'
	   WHEN  MAX(B.device_type) = 7  THEN  'Virtual Device'
	   WHEN  MAX(B.device_type) = 9  THEN  'Azure Storage' 
	   WHEN  MAX(B.device_type) = 105 THEN  'Permanent Backup Device'		   
	   ELSE  'Unknown'
   END, 
   COUNT(B.physical_device_name) as no_of_files,
   MAX(B.backup_finish_date) as backup_finish_date 
FROM 
   ( 
   SELECT   
       CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
       msdb.dbo.backupset.database_name,  
       MAX(msdb.dbo.backupset.backup_finish_date) AS last_db_backup_date 
   FROM    msdb.dbo.backupmediafamily  
       INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id  
   WHERE   msdb..backupset.type = 'D' 
      and  msdb.dbo.backupset.database_name  NOT IN ('model', 'msdb','master','Statistics_DB')
   GROUP BY 
       msdb.dbo.backupset.database_name  
   ) AS A 
    
   LEFT JOIN  

   ( SELECT   
   CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
   msdb.dbo.backupset.database_name,  
   msdb.dbo.backupset.backup_start_date,  
   msdb.dbo.backupset.backup_finish_date, 
   msdb.dbo.backupset.expiration_date, 
   CAST( (msdb.dbo.backupset.backup_size / (1024. * 1024. * 1024) ) AS DECIMAL(8,2))  AS backup_size,  
   msdb.dbo.backupmediafamily.logical_device_name,  
   msdb.dbo.backupmediafamily.physical_device_name,   
   msdb.dbo.backupset.name AS backupset_name, 
   msdb.dbo.backupset.description ,
   msdb.dbo.backupmediafamily.device_type
FROM   msdb.dbo.backupmediafamily  
   INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id  
WHERE  msdb..backupset.type = 'D' 
and msdb.dbo.backupset.database_name   NOT IN ('model', 'msdb','master','Statistics_DB')
) AS B 
   ON A.[server] = B.[server] AND A.[database_name] = B.[database_name] AND A.[last_db_backup_date] = B.[backup_finish_date] 
   
   LEFT JOIN 
   (
      SELECT 
      database_name = DB_NAME(database_id)
    , log_size_gb = CAST((SUM(CASE WHEN type_desc = 'LOG' THEN size END) * 8. / (1024. * 1024.))AS DECIMAL(8,2))
    , data_size_gb = CAST((SUM(CASE WHEN type_desc = 'ROWS' THEN size END) * 8. / (1024. *1024)) AS DECIMAL(8,2))
    , total_size_gb = CAST((SUM(size) * 8. / (1024. * 1024.)) AS DECIMAL(8,2))
FROM sys.master_files 
GROUP BY database_id
   )  AS  C
   on A.[database_name] = C.[database_name]
AND A.[database_name] = B.[database_name]
group BY  A.[Server],A.[database_name]"

$queryString1 = "SELECT  
   A.[Server],  
   A.database_name,
   C.data_size_mb,
   C.log_size_mb,
   C.total_size_mb,
   A.last_db_backup_date,  
   B.backup_start_date,  
   B.expiration_date, 
   B.backup_size,  
   B.logical_device_name,  
   B.physical_device_name,   
   B.backupset_name, 
   B.description
   
FROM 
   ( 
   SELECT   
       CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
       msdb.dbo.backupset.database_name,  
       MAX(msdb.dbo.backupset.backup_finish_date) AS last_db_backup_date 
   FROM    msdb.dbo.backupmediafamily  
       INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id  
   WHERE   msdb..backupset.type = 'D' 
   GROUP BY 
       msdb.dbo.backupset.database_name  
   ) AS A 
    
   LEFT JOIN  

   ( SELECT   
   CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
   msdb.dbo.backupset.database_name,  
   msdb.dbo.backupset.backup_start_date,  
   msdb.dbo.backupset.backup_finish_date, 
   msdb.dbo.backupset.expiration_date, 
   msdb.dbo.backupset.backup_size,  
   msdb.dbo.backupmediafamily.logical_device_name,  
   msdb.dbo.backupmediafamily.physical_device_name,   
   msdb.dbo.backupset.name AS backupset_name, 
   msdb.dbo.backupset.description 
FROM   msdb.dbo.backupmediafamily  
   INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id  
WHERE  msdb..backupset.type = 'D' ) AS B 
   ON A.[server] = B.[server] AND A.[database_name] = B.[database_name] AND A.[last_db_backup_date] = B.[backup_finish_date] 
   
   LEFT JOIN 
   (
      SELECT 
      database_name = DB_NAME(database_id)
    , log_size_mb = CAST(SUM(CASE WHEN type_desc = 'LOG' THEN size END) * 8. / 1024 AS DECIMAL(8,2))
    , data_size_mb = CAST(SUM(CASE WHEN type_desc = 'ROWS' THEN size END) * 8. / 1024 AS DECIMAL(8,2))
    , total_size_mb = CAST(SUM(size) * 8. / 1024 AS DECIMAL(8,2))
FROM sys.master_files 
GROUP BY database_id
   )  AS  C

   on A.[database_name] = C.[database_name]
ORDER BY  
   A.database_name"

try {
   $sql_cmd_list_output_data = Invoke-Sqlcmd -Query $queryString -ServerInstance "$sql_srv_instance" | Select-Object -Property @{Name="last_db_backup_date"; Expression={$_.last_db_backup_date.DateTime}}, @{Name="backup_start_date"; Expression={$_.backup_start_date.DateTime}}, @{Name="backup_finish_date"; Expression={$_.backup_finish_date.DateTime}}, @{Name="physical_device_name"; Expression={$_.physical_device_name}}, * -ExcludeProperty last_db_backup_date, backup_start_date, backup_finish_date, physical_device_name, ItemArray, Table, RowError, RowState, HasErrors
   $sql_cmd_output_data = $sql_cmd_list_output_data | ConvertTo-Json
   
   if ($sql_cmd_list_output_data.Count -ge 1){
		$output_data = @"
{
   "output": "$true" ,
   "data": {
      "DBsSizeAndLastBkpData": $sql_cmd_output_data
   }
}
"@
   } else {
		$output_data = @"
{
   "output": "$true" ,
   "data": {
      "DBsSizeAndLastBkpData": [$sql_cmd_output_data]
   }
}
"@
   }
}catch {
   $output_data = @"
{
   "output": "$false" ,
   "Error": "$_.Exception.Message"
}
"@
}
return $output_data
