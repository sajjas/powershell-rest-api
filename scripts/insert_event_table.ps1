param (
    $sql_srv_instance,
	$db_name,
	$username,
	$password,
    $process_id, 
    $server_id, 
    $user_id, 
    $date_time_started, 
    $date_time_completed, 
    $process_parameters, 
    $service_now_incident_id, 
    $process_status_id, 
    $creation_date
)

#$db_name = "2"
#$backup_type_name = "1"
#$nas_drive_path = "2"
#$nas_drive_region = "2"

$insertquery=" 
INSERT INTO [automation].[Event] 
           ([ProcessId] 
           ,[ServerId] 
           ,[UserId]
           ,[DateTimeStarted] 
           ,[DateTimeCompleted] 
           ,[ProcessParameters]
           ,[ServiceNowIncidentId] 
           ,[ProcessStatusId] 
           ,[CreationDate]) 
     VALUES 
           ('$process_id' 
           ,'$server_id' 
           ,'$user_id'
           ,'$date_time_started'
           ,'$date_time_completed'
           ,'$process_parameters'
           ,'$service_now_incident_id'
           ,'$process_status_id'
           ,'$creation_date') 
GO 
" 
try {
    $sql_cmd_output_data = Invoke-SQLcmd -ServerInstance "$sql_srv_instance" -Query "$insertquery" -Database "$db_name" -Username "$username" -Password "$password"
	
    $output_data = @"
{
    "output": "$true" ,
    "data": {
        "actual_output": Data Inserted Successfully - $sql_cmd_output_data
    }
}
"@
}catch {
    $output_data = @"
{
    "output": "$false" ,
    "Error": "$_.Exception.Message"
}
"@
}

$output_data
