param (
	$sql_srv_instance,
	$db_name,
	$start_date,
	$end_date
)

$start_date = "'" + "$start_date" + "'" | out-string
$end_date = "'" + "$end_date" + "'" | out-string

if ([String]::IsNullOrEmpty($db_name) -eq $true) {
	$where_cmd = "WHERE ((CONVERT(datetime, msdb.dbo.backupset.backup_start_date, 102) BETWEEN CONVERT(datetime, $start_date) AND CONVERT(datetime, $end_date) + 1))"
} else {
	$where_cmd = "WHERE (msdb.dbo.backupset.database_name = '$db_name' AND (CONVERT(datetime, msdb.dbo.backupset.backup_start_date, 102) BETWEEN CONVERT(datetime, $start_date) AND CONVERT(datetime, $end_date) + 1))"
}

# --------------------------------------------------------------------------------- 
# --Database Backups for all databases For Previous Week 
# --------------------------------------------------------------------------------- 
$queryString = "SELECT 
CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
msdb.dbo.backupset.database_name, 
msdb.dbo.backupset.backup_start_date, 
msdb.dbo.backupset.backup_finish_date, 
msdb.dbo.backupset.expiration_date, 
CASE msdb..backupset.type 
WHEN 'D' THEN 'Database' 
WHEN 'L' THEN 'Log' 
END AS backup_type, 
msdb.dbo.backupset.backup_size, 
msdb.dbo.backupmediafamily.logical_device_name, 
msdb.dbo.backupmediafamily.physical_device_name, 
msdb.dbo.backupset.name AS backupset_name, 
msdb.dbo.backupset.description 
FROM msdb.dbo.backupmediafamily 
INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id " +
$where_cmd + " ORDER BY 
msdb.dbo.backupset.database_name, 
msdb.dbo.backupset.backup_finish_date" 

try {
   $sql_cmd_output_data = Invoke-Sqlcmd -Query $queryString -ServerInstance "$sql_srv_instance" | Select-Object -Property @{Name="last_db_backup_date"; Expression={$_.last_db_backup_date.DateTime}}, @{Name="backup_start_date"; Expression={$_.backup_start_date.DateTime}}, @{Name="backup_finish_date"; Expression={$_.backup_finish_date.DateTime}}, @{Name="physical_device_name"; Expression={$_.physical_device_name}}, * -ExcludeProperty last_db_backup_date, backup_start_date, backup_finish_date, physical_device_name, ItemArray, Table, RowError, RowState, HasErrors | ConvertTo-Json

   $output_data = @"
{
   "output": "$true" ,
   "data": {
      "DatabaseBackupHistory": $sql_cmd_output_data
   }
}
"@
}catch {
   $output_data = @"
{
   "output": "$false" ,
   "Error": "$_.Exception.Message"
}
"@
}
return $output_data