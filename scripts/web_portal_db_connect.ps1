param (
	$site_name
)
Import-Module WebAdministration

$path = Get-Website | where {$_.name -eq $site_name} | ConvertTo-Json

$path1 = $path | ConvertFrom-Json
$physical_path = ($path1.physicalPath | ConvertTo-Json).Trim('"')

$webconfig = Get-WebConfigFile "IIS:\Sites\$site_name"
$final_path = $physical_path + "\\" + $webconfig.name

$webConfigFile = [xml](Get-Content $final_path)
$connString = $webConfigFile.configuration.connectionStrings.add

[array]$data = $connString.ConnectionString.split(";")

foreach ($d in $data) { 
  if ($d -Match "provider connection string") {
    $db_server =  $d.Replace('provider connection string="data source=', "")
	$db_server = $db_server.Replace('\', '\\')
  }
  
  if ($d -Match "initial") {
    $database =  $d.Replace("initial catalog=", "")
  }
  
  if ($d -Match "user id") {
    $username =  $d.Replace("user id=", "")
  }
  
  if ($d -Match "password") {
    $password =  $d.Replace("password=", "")
  }
}

$data=@"
{"db_server":"$db_server","database":"$database","username":"$username","password":"$password"}
"@

$output = $data | ConvertFrom-Json

return $output