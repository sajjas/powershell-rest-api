param (
    $SQLInstance,
    $NASDriveLetter,
    $NASDrivePath
)

try {
    # ----------------------------Enable cmdshell----------------------
    Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 1; RECONFIGURE;"
    Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 1; RECONFIGURE;"

    $sp_configure_enable_output = @"
{
    "output": "$true"
}
"@
}catch {
    $sp_configure_enable_output = @"
{
    "output": "$false" ,
    "Error": "$_.Exception.Message"
}
"@
}

$result = $sp_configure_enable_output | ConvertFrom-Json

if ($result.output -eq 'True') {
    # --------------------Share Mapping is here, use appropriate value-----------------
    try {
        $InventAccount = "EY\P.SMOO.SQL"
        $DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
        # retrieve the password.
		[Byte[]] $key = (1..16)  	
        $SecurePassword = ConvertTo-SecureString $DBAToolPassword -Key $key
		$DBAToolPassword = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword))
		$sql_cmd_output_data = (Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC xp_cmdshell 'net use $NASDriveLetter $NASDrivePath /User:$InventAccount $DBAToolPassword'").output | Out-String
        
		if ($sql_cmd_output_data -NotMatch "The local device name is already in use" -And $sql_cmd_output_data -Match "error") {
			$output_data = @"
{
    "output": "$false" ,
    "data": {
        "NAS_Drive_Letter": "$NASDriveLetter",
        "actual_output": "$sql_cmd_output_data"
    }
}
"@
		} else {
			$output_data = @"
{
    "output": "$true" ,
    "data": {
        "NAS_Drive_Letter": "$NASDriveLetter",
        "actual_output": "$sql_cmd_output_data"
    }
}
"@
		}      
    } catch {
		$error_data = "$_.Exception.Message"
        $output_data = @"
{
        "output": "$false" ,
        "Error": "$error_data"
}
"@
    }

    return $output_data

} elseif ($result.output -eq "False") {
	$error_data = "$_.Exception.Message"
    $output_data = @"
    {
        "output": "$false" ,
        "Error": "$error_data"
    }
"@
    return $output_data
}  

# ----------------------------Disable cmdshell----------------------
Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 0; RECONFIGURE;"
Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 0; RECONFIGURE;"