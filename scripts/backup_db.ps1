param (
    $SQLInstance,
    $DBName,
    $BackupType,
    $NASDriveLetter,
    $db_splits
)

$Date = Get-Date -format yyyy-MM-dd

#set-executionpolicy remotesigned -force
#$Modules = Get-Module -ListAvailable | Select-Object Name -ExpandProperty Name
#foreach ($mod in $Modules) {
#    if ($mod -eq "SqlServer") {
#        Import-Module -Name $mod
#    } elseif ($mod -eq "SQLPS") {
#        Import-module -name $mod
#    } 
#}

$sql_srv_inst = $SQLInstance -replace '\\','-'

$backupfiles = @()
foreach ($split in 1..$db_splits) {
    $backupfiles += "$NASDriveLetter\$sql_srv_inst-$DBName-DOU-$BackupType-$date-split-$split.bak"
}

If ($BackupType -eq 'FULL')  {
	
    try {
        # Backup a complete database
        Backup-SqlDatabase -ServerInstance $SQLInstance `
                            -Database $DBName `
                            -CopyOnly `
                            -BackupFile $backupfiles `
                            -CompressionOption On `
                            -BackupAction Database `
                            -Initialize `
                            -checksum
        $success_data = @"
{
	"BackupType": "$BackupType", 
	"database": "$DBName", 
	"Location": "$backupfiles", 
	"Server": "$SQLInstance"
}
"@

        $output_data = @"
{
	"output": "$true" ,
	"data": $success_data 
}
"@
        return $output_data

    } catch {
		$error_data = "$_".replace('\', '\\').replace('"', ' ') 
        $output_data = @"
{
	"output": "$false" ,
	"Error": "$error_data"
}
"@
        return $output_data | ConvertFrom-Json
    }
    
# As per the Client Requirements T-LOG backup is not required.
# But the below code block will supports it, if in case required.

# }  ElseIf ($BackupType  -eq 'T-LOG')  {

# 	try {
# 		# Backup the transaction log
# 		Backup-SqlDatabase -ServerInstance $SQLInstance `
# 							-Database $DBName `
# 							-BackupFile "$($NASDriveLetter)\$DBName-$BackupType-$date.bak" `
# 							-CompressionOption On `
# 							-BackupAction Log `
# 							-checksum -verbose
# 		$success_data = @"
# [
#     {
#         "BackupType": "$BackupType", 
#         "database": "$DBName", 
#         "Location": "$($NASDriveLetter)\$DBName-$BackupType-$date.bak", 
#         "Server": "$SQLInstance"
#     }
# ]
# "@

#         $output_data = @"
# [
#     {
#         "output": "$true" ,
#         "data": $success_data 
#     }
# ]
# "@
#         return $output_data

#     } catch {
#         $output_data = @"
# [
#     {
#         "output": "$false" ,
#         "Error": "$_.Exception.Message"
#     }
# ]
# "@
#         return $output_data
#     }

}  ElseIf ($BackupType -eq 'DIFF')  {

	try {
		# Create a differential backup
		Backup-SqlDatabase -ServerInstance $SQLInstance `
							-Database $DBName `
							-BackupFile $backupfiles `
							-CompressionOption On `
							-BackupAction Database `
							-Incremental `
                            -Initialize `
							-checksum
		$success_data = @"
{
	"BackupType": "$BackupType", 
	"database": "$DBName", 
	"Location": "$backupfiles", 
	"Server": "$SQLInstance"
}
"@

        $output_data = @"
{
	"output": "$true" ,
	"data": $success_data 
}
"@
        return $output_data

    } catch {
        $error_data = "$_".replace('\', '\\').replace('"', ' ') 
        $output_data = @"
{
	"output": "$false" ,
	"Error": "$error_data"
}
"@
        return $output_data | ConvertFrom-Json
    }

}  Else {
    'Cannot determine Backup Type Selected'
} 
