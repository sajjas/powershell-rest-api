param (
    $sql_srv_instance
)

$output = invoke-sqlcmd -query "select SERVERPROPERTY('IsClustered') as IsClustered" -serverInstance "$sql_srv_instance" | Select -ExpandProperty IsClustered | ConvertTo-Json

return $output