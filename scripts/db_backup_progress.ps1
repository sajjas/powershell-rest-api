param (
    $sql_srv_instance,
    $db_name,
	$request_type
)

$queryString = "SELECT session_id as SPID, command, a.text AS Query, start_time, percent_complete, dateadd(second,estimated_completion_time/1000, getdate()) as estimated_completion_time
FROM sys.dm_exec_requests r CROSS APPLY sys.dm_exec_sql_text(r.sql_handle) a
WHERE r.command in ('BACKUP DATABASE')"

$output = Invoke-Sqlcmd -Query $queryString -ServerInstance "$sql_srv_instance" | ConvertTo-Json
$data = $output | ConvertFrom-Json

if($data -ne $null) { 
	foreach ($item in $data) {
		if ($item.Query -Match $db_name) {
			if ($request_type -eq "backup_create_progress") {
				$status_complete = [math]::round($item.percent_complete) | ConvertTo-Json 
				return $status_complete
			} elseif ($request_type -eq "backup_create_cancel"){
				# https://blog.sqlauthority.com/2015/08/31/sql-server-spid-is-killedrollback-state-what-to-do-next/
				$spid = $item.SPID | ConvertTo-Json
				$hostprocess = (invoke-sqlcmd -query "select distinct(hostprocess) from master..sysprocesses where spid = '$spid'" -ServerInstance "$sql_srv_instance").hostprocess
				Invoke-Sqlcmd -Query "KILL $spid;KILL $spid with STATUSONLY;" -ServerInstance "$sql_srv_instance"
				invoke-command -scriptblock { stop-process $hostprocess -Force} 
				Start-Sleep -s 15
				$hostprocess = (invoke-sqlcmd -query "select distinct(hostprocess) from master..sysprocesses where spid = '$spid'" -ServerInstance "$sql_srv_instance").hostprocess
				if ([String]::IsNullOrEmpty($hostprocess) -eq $true) {
					return $true
				} else {
					return $false
				}
			}
		}
	}	
} else {
	return 0 | Out-String
}
