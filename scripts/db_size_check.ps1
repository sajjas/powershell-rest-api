param (
	$sql_srv_instance,
	$db_name	
)

Set-StrictMode -Version 2
[Void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ConnectionInfo")
[Void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO")
[Void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoExtended")
$Conn = new-object Microsoft.SqlServer.Management.Common.ServerConnection
$Conn.applicationName = "PowerShell GetSQLDBInfo (using SMO)"

#Set the parameters for the environment
$Conn.ServerInstance=$sql_srv_instance
$Conn.LoginSecure = $true                    #Set to true connect using Windows Authentication
#$Conn.Login = "sa"                          #Do not apply if you use Windows Authentication
#$Conn.Password = "SAPassword"               #Do not apply if you use Windows Authentication

#Connect to the SQL Server and get the databases
$srv = New-Object Microsoft.SqlServer.Management.Smo.Server $conn
$dbs = $srv.Databases

#Process databases if the matches to input
foreach ($db in $dbs) {
	if ($db.Name -eq $db_name) {
		$dbfilesize=[math]::floor($db.FileGroups.files.Size/1024)           #Convert to MB	
		
		#Disconnect from the SQL Server database
		$srv.ConnectionContext.Disconnect()
		
		return $dbfilesize
	}
}
