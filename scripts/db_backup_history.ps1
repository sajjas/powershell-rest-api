param (
	$sql_srv_instance,
	$db_name
)

# ------------------------------------------------------------------------------------------- 
# --Most Recent Database Backup for Each Database 
# ------------------------------------------------------------------------------------------- 
$queryString = "SELECT  
   CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
   msdb.dbo.backupset.database_name,  
   MAX(msdb.dbo.backupset.backup_finish_date) AS last_db_backup_date 
FROM   msdb.dbo.backupmediafamily  
   INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id  
WHERE  msdb..backupset.type = 'D' and msdb..backupset.database_name = '$db_name'
GROUP BY 
   msdb.dbo.backupset.database_name  
ORDER BY  
   msdb.dbo.backupset.database_name;"
   
# ------------------------------------------------------------------------------------------- 
# --Most Recent Database Backup for Each Database - Detailed 
# ------------------------------------------------------------------------------------------- 
$queryString1 = "SELECT  
   A.[Server],  
   B.database_name,
   A.last_db_backup_date,  
   B.backup_start_date,
   B.backup_finish_date,  
   B.expiration_date, 
   B.backup_size,  
   B.logical_device_name,  
   B.physical_device_name,   
   B.backupset_name, 
   B.description,
   B.backup_type
FROM 
   ( 
   SELECT   
       CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
       msdb.dbo.backupset.database_name,  
       MAX(msdb.dbo.backupset.backup_finish_date) AS last_db_backup_date 
   FROM    msdb.dbo.backupmediafamily  
       INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id  
   WHERE   msdb..backupset.type = 'D' and msdb..backupset.database_name = '$db_name'
   GROUP BY 
       msdb.dbo.backupset.database_name 
   ) AS A 
    
   LEFT JOIN  

   ( 
   SELECT   
   CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
   msdb.dbo.backupset.database_name,  
   msdb.dbo.backupset.backup_start_date,  
   msdb.dbo.backupset.backup_finish_date, 
   msdb.dbo.backupset.expiration_date, 
   msdb.dbo.backupset.backup_size,  
   msdb.dbo.backupmediafamily.logical_device_name,  
   msdb.dbo.backupmediafamily.physical_device_name,   
   msdb.dbo.backupset.name AS backupset_name, 
   msdb.dbo.backupset.description,
   CASE msdb..backupset.type 
WHEN 'D' THEN 'Database' 
WHEN 'L' THEN 'Log' 
When 'I' THEN 'Differential database'
END AS backup_type
FROM   msdb.dbo.backupmediafamily  
   INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id  
WHERE  msdb..backupset.type = 'D' and msdb..backupset.database_name = '$db_name'
   ) AS B 
   ON A.[server] = B.[server] AND A.[database_name] = B.[database_name] AND A.[last_db_backup_date] = B.[backup_finish_date]" 

try {
   $sql_cmd_output_data = Invoke-Sqlcmd -Query $queryString1 -ServerInstance "$sql_srv_instance" | Select-Object -Property @{Name="last_db_backup_date"; Expression={$_.last_db_backup_date.DateTime}}, @{Name="backup_start_date"; Expression={$_.backup_start_date.DateTime}}, @{Name="backup_finish_date"; Expression={$_.backup_finish_date.DateTime}}, @{Name="physical_device_name"; Expression={$_.physical_device_name}}, * -ExcludeProperty last_db_backup_date, backup_start_date, backup_finish_date, physical_device_name, ItemArray, Table, RowError, RowState, HasErrors | ConvertTo-Json

   $output_data = @"
{
   "output": "$true" ,
   "data": {
      "actual_output": $sql_cmd_output_data
   }
}
"@
}catch {
   $output_data = @"
{
   "output": "$false" ,
   "Error": "$_.Exception.Message"
}
"@
}
$output_data
