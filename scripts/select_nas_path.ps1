param (
	$nas_region,
	$db_server,
	$database,
	$username,
	$password
)

$select_nas_path_query = "SELECT BackupPath FROM automation.NASShare where RegionId = '$nas_region'"

$select_nas_path = Invoke-SQLcmd -ServerInstance "$db_server" -Query "$select_nas_path_query" -Database "$database" -Username "$username" -Password "$password"

return $select_nas_path | ConvertTo-Json