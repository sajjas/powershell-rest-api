# Param block at the beginning of the script

[CmdletBinding()]
param (
   	$PHYDEVICENAME,
	$BACKUPSETNAME
)

function Test-IsGuid
{
	[OutputType([bool])]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$ObjectGuid
	)
	
	# Define verification regex
	[regex]$guidRegex = '(?im)^[{(]?[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?[0-9A-F]{1}?$'

	# Check guid against regex
	return $ObjectGuid -match $guidRegex
}

function Test-DOU
{
	[OutputType([bool])]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$ObjectPath
	)
	
	# Check guid against regex
	return $ObjectPath.Contains("-DOU-")
}

function Test-BackupSetName
{
	[OutputType([bool])]
	param
	(
		[Parameter(Mandatory = $true)]
		[string]$BackupSetName
	)
	
	# Check guid against regex
	return $BackupSetName.Contains("Veeam")
}

function test-guidveeam {

	$isGuid = Test-IsGuid -ObjectGuid $PHYDEVICENAME

	$isVeeam = Test-BackupSetName -BackupSetName $BACKUPSETNAME

	$isdou = Test-DOU -ObjectPath $PHYDEVICENAME

	if ($isGuid -eq "$true" -Or $isVeeam -eq "$true") {
		return "veeam"
	} elseif ($isdou -eq "$true") {
		return "dou"
	} else {
		return "non-dou"
	}
}

test-guidveeam
