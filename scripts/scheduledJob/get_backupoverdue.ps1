param(
	[Parameter(Mandatory = $false, ValueFromPipeline = $true)]
    [String]$JOB_INPUT,
    [Parameter(Mandatory = $false, ValueFromPipeline = $true)]
    [String]$SERVERNAME
)

#$scheduled_job_id = 68
#$days_without_backup = 1
#$backup_type = "FULL"
#$db_srv_instance = "DEFRAVMQMISSQ01.eyqa.net"
#$sql_srv_instance = "DEFRAVMQMISSQ01.eyqa.net\INST1"

# -----------------------------------------------------------------------------
#          Global Lists Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

# Overdue backup database return list, insert into Alert table and insert into ScheduledJobRunHistory table.
function getOverdueBackupDBS {
	param([String]$JOB_INPUT_ATTEMP,[String]$SERVERNAME_ATTEMP,[String]$INSTANCENAME_ATTEMP)
	
	$json_data = $JOB_INPUT_ATTEMP | ConvertFrom-Json 
	$db_srv_instance = $SERVERNAME_ATTEMP
	$sql_srv_instance = $INSTANCENAME_ATTEMP
	$backup_type = $json_data.backup_type
	$scheduled_job_id = $json_data.scheduled_job_id
	$days_without_backup = $json_data.overdue_backup_days
	
	if ($backup_type -eq "FULL"){
		$backup_type_id = "D"
	} elseif ($backup_type -eq "DIFF"){
		$backup_type_id = "I"
	}
	# -----------------------------------------------------------------------------
	log -Message "Email:	$email "
	log -Message "Scheduled Job Id:	$scheduled_job_id "
	log -Message "Overdue Backup Days:	$days_without_backup "
	log -Message "Backup Type:	$backup_type "
	$db_srv_instance = "DEFRAVMQMISSQ01.eyqa.net"
	$sql_srv_instance = "DEFRAVMQMISSQ01.eyqa.net\INST1"
	# -----------------------------------------------------------------------------
	# Fetching List of Overdue Backup Databases List
	$overdue_db_backup_queryString = "
		SELECT DISTINCT msdb.dbo.backupset.database_name 
		FROM msdb.dbo.backupset 
		WHERE msdb..backupset.type = '$backup_type_id' 
		AND (CONVERT(datetime, msdb.dbo.backupset.backup_finish_date, 102) >= GETDATE() - $days_without_backup)
		"
	log -Message "Overdue Backup Databases Query:	$overdue_db_backup_queryString "
	$run_start_time = Get-Date -format "yyyy-MM-dd HH:mm:ss"
	try {
		# Create Remote Session to DB Server Instance
		$remote_session = Create-PSSession -ServerName $db_srv_instance
		log -Message "Session Created to Remote Server: $remote_session"
		# Connect to sql server instance
		$databases = Invoke-Command -Session $remote_session -ScriptBlock {(invoke-sqlcmd -serverInstance $using:sql_srv_instance -query $using:overdue_db_backup_queryString).database_name}
		
		# Disconnect remote-session
		Remove-PSSession $remote_session
		log -Message "Session Removed to Remote Server: $remote_session"
		
		$run_status = "COMPLETED"
		$get_overdue_backup_dbs_query_output_data = @{output = $true; Success_Message = $databases | Out-String} | ConvertTo-Json
	} catch {
		$get_overdue_backup_dbs_query_output_data = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
		$run_status = "FAILED"
	}
	
	$run_end_time = Get-Date -format "yyyy-MM-dd HH:mm:ss"
	$run_output = @{OverdueBackupDatabasesList = $get_overdue_backup_dbs_query_output_data | Out-String} | ConvertTo-Json
	log -Message "Overdue Backup Databases List:	$get_overdue_backup_dbs_query_output_data "
	
	$backup_type = @{BackupType = $backup_type} | ConvertTo-Json
	# -----------------------------------------------------------------------------
	if ([String]::IsNullOrEmpty($databases) -ne $true) {
		foreach ($database in $databases) {
			$queryString = "
			INSERT INTO [automation].[Alert]
				   ([TypeId]
				   ,[CreationDate]
				   ,[StatusId]
				   ,[Info]
				   ,[Last_Modified_Date]
				   ,[ServerName]
				   ,[InstanceName]
				   ,[DBName])
				VALUES
				   ('OVERDUE_BACKUP'
				   ,getdate()
				   ,'CREATED'
				   ,'$backup_type'
				   ,getdate()
				   ,'$db_srv_instance'
				   ,'$sql_srv_instance'
				   ,'$database');
			"
			try {
				log -Message "invoke-sqlcmd -ServerInstance $VAR_SERVERINSTANCE -Query $queryString -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD"
				$insert_alert_table_query_output = invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
				$insert_alert_table_query_output_data = @{output = $true; Success_Message = $insert_alert_table_query_output} | ConvertTo-Json
			} catch {
				$insert_alert_table_query_output_data = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
			}
			
			log -Message "Insert into Alert Table Query Output:		$insert_alert_table_query_output_data"
		}
	} 
	# -----------------------------------------------------------------------------
	$queryString = "
		INSERT INTO [automation].[ScheduledJobsRunHistory]
			   ([ScheduledJobid]
			   ,[ServerName]
			   ,[InstanceName]
			   ,[DBName]
			   ,[RunStarttime]
			   ,[RunEndtime]
			   ,[RunStatus]
			   ,[RunOutput])
			VALUES
			   ('$scheduled_job_id'
			   ,'$db_srv_instance'
			   ,'$sql_srv_instance'
			   ,'NULL'
			   ,'$run_start_time'
			   ,'$run_end_time'
			   ,'$run_status'
			   ,'$run_output');
		"
	try {
		log -Message "invoke-sqlcmd -ServerInstance $VAR_SERVERINSTANCE -Query $queryString -Database $VAR_DATABASE -Username $VAR_USERNAME -Password $VAR_PASSWORD"
		$insert_scheduled_jobs_run_history_query_output = invoke-sqlcmd -ServerInstance "$VAR_SERVERINSTANCE" -Query "$queryString" -Database "$VAR_DATABASE" -Username "$VAR_USERNAME" -Password "$VAR_PASSWORD"
		$insert_scheduled_jobs_run_history_query_output_data = @{output = $true; Success_Message = $insert_alert_table_query_output} | ConvertTo-Json
	} catch {
		$insert_scheduled_jobs_run_history_query_output_data = @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
	}
	log -Message "Insert into Scheduled Job Run History Table Output:	$insert_scheduled_jobs_run_history_query_output_data"
}

# -------------------------------------------------
# -------------------------
# -------------------------
# -------------------------------------------------

# Create Session with Remote Server
Function Create-PSSession {

[CmdletBinding()]
Param (
    [Parameter(Mandatory=$True)]
    [string]$ServerName
)

$pwd = Get-Location
$DBAToolPassword = Get-Content $("$pwd\\lib\\Security.dll")

    If ($ServerName -match "ey.net" -and $ServerName -notmatch "cloudapp"){
        $InventAccount = "EY\P.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
    } Elseif ($ServerName -match "eydmz.net"){
        $InventAccount = "EYDMZ.NET\Z.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoASQA2ADEANQBjAGEAdgB4AFcATQBUAGYAZgA3AEkAVQBCAHMAUgBTAHcAPQA9AHwAOQA2ADAAMwA5ADcAZQBlADYANQAzADQAYgA3AGYAYQA5ADYAMAA3ADgAOAA5ADIANQAzADYAOQBmADEAMgBhAGMAMAA1ADkAYQAyADcAZAA0ADcAZAA5ADEAZgA3ADcAYQAyADEAZABjAGEAMgBkADUAMABiAGIAMgBkAGQAYwA='
    } Elseif ($ServerName -match "eyxstaging.net"){
        $InventAccount = "EYXSTAGING.NET\X.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoAWgByAHYAMAB4AFkAUQBVAFAAQgBXAEEAUgA5ADAAawBmAEQAbAB5AEEAPQA9AHwAMgA4ADkAZgBmAGEANQA3AGUANwAwADQAZAA3ADUAYQBkADMAZgAwADMANQBmADQAYwA1AGQAZgA0ADIAMwA0ADgAYwA3AGYANQBkAGUAMwA1ADUANgA3ADYAYgBhAGQAOQAxADcAMgBmAGQAOAA0ADkAMABhAGIAYgBkADUAYQA='
    } Elseif ($ServerName -match "eyua.net"){
        $InventAccount = "EYUA\U.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHIAZgBzAGkAOABxADUAOQBmAGIAVwB4AFcAKwBHAEEANQA4ADYANQBpAGcAPQA9AHwAMQBkADUAOAA3ADEAYwA2AGYANwA3AGIANwA2AGUANQAwADUAYQA2AGYAZQBkADAAZgAwAGEAMQA5ADAAMQBkADMAYQAwADcAYwAyAGIAZQAwADUAMgBiADYAMQBjAGYAMwA5ADcAMAA2ADUAMgBjADYAMwA5ADkAYgA0ADUAOQA='
    } Elseif ($ServerName -match "eyqa.net"){
        $InventAccount = "EYQA\Q.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHcAdgA3AFcAcQBIAEYAUQAwAHoAUABNAGkAZAB4AHYANQBoAFcAVABWAHcAPQA9AHwANQAxADYAYwBiADcAOABmADMAYQAzADUAMgBlAGYANgBmAGUAMwA0ADEAMwA0AGMANwBkADcAYgA2ADMAZgAxADEAYQA4AGIAMQAxADcANwA2ADcAYwAwADYAMABkADQANwAyADcAMwA1ADMAYQA1ADYAZABlADgAMABmAGIAZQA='
    } Elseif ($ServerName -match "eydev.net"){
        $InventAccount = "EYDEV\D.SMOO.SQL"
		$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEYANABNAFcAbgBTAHAAUQBMAGYAYgAxAGgASAAyAGUAQgBtAGYAZQBTAFEAPQA9AHwAMwBlAGEAZQA1AGYAMgBlAGUANABkADQAOABlAGYAMwBlAGMAMABhADYAMAAzADkAMgBjADgAYQAxADQAMgA0AGEAMQA4ADQAZQA4AGMAMAAzADgAMgA5AGIANwBmADEAYgA0ADkAZgBmADIAYgA3AGEAYQBhAGYAZABhADIANwA='
    } Elseif ($ServerName -match "ey.net" -and $ServerName -match "cloudapp"){
        $InventAccount = "CLOUDAPP\C.SQLINVENT"
    } Elseif ($ServerName -match "eydev.net" -and $ServerName -match "cloudapp"){
        $InventAccount = "CLOUDAPPDEV\A.SQLINVENT"
    } Else {
        If ($ServerName -match '^[A-Z]{6,7}[Pp]' -and ($($ServerName.Substring(0,2)) -notmatch "^AC" -and $ServerName -notmatch ".cloudapp.ey.net$")){
            $InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
            If ($ServerName -notmatch 'ey.net$'){
                $ServerName += ".ey.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Zz]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYDMZ.NET\Z.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoASQA2ADEANQBjAGEAdgB4AFcATQBUAGYAZgA3AEkAVQBCAHMAUgBTAHcAPQA9AHwAOQA2ADAAMwA5ADcAZQBlADYANQAzADQAYgA3AGYAYQA5ADYAMAA3ADgAOAA5ADIANQAzADYAOQBmADEAMgBhAGMAMAA1ADkAYQAyADcAZAA0ADcAZAA5ADEAZgA3ADcAYQAyADEAZABjAGEAMgBkADUAMABiAGIAMgBkAGQAYwA='
            If ($ServerName -notmatch 'eydmz.net$'){
                $ServerName += ".eydmz.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Xx]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYXSTAGING.NET\X.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHoAWgByAHYAMAB4AFkAUQBVAFAAQgBXAEEAUgA5ADAAawBmAEQAbAB5AEEAPQA9AHwAMgA4ADkAZgBmAGEANQA3AGUANwAwADQAZAA3ADUAYQBkADMAZgAwADMANQBmADQAYwA1AGQAZgA0ADIAMwA0ADgAYwA3AGYANQBkAGUAMwA1ADUANgA3ADYAYgBhAGQAOQAxADcAMgBmAGQAOAA0ADkAMABhAGIAYgBkADUAYQA='
            If ($ServerName -notmatch 'eyxstaging.net$'){
                $ServerName += ".eyxstaging.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Uu]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYUA\U.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHIAZgBzAGkAOABxADUAOQBmAGIAVwB4AFcAKwBHAEEANQA4ADYANQBpAGcAPQA9AHwAMQBkADUAOAA3ADEAYwA2AGYANwA3AGIANwA2AGUANQAwADUAYQA2AGYAZQBkADAAZgAwAGEAMQA5ADAAMQBkADMAYQAwADcAYwAyAGIAZQAwADUAMgBiADYAMQBjAGYAMwA5ADcAMAA2ADUAMgBjADYAMwA5ADkAYgA0ADUAOQA='
            If ($ServerName -notmatch 'eyua.net$'){
                $ServerName += ".eyua.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Qq]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYQA\Q.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AHcAdgA3AFcAcQBIAEYAUQAwAHoAUABNAGkAZAB4AHYANQBoAFcAVABWAHcAPQA9AHwANQAxADYAYwBiADcAOABmADMAYQAzADUAMgBlAGYANgBmAGUAMwA0ADEAMwA0AGMANwBkADcAYgA2ADMAZgAxADEAYQA4AGIAMQAxADcANwA2ADcAYwAwADYAMABkADQANwAyADcAMwA1ADMAYQA1ADYAZABlADgAMABmAGIAZQA='
            If ($ServerName -notmatch 'eyqa.net$'){
                $ServerName += ".eyqa.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Dd]' -and $($ServerName.Substring(0,2)) -notmatch "^AC"){
            $InventAccount = "EYDEV\D.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEYANABNAFcAbgBTAHAAUQBMAGYAYgAxAGgASAAyAGUAQgBtAGYAZQBTAFEAPQA9AHwAMwBlAGEAZQA1AGYAMgBlAGUANABkADQAOABlAGYAMwBlAGMAMABhADYAMAAzADkAMgBjADgAYQAxADQAMgA0AGEAMQA4ADQAZQA4AGMAMAAzADgAMgA5AGIANwBmADEAYgA0ADkAZgBmADIAYgA3AGEAYQBhAGYAZABhADIANwA='
            If ($ServerName -notmatch 'eydev.net$'){
                $ServerName += ".eydev.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Pp]' -and $($ServerName.Substring(0,2)) -match "^AC"){
            $InventAccount = "CLOUDAPP\C.SQLINVENT"
            If ($ServerName -notmatch 'cloudapp.ey.net$'){
                $ServerName += ".cloudapp.ey.net"
            }
        } Elseif ($ServerName -match '^[A-Z]{6,7}[Dd]' -and $($ServerName.Substring(0,2)) -match "^AC"){
            $InventAccount = "CLOUDAPPDEV\A.SQLINVENT"
            If ($ServerName -notmatch 'cloudapp.eydev.net$'){
                $ServerName += ".cloudapp.ey.net"
            }
        } Else {
            $InventAccount = "EY\P.SMOO.SQL"
			$DBAToolPassword = '76492d1116743f0423413b16050a5345MgB8AEUARgAwAFYAVwBmAGQAYgByAEcAMQBXAE4AdgAwAHcAaABoAHkASwBoAFEAPQA9AHwAZgBiADEANwBlADgAZABmADUAMwBmAGEAOQA3AGYAYwA0AGYAMgBhAGYAYQA0ADQANgA3AGMAYgBhADIAZgA0ADMAOQA0AGQAOAAzADkANgBmADQAOQBiADUAMAA2ADEAOAA3AGEANwBiADUAZAA4AGMANQBlADUAMAAxADQAMAA='
            If ($ServerName -notmatch 'ey.net$'){
                $ServerName += ".ey.net"
            }
        }
    }
    If ($InventAccount -ne $null) {
		# retrieve the password.
		[Byte[]] $key = (1..16)  	
        $SecurePassword = ConvertTo-SecureString $DBAToolPassword -Key $key
        $InventCredential = New-Object System.Management.Automation.PSCredential ($InventAccount, $SecurePassword)
        $RemoteSessionOption = New-PSSessionOption -SkipCACheck -OpenTimeout 180000 -IdleTimeout 180000 #3 minutes
        $Session = New-PSSession -ComputerName $ServerName -Credential $InventCredential -SessionOption $RemoteSessionOption -ErrorAction Stop
        return $Session
    }
}

# -----------------------------------------------------------------------------
function log {
	[CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, Position = 1)]
        [string]$Message
    )
	
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> C:\Users\A1263268-3\Documents\db-backup-rest-api-server\scripts\scheduledJob\overdueBackupLog.txt
}

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
log -Message " --- START  getOverdueBackupDBS ---"
log -Message "JOB INPUT  -> $JOB_INPUT"
log -Message "SERVERNAME -> $SERVERNAME"
log -Message "INSTANCENAME -> $INSTANCENAME"
getOverdueBackupDBS -JOB_INPUT_ATTEMP $JOB_INPUT -SERVERNAME_ATTEMP $SERVERNAME
log(" --- END  getOverdueBackupDBS ---")
