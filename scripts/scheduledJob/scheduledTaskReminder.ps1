$SMTP = "smtp.ey.net"
$FROM = "adminScheduledTaskReminder@ey.com"
$LOCAL_HOSTNAME = hostname

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

# -----------------------------------------------------------------------------
function sendReminderNotification {
    Param (
		[array]$TO,
		[string]$TABLEdata
	)
    
	$messageParameters = @{
		Subject = "Reminder: Your Scheduled Task Will be run soon"
		Body = "You have Scheduled the below task. It will be run soon 
				
				$TABLEdata"
		From = "$FROM"
		To = $TO.split(',')
		SmtpServer = "$SMTP"
    }
    log("EMAIL TO ADDR:	$TO")
	log("EMAIL FROM ADDR:	$FROM")
	LOG("BODY:	$TABLEdata")
	LOG("SMTP SERVER:	$SMTP")
	try {
		Send-MailMessage @messageParameters -BodyAsHtml
		log("----> Email sent From: $From To: $To  using smtp server: $SMTP")
		return $true
	} catch {
		log("Failed to send Email:	$_.Exception.Message")
		return $false
	}
}

function createReminderScheduledTask {
	$action = New-ScheduledTaskAction -Execute 'Powershell.exe' -Argument '"C:\SQL-Notification\jobs\scheduledTaskReminder.ps1"'
	$trigger = New-ScheduledTaskTrigger `
		-Once `
		-At (Get-Date) `
		-RepetitionInterval (New-TimeSpan -Minutes 15) `
		-RepetitionDuration (New-TimeSpan -Days (365 * 23))
	Register-ScheduledTask -Action $action -Trigger $trigger -TaskPath "\DBSRE" -TaskName "DBSRE-ScheduledTask-Reminder" -Description "DBSRE-ScheduledTask-Reminder"
}

function tableData($data) {
	$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
	$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
	$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
	$style = $style + "TD{border: 1px solid black; padding: 5px; }"
	$style = $style + "</style>"
	
	$new_data = $data | ConvertFrom-Json
	if ($new_data.PSobject.Properties.Name -contains "DBName") {
		$tableOutput = $new_data | select ServerName, InstanceName, DBName, ScheduledTime, ProcessAction | ConvertTo-Html -Head $style
	} else {
		$tableOutput = $new_data | select ServerName, InstanceName, ScheduledTime, ProcessAction | ConvertTo-Html -Head $style
	}
	return $tableOutput
}

function fetchReminderScheduledTaskInfo {
	log("======== START FETCHING TASK LIST FOR EMAIL REMINDER ========")
	$taskListForEmailReminders = Invoke-Command -ComputerName $LOCAL_HOSTNAME -ScriptBlock {invoke-sqlcmd -serverInstance $using:VAR_SERVERINSTANCE -query "select Id, ProcessId, ServerName, InstanceName, DBName, ReminderEmail, ScheduledTime, ReminderCount from [automation].[ScheduledTasks] where StatusId = 'SCHEDULED' And ReminderCount = 0 And (CONVERT(datetime, DATEADD(minute, DATEDIFF(minute, GETDATE(), GETUTCDATE()), GETDATE())) >= (CONVERT(datetime, ScheduledTime) - CONVERT(datetime, (ReminderMins + 15))))" -Database $using:VAR_DATABASE -Username $using:VAR_USERNAME -Password $using:VAR_PASSWORD}
	if ([String]::IsNullOrEmpty($taskListForEmailReminders) -ne $true) {
		foreach ($taskItem in $taskListForEmailReminders) {
			log("PROCESSING TASK LIST ITEM")
			# ---------------------------------------------------------------------------------
			$PROCESSID = $taskItem.ProcessId
			$TASKID = $taskItem.Id
			$SERVERNAME = $taskItem.ServerName
			$INSTANCENAME = $taskItem.InstanceName
			$SCHEDULETIME = $taskItem.ScheduledTime
			$INC_REMINDERCOUNT = $taskItem.ReminderCount + 1
			# ---------------------------------------------------------------------------------
			$PROCESS_ACTION = Invoke-Command -ComputerName $LOCAL_HOSTNAME -ScriptBlock {invoke-sqlcmd -serverInstance $using:VAR_SERVERINSTANCE -query "select Action from [automation].[Process] where Id = $using:PROCESSID" -Database $using:VAR_DATABASE -Username $using:VAR_USERNAME -Password $using:VAR_PASSWORD}    
			$PROCESSACTION = $PROCESS_ACTION.Action
			if ($PROCESSACTION -eq "BACKUP_NOW") {
				$DBNAME = $taskItem.DBName
				$PROCESSACTION = "DB BACKUP"
				$data = @{ServerName= $SERVERNAME; InstanceName= $INSTANCENAME; DBName= $DBNAME; ScheduledTime= $SCHEDULETIME | Out-String; ProcessAction= $PROCESSACTION} | ConvertTo-Json
			} else {
				$data = @{ServerName= $SERVERNAME; InstanceName= $INSTANCENAME; ScheduledTime= $SCHEDULETIME | Out-String; ProcessAction= $PROCESSACTION} | ConvertTo-Json
			}
			
			log("PROCESSID:	$PROCESSID")
			log("TASKID:	$TASKID")
			log("SERVERNAME:	$SERVERNAME")
			log("INSTANCENAME:	$INSTANCENAME")
			log("DBNAME:	$DBNAME")
			log("SCHEDULEDTIME:	$SCHEDULETIME")
			log("PROCESSACTION:	$PROCESSACTION")
			# ---------------------------------------------------------------------------------
			$TABLE_DATA = tableData($data)
			# ---------------------------------------------------------------------------------
			log("======== START SENDING EMAIL ========")
			$TO_ADDR = $taskItem.ReminderEmail
			$Email_Sent_Result = sendReminderNotification -TO $TO_ADDR -TABLEdata $TABLE_DATA
			log("======== END SENDING EMAIL ========")
			# ---------------------------------------------------------------------------------
			$Email_Sent_Result = $true
			if ($Email_Sent_Result -eq $true) {
				log("======== UPDATING SCHEDULED TASK TABLE WITH REMINDERMINS TO 1 ========")
				Invoke-Command -ComputerName $LOCAL_HOSTNAME -ScriptBlock {invoke-sqlcmd -serverInstance $using:VAR_SERVERINSTANCE -query "UPDATE [automation].[ScheduledTasks] SET ReminderCount = $using:INC_REMINDERCOUNT WHERE Id = $using:TASKID" -Database $using:VAR_DATABASE -Username $using:VAR_USERNAME -Password $using:VAR_PASSWORD}    
			}
		}
	} else {
		log("TASK LIST FOR EMAIL REMINDER --> [EMPTY]")
	}
}

# -----------------------------------------------------------------------------
function log ($Message)
{
	#$scriptDir = Get-Location
	$scriptDir = "C:"
    $logDate = "{0:MM-dd-yy}/{0:HH.mm.ss}" -f (Get-Date)
    write-output "[$logDate] - $Message" >> $scriptDir/reminder-task-output.log
}

# Main Function
log("+++++++++++++ START SCHEDULED TASK REMINDER ++++++++++++++")
fetchReminderScheduledTaskInfo
log("+++++++++++++ END SCHEDULED TASK REMINDER ++++++++++++++")