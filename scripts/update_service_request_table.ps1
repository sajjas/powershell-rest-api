param (
	$type,
	$insert_id,
	$connString_db_server,
	$connString_db_name,
	$connString_username,
	$connString_password,
	$type_id,
	$status_id,
	$db_srv_instance,
    $sql_srv_instance,
	$db_name,
	$service_now_incident_id,
	$info,
	$user_id,
	$comments
)

if ($type -eq "INSERT") {
	$queryString=" 
	INSERT INTO [automation].[ServiceRequest] 
			   ([TypeId]
			   ,[StatusId]
			   ,[ServerName] 
			   ,[InstanceName]
			   ,[DBName]
			   ,[SNRefNo]
			   ,[Info] 
			   ,[Comments] 
			   ,[RequestedUser]
			   ,[CreationDate]) 
		 VALUES 
			   ('$type_id'
			   ,'$status_id'
			   ,'$db_srv_instance' 
			   ,'$sql_srv_instance' 
			   ,'$db_name'
			   ,'$service_now_incident_id'
			   ,'$info'
			   ,'$comments'
			   ,'$user_id'
			   ,getdate());
	SELECT SCOPE_IDENTITY();
	"	
} elseif ($type -eq "UPDATE") {
	$queryString=" 
	UPDATE [automation].[ServiceRequest] SET
		TypeId='$type_id', 
		StatusId='$status_id',
		ServerName='$db_srv_instance',
		InstanceName='$sql_srv_instance', 
		DBName='$db_name', 
		SNRefNo='$service_now_incident_id', 
		Info='$info', 
		Comments='$comments', 
		RequestedUser='$user_id',
		CreationDate=getdate()
	WHERE Id='$insert_id'; 
	SELECT SCOPE_IDENTITY();
	"
}

$sql_cmd_output_data = Invoke-SQLcmd -ServerInstance "$connString_db_server" -Query "$queryString" -Database "$connString_db_name" -Username "$connString_username" -Password "$connString_password"

return $sql_cmd_output_data.Column1
