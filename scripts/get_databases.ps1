Function GetDatabases {
	param (
		$sql_srv_instance
	)
	
	$select_db_query = "select name from sys.databases WHERE name NOT IN ('master', 'tempdb', 'model', 'msdb');"

	$invoke_sql_query = Invoke-Sqlcmd -ServerInstance "$sql_srv_instance" -Query "$select_db_query" | Select -ExpandProperty Name | ConvertTo-Json

	#$data = '"{0}"' -f ( $invoke_sql_query -join '","' )

	$data = @"
{ 
	"DatabaseNames": $invoke_sql_query
}
"@
	return $data
}