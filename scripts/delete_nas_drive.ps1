param (
    $SQLInstance,
    $NASDriveLetter
)

try {
    # ----------------------------Enable cmdshell----------------------
    Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 1; RECONFIGURE;"
    Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 1; RECONFIGURE;"

    $sp_configure_enable_output = @"
{
    "output": "$true"
}
"@
}catch {
    $sp_configure_enable_output = @"
{
    "output": "$false" ,
    "Error": "$_.Exception.Message"
}
"@
}

$result = $sp_configure_enable_output | ConvertFrom-Json

if ($result.output -eq 'True') {
    try {
        # --------------Delete Mapped Drive---------------- 
        $response = (Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC xp_cmdshell 'net use $NASDriveLetter /delete'").output

        $delete_drive = @"
{
        "output": "$true",
        "response": "$response"
}
"@
    }catch {
        $delete_drive = @"
{
        "output": "$false" ,
        "Error": "$_.Exception.Message"
}
"@
    }
    $delete_drive

} elseif ($result.output -eq "False") {
    $output_data = @"
    {
        "output": "$false" ,
        "Error": "$_.Exception.Message"
    }
"@
    $output_data
}  

# try {
# ----------------------------Disable cmdshell----------------------
Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'xp_cmdshell', 0; RECONFIGURE;"
Invoke-Sqlcmd -ServerInstance $SQLInstance -Query "EXEC sp_configure 'show advanced options', 0; RECONFIGURE;"

#     $sp_configure_disable_output = @"
# {
#     "output": "$true"
# }
# "@
# }catch {
#     $sp_configure_disable_output = @"
# {
#     "output": "$false" ,
#     "Error": "$_.Exception.Message"
# }
# "@
# }
# $sp_configure_disable_output