Param (
    $JobName,
    $AlertType,
    $CreatedBy,
    $JobTarget,
    $JobScriptFile,
    $JobInputs,
    $ScheduledJobFrequency,
    $ScheduledJobTime,
    $ScheduledJobStatus,
    $type,
    $connString_db_server,
    $connString_db_name,
    $connString_username,
    $connString_password,
    $insert_id
)

if ($type -eq "INSERT") {
    $queryString = "
    INSERT INTO [automation].[ScheduledJobs]
               ([JobName]
               ,[AlertType]
               ,[JobTarget]
               ,[JobScriptFile]
               ,[JobInputs]
               ,[ScheduledJobFrequency]
               ,[ScheduledJobTime]
               ,[ScheduledJobStatus]
			   ,[CreatedBy]
               ,[CreatedDate])
         VALUES
               ('$JobName'
               ,'$AlertType'
               ,'$JobTarget'
               ,'$JobScriptFile'
               ,'$JobInputs'
               ,'$ScheduledJobFrequency'
               ,'$ScheduledJobTime'
               ,'$ScheduledJobStatus'
               ,'$CreatedBy'
               , getdate());
    SELECT SCOPE_IDENTITY();
    "
} elseif ($type -eq "UPDATE") {
    $queryString="
    UPDATE [automation].[ScheduledJobs] SET
        JobName='$JobName',
		AlertType='$AlertType',
        JobTarget='$JobTarget',
        JobScriptFile='$JobScriptFile',
        JobInputs='$JobInputs',
        ScheduledJobFrequency='$ScheduledJobFrequency',
        ScheduledJobTime='$ScheduledJobTime',
        ScheduledJobStatus='$ScheduledJobStatus',
        CreatedBy='$CreatedBy',
        CreatedDate=getdate()
    WHERE Id='$insert_id';
    "
}

$sql_cmd_output_data = invoke-sqlcmd -ServerInstance "$connString_db_server" -Query "$queryString" -Database "$connString_db_name" -Username "$connString_username" -Password "$connString_password"

return $sql_cmd_output_data
