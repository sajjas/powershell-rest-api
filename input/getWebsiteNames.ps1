Function Get-webPortalSiteNames {
	$web_portal_site = "ey-web-app-asp-net"          	# Change me, if Required     	
	$powershell_scripts_site = "ey-web-scripts"			# Change me, if Required	
	
	
	$sites = @"
{
	"web_portal_site": "$web_portal_site",
	"powershell_scripts_site": "$powershell_scripts_site"
}
"@
	return $sites | ConvertFrom-Json
}	