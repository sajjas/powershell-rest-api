. lib\remoteSessionHelper.ps1
. input\getWebsiteNames.ps1
. output\log_output.ps1
. scripts\get_databases.ps1

Import-Module UniversalDashboard
#Enable-UDLogging

# -----------------------------------------------------------------------------
#          Database Connection Definition
# -----------------------------------------------------------------------------
$VAR_SERVERINSTANCE = "DERUSVMDIGNSQ01.eydev.net\inst1"
$VAR_DATABASE = "EYDBSRE"
$VAR_USERNAME = "DigitalOU"
$VAR_PASSWORD = "Pa!@#word12345"

# -------------------------------------------------------------------------------------------
# 								Get Backup Request Progress 
#	EndPoint	: get-db-backup-progress
#	Method		: POST
# -------------------------------------------------------------------------------------------
$GET_BACKUP_REQUEST_PROGRESS = New-UDEndpoint -Url "/get-db-backup-progress" -Method "POST" -Endpoint {
    
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
	$sql_srv_instance = $NewBody.sql_srv_instance
    $db_name = $NewBody.db_name
	$file_name = "$db_srv_instance-$db_name-backup-progress.log"
	log -Filename "$file_name" -Message "***********************************	Get DB backup Progress Requst Started For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	
	#
	$sites = Get-webPortalSiteNames
	$powershell_scripts_site_name = $sites.powershell_scripts_site
	$Hostname = get-content env:computername
	$Binding = Get-WebSite "$powershell_scripts_site_name" | Get-WebBinding | Select -ExpandProperty bindingInformation
	$Port = $Binding.Trim('*:')
	$host = "$Hostname" + ":" + "$Port"

    ######### Trigger Checking DB Backup Progress #########
	log -Filename "$file_name" -Message "Request : Trigger DB Backup Progress"
    $Script = Invoke-webRequest -Uri "http://$host/db_backup_progress.ps1" -UseBasicParsing -UseDefaultCredentials
    $ScriptBlock = [Scriptblock]::Create($Script.Content)
	$remote_session = Create-PSSession -ServerName $db_srv_instance
    $db_backup_progress = Invoke-Command -Session $remote_session -ScriptBlock $ScriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name, "backup_create_progress"))
    log -Filename "$file_name" -Message "db_backup_progress : $db_backup_progress"
    Remove-PSSession $remote_session
	log -Filename "$file_name" -Message "***********************************	Get DB backup Progress Requst Completed For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
	
	$output = $db_backup_progress | Out-String
	if ([String]::IsNullOrEmpty($output) -eq $true) {
		return 0 | Out-String
	} else {
		return $db_backup_progress
	}
}

# -------------------------------------------------------------------------------------------
# 								Get Databases 
#	EndPoint	: get-databases
#	Method		: POST
# -------------------------------------------------------------------------------------------
$GET_DATABASES= New-UDEndpoint -Url "/list-databases" -Method "POST" -Endpoint {
    
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
    $sql_srv_instance = $NewBody.sql_srv_instance
	$file_name = "$db_srv_instance-list-databases.log"
	log -Filename "$file_name" -Message "***********************************	Fetch Databases Requst Started For -> Remote Server : $db_srv_instance	***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	
	#
	#$sites = Get-webPortalSiteNames
	#$powershell_scripts_site_name = $sites.powershell_scripts_site
	#$Hostname = get-content env:computername
	#$Binding = Get-WebSite "$powershell_scripts_site_name" | Get-WebBinding | Select -ExpandProperty bindingInformation
	#$Port = $Binding.Trim('*:')
	#$host = "$Hostname" + ":" + "$Port"

    ######### Trigger Fetch Databases #########
	log -Filename "$file_name" -Message "Request : Trigger Fetch Databases"
    #$Script = Invoke-webRequest -Uri "http://$host/get_databases.ps1" -UseBasicParsing -UseDefaultCredentials
    #$ScriptBlock = [Scriptblock]::Create($Script.Content)
	$remote_session = Create-PSSession -ServerName $db_srv_instance

	if ($remote_session.Count -eq 1) {
		$databases = Invoke-Command -Session $remote_session -ScriptBlock $ScriptBlock -ArgumentList ($args + @($sql_srv_instance))
	} elseif ($remote_session.Count -eq 2) {
		$Jumpy_session = $remote_session[0]
		$remote_server_session = $remote_session[1]
		$databases = Invoke-Command -Session $Jumpy_session -ScriptBlock {$scriptBlock = [scriptblock]::Create(${using:function:GetDatabases});Invoke-Command -Session $(Get-PSSession) -ScriptBlock $scriptBlock -ArgumentList ($args + @($using:sql_srv_instance))}
		Invoke-Command -Session $Jumpy_session -ScriptBlock {Remove-PSSession $(Get-PSSession)}
	}
	log -Filename "$file_name" -Message "Databases : $databases"
    if ($remote_session.Count -eq 1) {
		Remove-PSSession $remote_session
	} elseif ($remote_session.Count -eq 2) {
		Remove-PSSession $Jumpy_session
	}
	log -Filename "$file_name" -Message "***********************************	Fetch Databases Requst Completed For -> Remote Server : $db_srv_instance	***********************************"
	
	return $databases | out-string
}

# -------------------------------------------------------------------------------------
# ---------------------------- Create Backup With Validation Request ------------------
#	EndPoint	: create-backup-request
#	Method		: POST
# -------------------------------------------------------------------------------------

$CREATE_BACKUP_REQUEST = New-UDEndpoint -Url "/create-backup-request" -Method "POST" -Endpoint {
 
    param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
    $sql_srv_instance = $NewBody.sql_srv_instance
    $db_name = $NewBody.db_name
    $backup_type = $NewBody.backup_type
    $nas_drive_region = $NewBody.nas_drive_region
	$file_name = "$db_srv_instance-$db_name-create-backup.log"
	log -Filename "$file_name" -Message "***********************************	Create Backup Requst Started For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	
	######## Trigger Fetching RestApi Server IIS Site Host and Port ########
	$sites = Get-webPortalSiteNames 
	$web_portal_site_name = $sites.web_portal_site
	$powershell_scripts_site_name = $sites.powershell_scripts_site
	$Hostname = get-content env:computername
	$Binding = Get-WebSite $powershell_scripts_site_name | Get-WebBinding | Select -ExpandProperty bindingInformation
	$Port = $Binding.Trim('*:')
	$host = "$Hostname" + ":" + "$Port"
	log -Filename "$file_name" -Message "RestApi Server IIS Site Host and Port : $host"
	
	######## Trigger Fetching ConnectionString values from web portal web.config file in IIS ########
	log -Filename "$file_name" -Message "Request : Derive ConnectionString values from web portal web.config file in IIS"
	$Script = Invoke-webRequest -Uri "http://$host/web_portal_db_connect.ps1" -UseBasicParsing -UseDefaultCredentials
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$cmd_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $scriptBlock -ArgumentList ($args + @($web_portal_site_name))
	log -Filename "$file_name" -Message "ConnectionString Details : $cmd_output"
	
	######## Trigger Fetching NAS Drive Path from NASShare Table ########
	log -Filename "$file_name" -Message "Request : Derive NAS Drive Path from NASShare Table"
	$Script = Invoke-webRequest -Uri "http://$host/select_nas_path.ps1" -UseBasicParsing -UseDefaultCredentials
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$nas_drive_path_sql_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $scriptBlock -ArgumentList ($args + @($nas_drive_region, $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password))
	$nas_path_output = $nas_drive_path_sql_output | ConvertFrom-Json
	$nas_drive_path_json_output = $nas_path_output.backupPath | ConvertTo-Json
	$nas_drive_path_serialized = $nas_drive_path_json_output.Trim('"').Replace('\\', '\')	
	# ----------- Derived Input Parameter DRIVE_PATH --------------
	$nas_drive_path = $nas_drive_path_serialized
	log -Filename "$file_name" -Message "NAS Drive Path : $nas_drive_path"
	
	# Create Remote Session to DB Server Instance
	$remote_session = Create-PSSession -ServerName $db_srv_instance
	
	######### Trigger checking DB Size ########
	log -Filename "$file_name" -Message "Request : Trigger checking DB Size <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	$Script = Invoke-webRequest -Uri "http://$host/db_size_check.ps1" -UseDefaultCredentials -UseBasicParsing
	$scriptBlock = [scriptblock]::Create($Script.Content)
	#$db_size = Invoke-Command -Session $remote_session -ScriptBlock { Get-SqlDatabase -Name "$using:db_name" -ServerInstance "$using:sql_srv_instance" | Select-Object -Property @{label="Size";expression={[math]::round($_.Size)}} | ConvertTo-Json}
	$db_size = Invoke-Command -Session $remote_session -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name))
	#$dbobj = $db_size | ConvertFrom-Json
	if ([String]::IsNullOrEmpty($db_size) -eq $true) {
		return "DB Size returned Empty Value $db_size"
	} elseif ( $db_size -gt 500000) {
		return "$false"
	}
	log -Filename "$file_name" -Message "DB Size (MB) : $db_size"
	
	######## Trigger checking last backup details if Backup Request is DIFFERENTIAL ########
	if ($backup_type -eq "DIFF") {
		log -Filename "$file_name" -Message "Request : Trigger checking last backup details if Backup Request is DIFFERENTIAL <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
		######### Trigger Checking Last backup validity ########
		$Script = Invoke-webRequest -Uri "http://$host/db_backup_history.ps1" -UseDefaultCredentials -UseBasicParsing
		$scriptBlock = [scriptblock]::Create($Script.Content)
		$db_backup_history = Invoke-Command -Session $remote_session -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name))
		
		$data = $db_backup_history | ConvertFrom-Json
		$physical_device_name = $data.data.actual_output[0][0].physical_device_name | ConvertTo-Json
		$backupset_name = $data.data.actual_output[0][0].backupset_name | ConvertTo-Json
		if ([String]::IsNullOrEmpty($backupset_name) -eq $true) {
			$backupset_name="Empty"
		}
		
		$Script = Invoke-webRequest -Uri "http://$host/test_guid_veeam.ps1" -UseDefaultCredentials -UseBasicParsing
		$scriptBlock = [scriptblock]::Create($Script.Content)
		$test_last_backup_result = Invoke-Command -Session $remote_session -ScriptBlock $scriptBlock -ArgumentList ($args + @($physical_device_name, $backupset_name))
		
		if ($test_last_backup_result -eq "veeam") {
			return "veeam"
		} elseif ($test_last_backup_result -eq "non-dou") {
			return "non-dou"
		}
		log -Filename "$file_name" -Message "Last Backup Type : $test_last_backup_result"
	}
	
	######### Trigger Get Drive Letter #########
	log -Filename "$file_name" -Message "Request : Trigger Get Drive Letter <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	$drive_letter_output = Invoke-Command -Session $remote_session -ScriptBlock {(68..90 | %{$L=[char]$_; if ((gdr).Name -notContains $L) {$L}})[0]}
	if ([String]::IsNullOrEmpty($drive_letter_output) -eq $true) {
		return "Empty Drive Letter"
	} else {
		# ----------- Derived Input Parameter DRIVE_LETTER --------------
		$drive_letter = $drive_letter_output + ':'
	}
	log -Filename "$file_name" -Message "Drive Letter : $drive_letter"
	
	######### Trigger Create NAS Drive #########
	log -Filename "$file_name" -Message "Request : Trigger Create NAS Drive <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	$Script = Invoke-webRequest -Uri "http://$host/create_nas_drive.ps1" -UseDefaultCredentials -UseBasicParsing
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$create_nas_drive_output = Invoke-Command -Session $remote_session -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $drive_letter, $nas_drive_path))
	$data = $create_nas_drive_output | ConvertFrom-Json
	if ($data.output -eq $false) { 
		return $create_nas_drive_output
	}
	log -Filename "$file_name" -Message "Create NAS Drive Output : $create_nas_drive_output"
	
	######### Trigger Split count for backup ########
	log -Filename "$file_name" -Message "Request : Trigger Split count for backup <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	$Script = Invoke-webRequest -Uri "http://$host/last_backup_file_size.ps1" -UseDefaultCredentials -UseBasicParsing
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$last_backup_file_size_result = Invoke-Command -Session $remote_session -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name))
	$output = $last_backup_file_size_result | ConvertFrom-Json
	$BackupSize_GB = $output.data.actual_output | Select-Object -first 1
	try {
		if ( $BackupSize_GB.BackupSize_GB -le 10 ) {
			$final_splits_count = 1
		} else {
			$splits_count = $BackupSize_GB.BackupSize_GB / 10
			if ( $splits_count -gt 10 ) {
				$final_splits_count =  10
			} else {
				$final_splits_count = [math]::floor($splits_count)
			}
		}
	} catch {
		return $_.Exception.Message
	}
	# ----------- Derived Input Parameter DB-SPLITS --------------
	$db_splits = $final_splits_count | ConvertTo-Json
	log -Filename "$file_name" -Message "Backup Files Split Count : $db_splits"
	
	######### Trigger DB Backup #########
	log -Filename "$file_name" -Message "Request : Trigger DB Backup <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	# ----------- Derived Input Parameter DB BACKUP DATE TIME STARTED --------------
	$date_time_started = "{0:yyyy-MM-dd HH:mm:ss}" -f (get-date)
	$Script = Invoke-webRequest -Uri "http://$host/backup_db.ps1" -UseDefaultCredentials -UseBasicParsing
	$ScriptBlock = [Scriptblock]::Create($Script.Content)
	$db_backup_result = Invoke-Command -Session $remote_session -ScriptBlock $ScriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name, $backup_type, $drive_letter, $db_splits))
	# ----------- Derived Input Parameter DB BACKUP DATE TIME ENDED --------------
	$date_time_completed = "{0:yyyy-MM-dd HH:mm:ss}" -f (get-date)
	
	if ($db_backup_result.output -eq $false) {
		return $db_backup_result | ConvertTo-Json
	}
	log -Filename "$file_name" -Message "DB Backup Request Output  : $db_backup_result"
	
	######### Trigger Delete NAS Drive #########
	log -Filename "$file_name" -Message "Request : Trigger Delete NAS Drive <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	$Script = Invoke-webRequest -Uri "http://$host/delete_nas_drive.ps1" -UseDefaultCredentials -UseBasicParsing
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$delete_nas_drive = Invoke-Command -Session $remote_session -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $drive_letter))
	log -Filename "$file_name" -Message "Delete NAS Drive Output  : $delete_nas_drive"
	
	######### Trigger Insert Backup Info into Events Table #########
	log -Filename "$file_name" -Message "Request : Trigger Insert Backup Info into Events Table <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	$process_id = $NewBody.process_id
	$server_id = $NewBody.server_id
	$user_id = $NewBody.user_id
	$process_parameters = $NewBody.process_parameters
	$service_now_incident_id = $NewBody.service_now_incident_id
	$process_status_id = $NewBody.process_status_id
	$creation_date = $NewBody.creation_date
	
	$Script = Invoke-webRequest -Uri "http://$host/insert_event_table.ps1" -UseDefaultCredentials -UseBasicParsing
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$insert_cmd_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $scriptBlock -ArgumentList ($args + @($cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password, $process_id, $server_id, $user_id, $date_time_started, $date_time_completed, $process_parameters, $service_now_incident_id, $process_status_id, $creation_date))
	log -Filename "$file_name" -Message "Insert Event Table Output  : $insert_cmd_output"
	
	Remove-PSSession $remote_session
	log -Filename "$file_name" -Message "***********************************	Create Backups Request Completed Successfully For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
    return "1"
}

# -------------------------------------------------------------------------------------
# ---------------------------- Create Backup Without Validation Request ------------------
#	EndPoint	: create-backup-request
#	Method		: POST
# -------------------------------------------------------------------------------------
$CREATE_BACKUP_WITHOUT_VALIDATION = New-UDEndpoint -Url "/create-backup" -Method "POST" -Endpoint {

    param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
    $sql_srv_instance = $NewBody.sql_srv_instance
    $db_name = $NewBody.db_name
    $backup_type = $NewBody.backup_type
    $nas_drive_region = $NewBody.nas_drive_region
	$user_id = $NewBody.user_id
	$service_now_incident_id = $NewBody.service_now_incident_id
	$type_id =  "DB-BACKUP"
	
	#--------------------- LOGGING --------------------
	$file_name = "$db_srv_instance-$db_name-create-backup.log"
	log -Filename "$file_name" -Message "***********************************	Create Backup Requst Started For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	
	######## Trigger Fetching RestApi Server IIS Site Host and Port ########
	$sites = Get-webPortalSiteNames 
	$web_portal_site_name = $sites.web_portal_site
	$powershell_scripts_site_name = $sites.powershell_scripts_site
	$Hostname = get-content env:computername
	$Binding = Get-WebSite $powershell_scripts_site_name | Get-WebBinding | Select -ExpandProperty bindingInformation
	$Port = $Binding.Trim('*:')
	$host = "$Hostname" + ":" + "$Port"
	log -Filename "$file_name" -Message "RestApi Server IIS Site Host and Port : $host"
	
	######## Trigger Fetching ConnectionString values from web portal web.config file in IIS ########
	log -Filename "$file_name" -Message "Request : Derive ConnectionString values from web portal web.config file in IIS"
	$Script = Invoke-webRequest -Uri "http://$host/web_portal_db_connect.ps1" -UseBasicParsing -UseDefaultCredentials
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$cmd_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $scriptBlock -ArgumentList ($args + @($web_portal_site_name))
	log -Filename "$file_name" -Message "ConnectionString Details : $cmd_output"
	
	######## Trigger Fetching NAS Drive Path from NASShare Table ########
	log -Filename "$file_name" -Message "Request : Derive NAS Drive Path from NASShare Table"
	$Script = Invoke-webRequest -Uri "http://$host/select_nas_path.ps1" -UseBasicParsing -UseDefaultCredentials
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$nas_drive_path_sql_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $scriptBlock -ArgumentList ($args + @($nas_drive_region, $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password))
	$nas_path_output = $nas_drive_path_sql_output | ConvertFrom-Json
	$nas_drive_path_json_output = $nas_path_output.backupPath | ConvertTo-Json
	$nas_drive_path_serialized = $nas_drive_path_json_output.Trim('"').Replace('\\', '\')	
	# ----------- Derived Input Parameter DRIVE_PATH --------------
	$nas_drive_path = $nas_drive_path_serialized
	log -Filename "$file_name" -Message "NAS Drive Path : $nas_drive_path"
	
	# ----------- Insert ServiceRequest Table with Request STARTED Status -----------
	$info = @"
{
		"backup_type": "$backup_type",
		"nas_drive_region": "$nas_drive_region",
		"nas_drive_path": "$nas_drive_path" 
}
"@
	$status_id = "STARTED"
	$ServiceRequest_Script = Invoke-webRequest -Uri "http://$host/update_service_request_table.ps1" -UseDefaultCredentials -UseBasicParsing
	$ServiceRequest_scriptBlock = [scriptblock]::Create($ServiceRequest_Script.Content)
	$ServiceRequest_Id = Invoke-Command -ComputerName $Hostname -ScriptBlock $ServiceRequest_scriptBlock -ArgumentList ($args + @("INSERT", 0, $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, "NULL"))
	log -Filename "$file_name" -Message "Insert ServiceRequest Table Output  : $ServiceRequest_Id"
	
	# Create Remote Session to DB Server Instance
	$remote_session = Create-PSSession -ServerName $db_srv_instance
	
	######### Trigger Get Drive Letter #########
	log -Filename "$file_name" -Message "Request : Trigger Get Drive Letter <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	$drive_letter_output = Invoke-Command -Session $remote_session -ScriptBlock {(68..90 | %{$L=[char]$_; if ((gdr).Name -notContains $L) {$L}})[0]}
	if ([String]::IsNullOrEmpty($drive_letter_output) -eq $true) {
		# ----------- Update ServiceRequest Table with Request ERROR Status -----------
		$status_id = "ERROR"
		$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $ServiceRequest_scriptBlock -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, "Empty Drive Letter"))
		log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
		return
	} else {
		# ----------- Derived Input Parameter DRIVE_LETTER --------------
		$drive_letter = $drive_letter_output + ':'
	}
	log -Filename "$file_name" -Message "Drive Letter : $drive_letter"
	
	######### Trigger Create NAS Drive #########
	log -Filename "$file_name" -Message "Request : Trigger Create NAS Drive <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	$Script = Invoke-webRequest -Uri "http://$host/create_nas_drive.ps1" -UseDefaultCredentials -UseBasicParsing
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$create_nas_drive_output = Invoke-Command -Session $remote_session -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $drive_letter, $nas_drive_path))
	$data = $create_nas_drive_output | ConvertFrom-Json
	if ($data.output -eq $false) { 
		# ----------- Update ServiceRequest Table with Request ERROR Status -----------
		$status_id = "ERROR"
		$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $ServiceRequest_scriptBlock -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, $create_nas_drive_output))
		log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
		return
	}
	log -Filename "$file_name" -Message "Create NAS Drive Output : $create_nas_drive_output"
	
	######### Trigger Split count for backup incase of FULL Backup Request ########
	if ($backup_type -eq "FULL"){
		log -Filename "$file_name" -Message "Request : Trigger Split count for backup <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
		$Script = Invoke-webRequest -Uri "http://$host/last_backup_file_size.ps1" -UseDefaultCredentials -UseBasicParsing
		$scriptBlock = [scriptblock]::Create($Script.Content)
		$last_backup_file_size_result = Invoke-Command -Session $remote_session -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name))
		$output = $last_backup_file_size_result | ConvertFrom-Json
		$BackupSize_GB = $output.data.actual_output | Select-Object -first 1
		try {
			if ( $BackupSize_GB.BackupSize_GB -le 10 ) {
				$final_splits_count = 1
			} else {
				$splits_count = $BackupSize_GB.BackupSize_GB / 10
				if ( $splits_count -gt 10 ) {
					$final_splits_count =  10
				} else {
					$final_splits_count = [math]::floor($splits_count)
				}
			}
		} catch {
			# ----------- Update ServiceRequest Table with Request ERROR Status -----------
			$status_id = "ERROR"
			$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $ServiceRequest_scriptBlock -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, $_.Exception.Message))
			log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
			return
		}
	} else {
		$final_splits_count = 1
	}
	# ----------- Derived Input Parameter DB-SPLITS --------------
	$db_splits = $final_splits_count | ConvertTo-Json
	log -Filename "$file_name" -Message "Backup Files Split Count : $db_splits"
	
	######### Trigger DB Backup #########
	log -Filename "$file_name" -Message "Request : Trigger DB Backup <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	
	# ----------- Update ServiceRequest Table with Request INPROGRESS Status -----------
	$status_id = "INPROGRESS"
	$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $ServiceRequest_scriptBlock -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, "NULL"))
	log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
	$Script = Invoke-webRequest -Uri "http://$host/backup_db.ps1" -UseDefaultCredentials -UseBasicParsing
	$ScriptBlock = [Scriptblock]::Create($Script.Content)
	$db_backup_result = Invoke-Command -Session $remote_session -ScriptBlock $ScriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name, $backup_type, $drive_letter, $db_splits))
	
	if ($db_backup_result.output -eq $false) {
		$error_output = $db_backup_result | ConvertTo-Json
		$status_id = "ERROR"
		# ----------- Update ServiceRequest Table with Request ERROR Status -----------
		$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $ServiceRequest_scriptBlock -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, $error_output))
		log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
		return
	} else {
		$status_id = "COMPLETED"
		# ----------- Update ServiceRequest Table with Request COMPLETED Status -----------
		$ServiceRequest_update_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $ServiceRequest_scriptBlock -ArgumentList ($args + @("UPDATE", $ServiceRequest_Id, $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password, $type_id, $status_id, $db_srv_instance, $sql_srv_instance, $db_name, $service_now_incident_id, $info, $user_id, "NULL"))
		log -Filename "$file_name" -Message "Update ServiceRequest Table Output  : $ServiceRequest_update_output"
	}
	log -Filename "$file_name" -Message "DB Backup Request Output  : $db_backup_result"
	
	######### Trigger Delete NAS Drive #########
	log -Filename "$file_name" -Message "Request : Trigger Delete NAS Drive <-> Remote Server : $db_srv_instance <-> DB Name : $db_name"
	$Script = Invoke-webRequest -Uri "http://$host/delete_nas_drive.ps1" -UseDefaultCredentials -UseBasicParsing
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$delete_nas_drive = Invoke-Command -Session $remote_session -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $drive_letter))
	log -Filename "$file_name" -Message "Delete NAS Drive Output  : $delete_nas_drive"
	
	Remove-PSSession $remote_session
	log -Filename "$file_name" -Message "***********************************	Create Backups Request Completed Successfully For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
}

# ---------------------------------------------------------------------------------------
# 								DB Backup History per date range Check Request
#	EndPoint	: db-backup-history
#	Method		: POST 
# ---------------------------------------------------------------------------------------
$GET_DB_BACKUP_HISTORY = New-UDEndpoint -Url "/db-backup-history-with-range" -Method "POST" -Endpoint {
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
	$sql_srv_instance = $NewBody.sql_srv_instance
	$db_name = $NewBody.db_name
	$start_date = $NewBody.start_date
	$end_date = $NewBody.end_date
	$file_name = "$db_srv_instance-db-backup-history.log"
	log -Filename "$file_name" -Message "***********************************	DB Backup Request History Check Request Started	For -> Remote Server : $db_srv_instance and -> DB Name : $db_name  ***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	
	#
	$sites = Get-webPortalSiteNames
	$powershell_scripts_site_name = $sites.powershell_scripts_site
	$Hostname = get-content env:computername
	$Binding = Get-WebSite "$powershell_scripts_site_name" | Get-WebBinding | Select -ExpandProperty bindingInformation
	$Port = $Binding.Trim('*:')
	$host = "$Hostname" + ":" + "$Port"

    ######### Trigger Checking DB Backup Request History #########
	log -Filename "$file_name" -Message "Request : Trigger Checking DB Backup Request History"
	$remote_session = Create-PSSession -ServerName $db_srv_instance
	$Script = Invoke-webRequest -Uri "http://$host/db_backup_history_with_date_range.ps1" -UseDefaultCredentials -UseBasicParsing
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$db_backup_history = Invoke-Command -Session $remote_session -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name, $start_date, $end_date))
	Remove-PSSession $remote_session
	
	$processes_data = $db_backup_history | ConvertFrom-Json
	if ($processes_data.output -eq $true) {
		$backup_histrory = $processes_data.data | ConvertTo-Json
		return $backup_histrory
	} else {
		return $db_backup_history
	}
}

# ---------------------------------------------------------------------------------------
# 								Get DBs, Size and LastBackupDate Request
#	EndPoint	: get-dbs-size-lastbkp-data
#	Method		: POST 
# ---------------------------------------------------------------------------------------
$GET_DBS_SIZE_LASTBKP_DATA = New-UDEndpoint -Url "/get-dbs-size-lastbkp-data" -Method "POST" -Endpoint {
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
	$sql_srv_instance = $NewBody.sql_srv_instance
	$file_name = "$db_srv_instance-dbs-size-lastbkp-data.log"
	log -Filename "$file_name" -Message "***********************************	Get DBs, Size and LastBackupDate Request Started	For -> Remote Server : $db_srv_instance  ***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	
	#
	$sites = Get-webPortalSiteNames
	$powershell_scripts_site_name = $sites.powershell_scripts_site
	$Hostname = get-content env:computername
	$Binding = Get-WebSite "$powershell_scripts_site_name" | Get-WebBinding | Select -ExpandProperty bindingInformation
	$Port = $Binding.Trim('*:')
	$host = "$Hostname" + ":" + "$Port"

    ######### Trigger Getting DBs, Size and LastBackupDate #########
	log -Filename "$file_name" -Message "Request : Trigger Getting DBs, Size and LastBackupDate"
	$remote_session = Create-PSSession -ServerName $db_srv_instance
	$test_session = $remote_session | Format-List
	log -Filename "$file_name" -Message "Session : $test_session"
	$Script = Invoke-webRequest -Uri "http://$host/get_db_name_size_lastbackupdate.ps1" -UseDefaultCredentials -UseBasicParsing
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$dbs_size_lastbkp_data = Invoke-Command -Session $remote_session -ScriptBlock $scriptBlock -ArgumentList ($args + @($sql_srv_instance))
	Remove-PSSession $remote_session
	Exit-PSSession
	$processes_data = $dbs_size_lastbkp_data | ConvertFrom-Json
	if ($processes_data.output -eq $true) {
		$backup_histrory = $processes_data.data | ConvertTo-Json
		log -Filename "$file_name" -Message "Response : $backup_histrory"
		return $backup_histrory
	} else {
		log -Filename "$file_name" -Message "Response : $dbs_size_lastbkp_data"
		return $dbs_size_lastbkp_data
	}
}

# -------------------------------------------------------------------------------------------
# 								DB Backup Request cancel 
#	EndPoint	: db-backup-request-cancel
#	Method		: POST
# -------------------------------------------------------------------------------------------
$GET_BACKUP_REQUEST_CANCEL = New-UDEndpoint -Url "/db-backup-request-cancel" -Method "POST" -Endpoint {
    
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
	$sql_srv_instance = $NewBody.sql_srv_instance
    $db_name = $NewBody.db_name
	$file_name = "$db_srv_instance-$db_name-backup-request-cancel.log"
	log -Filename "$file_name" -Message "***********************************	DB backup Request Cancel Started For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	
	#
	$sites = Get-webPortalSiteNames
	$powershell_scripts_site_name = $sites.powershell_scripts_site
	$Hostname = get-content env:computername
	$Binding = Get-WebSite "$powershell_scripts_site_name" | Get-WebBinding | Select -ExpandProperty bindingInformation
	$Port = $Binding.Trim('*:')
	$host = "$Hostname" + ":" + "$Port"

    ######### Trigger DB Backup Request Cancel #########
	log -Filename "$file_name" -Message "Request : Trigger DB Backup Request Cancel"
    $Script = Invoke-webRequest -Uri "http://$host/db_backup_progress.ps1" -UseBasicParsing -UseDefaultCredentials
    $ScriptBlock = [Scriptblock]::Create($Script.Content)
	$remote_session = Create-PSSession -ServerName $db_srv_instance
    $db_backup_request_cancel = Invoke-Command -Session $remote_session -ScriptBlock $ScriptBlock -ArgumentList ($args + @($sql_srv_instance, $db_name, "backup_create_cancel"))
    log -Filename "$file_name" -Message "db_backup_request_cancel_output : $db_backup_request_cancel"
    Remove-PSSession $remote_session
	log -Filename "$file_name" -Message "***********************************	DB backup Requst Cancel Completed For -> Remote Server : $db_srv_instance and -> DB Name : $db_name	***********************************"
	
	return $db_backup_request_cancel
}

# ---------------------------------------------------------------------------------------
# 								Scheduled Job data Insert into Table
#	EndPoint	: schedule-job-table-insert-api
#	Method		: POST
# ---------------------------------------------------------------------------------------
$SCHEDULE_JOB_INSERT_API = New-UDEndpoint -Url "/schedule-job-table-insert-api" -Method "POST" -Endpoint {

	param($Body)

    # Validate all JSON fields are containing valid JSON
    try {
        $NewBody = ConvertFrom-Json $Body -ErrorAction Stop;
        $validJson = $true;
    } catch {
        $validJson = $false;
    }

    if ($validJson -eq $false) {
        return "Provided text is not a valid JSON string"
    }
	
	# ----------- Input Parameters --------------
    $Job_Name = $NewBody.Job_Name
	$Alert_Type = $NewBody.Alert_Type
	$Created_By = $NewBody.Created_By
    $Target_Servers = $NewBody.Target_Servers | convertTo-Json
    $Job_Inputs = $NewBody.Job_Inputs
	$Job_Frequency = $NewBody.Job_Frequency
    $Job_Time = $NewBody.Job_Time
	
	if ($Job_Frequency -eq "MONTHLY") {
		$Job_Time = $Job_Time.Monthly | ConvertTo-Json
	} elseif ($Job_Frequency -eq "WEEKLY") {
		$Job_Time = $Job_Time.Weekly | ConvertTo-Json
	} elseif ($Job_Frequency -eq "DAILY") {
		$Job_Time = $Job_Time.Daily | ConvertTo-Json
	} elseif ($Job_Frequency -eq "HOURLY") {
		$Job_Time = $Job_Time.Hourly | ConvertTo-Json
	}
    
	$file_name = "$Job_Name.log"
	log -Filename "$file_name" -Message "***********************************	Scheduled Job Data Insert/Update into Table Started For -> Job : $Job_Name	***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	 
    ######## Trigger Fetching RestApi Server IIS Site Host and Port ########
	$sites = Get-webPortalSiteNames
	$web_portal_site_name = $sites.web_portal_site
	$powershell_scripts_site_name = $sites.powershell_scripts_site
	$Hostname = get-content env:computername
	$Binding = Get-WebSite $powershell_scripts_site_name | Get-WebBinding | Select-Object -ExpandProperty bindingInformation
	$PhysicalPath = (Get-WebSite $powershell_scripts_site_name).PhysicalPath
	$Port = $Binding.Trim('*:')
	$host = "$Hostname" + ":" + "$Port"
	log -Filename "$file_name" -Message "RestApi Server IIS Site Host and Port : $host"
	
	######## Trigger Fetching ConnectionString values from web portal web.config file in IIS ########
	log -Filename "$file_name" -Message "Request : Derive ConnectionString values from web portal web.config file in IIS"
	$Script = Invoke-webRequest -Uri "http://$host/web_portal_db_connect.ps1" -UseBasicParsing -UseDefaultCredentials
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$cmd_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $scriptBlock -ArgumentList ($args + @($web_portal_site_name))
	log -Filename "$file_name" -Message "ConnectionString Details : $cmd_output"

	######## Trigger Fetching Powershell script file name from LookupMaster Table ########
	$script_name_output = Invoke-Command -ComputerName $Hostname -ScriptBlock {invoke-sqlcmd -serverInstance $using:cmd_output.db_server -query "select LookupName from [automation].[LookupMaster] where LookupType = 'JOB_SCRIPT_NAME' And LookupCode = '$using:Alert_Type'" -Database $using:cmd_output.database -Username $using:cmd_output.username -Password $using:cmd_output.password} 
	$Job_Script_File = $script_name_output.LookupName
	
	$Script = Invoke-webRequest -Uri "http://$host/insert_scheduled_job_table.ps1" -UseBasicParsing -UseDefaultCredentials
	$scriptBlock = [scriptblock]::Create($Script.Content)
	log -Filename "$file_name" -Message "Request Arguments:	$Job_Name, $Alert_Type, $Created_By, $Target_Servers, $Job_Script_File, $Job_Inputs, $Job_Frequency, $Job_Time"
	if ($Job_Inputs.PSobject.Properties.Name -contains "scheduled_job_id") {
		######### Trigger Scheduled Job Data Update into Table #########
		$update_row_id = $Job_Inputs.scheduled_job_id
		$Job_Inputs = $NewBody.Job_Inputs | ConvertTo-Json
		log -Filename "$file_name" -Message "Request : Trigger Scheduled Job Data Update into Table"
		$update_scheduled_job_table_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $ScriptBlock -ArgumentList ($args + @($Job_Name, $Alert_Type, $Created_By, $Target_Servers, $Job_Script_File, $Job_Inputs, $Job_Frequency, $Job_Time, "CREATED", "UPDATE", $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password, $update_row_id))
        log -Filename "$file_name" -Message "Response : $update_scheduled_job_table_output"
		if ([String]::IsNullOrEmpty($update_scheduled_job_table_output) -eq $true) {
			return @{output = $true; Success_Message = $update_row_id} | ConvertTo-Json
		} else {
			return @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
		}
	} else {
		######### Trigger Scheduled Job Data Insert and update into Table #########
		log -Filename "$file_name" -Message "Request : Trigger Scheduled Job Data Insert into Table"
		$insert_scheduled_job_table_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $ScriptBlock -ArgumentList ($args + @($Job_Name, $Alert_Type, $Created_By, $Target_Servers, $Job_Script_File, $Job_Inputs, $Job_Frequency, $Job_Time, "CREATED", "INSERT", $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password, "NULL"))
		$insert_scheduled_job_table_output_parsed = $insert_scheduled_job_table_output.Column1
		log -Filename "$file_name" -Message "Response : $insert_scheduled_job_table_output_parsed"
		if ([String]::IsNullOrEmpty($insert_scheduled_job_table_output_parsed) -eq $true) {
			return @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
		} else {
			$NewBody.Job_Inputs | add-member -Name "scheduled_job_id" -value $insert_scheduled_job_table_output_parsed -MemberType NoteProperty
			$Job_Inputs = $NewBody.Job_Inputs | ConvertTo-Json
			$update_scheduled_job_table_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $ScriptBlock -ArgumentList ($args + @($Job_Name, $Alert_Type, $Created_By, $Target_Servers, $Job_Script_File, $Job_Inputs, $Job_Frequency, $Job_Time, "CREATED", "UPDATE", $cmd_output.db_server, $cmd_output.database, $cmd_output.username, $cmd_output.password, $insert_scheduled_job_table_output_parsed))
			log -Filename "$file_name" -Message "Response : $update_scheduled_job_table_output"
			if ([String]::IsNullOrEmpty($update_scheduled_job_table_output) -eq $true) {
				return @{output = $true; Success_Message = $insert_scheduled_job_table_output_parsed} | ConvertTo-Json
			} else {
				return @{output = $false; Fail_Message = $_.Exception.Message} | ConvertTo-Json
			}
		}
	}
    log -Filename "$file_name" -Message "***********************************	Scheduled Job Data Insert/Update into Table Completed For -> Job : $Job_Name	***********************************"
}
	
# ---------------------------------------------------------------------------------------
# 								DB Server Clustered Mode Check Request
#	EndPoint	: clustered-mode-check
#	Method		: POST 
# ---------------------------------------------------------------------------------------
$GET_DB_SERVER_CLUSTERED_MODE_CHECK = New-UDEndpoint -Url "/clustered-mode-check" -Method "POST" -Endpoint {
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	
	# ----------- Input Parameters --------------
    $db_srv_instance = $NewBody.db_srv_instance
	$sql_srv_instance = $NewBody.sql_srv_instance
	$file_name = "$db_srv_instance-clustered-mode.log"
	log -Filename "$file_name" -Message "***********************************	DB Server Clustered Mode Check Request Started	  ***********************************"
	log -Filename "$file_name" -Message "Request Body : $Body"
	
	#
	$sites = Get-webPortalSiteNames
	$powershell_scripts_site_name = $sites.powershell_scripts_site
	$Hostname = get-content env:computername
	$Binding = Get-WebSite "$powershell_scripts_site_name" | Get-WebBinding | Select -ExpandProperty bindingInformation
	$Port = $Binding.Trim('*:')
	$host = "$Hostname" + ":" + "$Port"

    ######### Trigger Checking DB Server Clustered Mode #########
	log -Filename "$file_name" -Message "Request : Trigger Checking DB Server Clustered Mode"
	$remote_session = Create-PSSession -ServerName $db_srv_instance
	$Script = Invoke-webRequest -Uri "http://$host/is_clustered_check.ps1" -UseBasicParsing -UseDefaultCredentials
    $ScriptBlock = [Scriptblock]::Create($Script.Content)
    $clustered_mode = Invoke-Command -Session $remote_session -ScriptBlock $ScriptBlock -ArgumentList ($args + @($sql_srv_instance))
    log -Filename "$file_name" -Message "DB Server Clustered Mode Status : $clustered_mode"
    Remove-PSSession $remote_session
	log -Filename "$file_name" -Message "***********************************	DB Server Clustered Mode Check Request Completed	***********************************"
	
	return $clustered_mode
}

# ---------------------------------------------------------------------------------------
# 								Get Domain Credentials Request
#	EndPoint	: get-domain-credentials
#	Method		: GET 
# ---------------------------------------------------------------------------------------
$GET_DOMAIN_CREDENTIALS = New-UDEndpoint -Url "/get-domain-credentials" -Method "GET" -Endpoint {
	$site_name = "ey-web-rest-api-server"
    
    $domain_name = $Request.Headers["domain_name"]

	$webconfig = Get-WebConfigFile "IIS:\Sites\$site_name"
	$xml = [xml](Get-Content $webconfig.name)
	$root = $xml.get_DocumentElement();
	$domain = $root.appSettings.credentials.domains.domain | where {$_.name -eq "$domain_name"}
	
	if($domain -eq $null) {
		return "Credentials not Fount For Domain: $domain_name"
	} else {
		return $domain
	}
}

# ------------------------------------------------------------------------------------------
# 								Delete Domain Credentials Request
#	EndPoint	: delete-domain-credentials
#	Method		: POST
# ------------------------------------------------------------------------------------------
$DELETE_DOMAIN_CREDENTIALS = New-UDEndpoint -Url "/delete-domain-credentials" -Method "POST" -Endpoint {
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	# ----------- Input Parameters --------------
    $domain_name = $NewBody.domain_name
	
	$site_name = "ey-web-rest-api-server"

	$webconfig = Get-WebConfigFile "IIS:\Sites\$site_name"
	$xml = [xml](Get-Content $webconfig.name)
	$root = $xml.get_DocumentElement();
	$domain = $root.appSettings.credentials.domains.domain | where {$_.name -eq "$domain_name"}
	
	if($domain -eq $null) {
		return "Credentials not Fount For Domain: $domain_name"
	} else {
		$xml.SelectNodes("//appSettings/credentials/domains/domain") | Where-Object { $_.name -eq '$domain_name' } | %{ $_.ParentNode.RemoveChild($_) }
		$xml.save($webconfig)
		return "Credentials Deleted Successfully For Domain: $domain_name"
	}
}

# ------------------------------------------------------------------------------------------
# 								Create Domain Credentials Request
#	EndPoint	: create-domain-credentials
#	Method		: POST 
# ------------------------------------------------------------------------------------------
$CREATE_DOMAIN_CREDENTIALS = New-UDEndpoint -Url "/create-domain-credentials" -Method "POST" -Endpoint {
	
	param($Body)
    $NewBody = $Body | ConvertFrom-Json 
	# ----------- Input Parameters --------------
    $domain_name = $NewBody.domain_name
    $username = $NewBody.username
	$password = $NewBody.password
	
	$site_name = "ey-web-rest-api-server"

	$webconfig = Get-WebConfigFile "IIS:\Sites\$site_name"
	$xml = [xml](Get-Content $webconfig.name)
	$root = $xml.get_DocumentElement();
	$domain = $root.appSettings.credentials.domains.domain | where {$_.name -eq "$domain_name"}
	
	if($domain -eq $null) {
		$domain_tag = $xml.createelement("domain")
		$domain_tag.setattribute("name", "$domain_name")

		$username_tag = $xml.createelement("username")	
		$username_tag.innertext = "$username"
		$domain_tag.appendchild($username_tag)

		$password_tag = $xml.createelement("password")	
		$password_tag.innertext = "$password"
		$domain_tag.appendchild($password_tag)

		$xml.selectsinglenode("configuration/appSettings/credentials/domains").appendchild($domain_tag)
		$xml.save($webconfig)
		
		return "Credentials Stored Successfully for Domain: $domain_name"
	} else {
		return "Domain Credentials Already Exists"
	}	
}

$GET_CONN_STRING = New-UDEndpoint -Url "/connstring" -Method "GET" -Endpoint {
	
	$site_name = "ey-web-app-asp-net"
	$scripts_site_name = "ey-web-scripts"
	$Hostname = get-content env:computername
	$Binding = Get-WebSite "$scripts_site_name" | Get-WebBinding | Select -ExpandProperty bindingInformation
	$Port = $Binding.Trim('*:')
	$host = "$Hostname" + ":" + "$Port"
	
	$Script = Invoke-webRequest -Uri "http://$host/event_db_connect.ps1" -UseDefaultCredentials
	$scriptBlock = [scriptblock]::Create($Script.Content)
	$cmd_output = Invoke-Command -ComputerName $Hostname -ScriptBlock $scriptBlock -ArgumentList ($args + @($site_name))
	
	return $cmd_output.db_server
}

# -------------------------------------------------------------------------------------
# 								Trigger Rest Api Server 
# -------------------------------------------------------------------------------------
#$Endpoints = $GET_BACKUP_REQUEST_PROGRESS, $GET_DATABASES, $CREATE_BACKUP_REQUEST, $CREATE_BACKUP_WITHOUT_VALIDATION, $GET_DB_BACKUP_HISTORY, $GET_DBS_SIZE_LASTBKP_DATA, $GET_BACKUP_REQUEST_CANCEL, $SCHEDULE_JOB_INSERT_API, $GET_DB_SERVER_CLUSTERED_MODE_CHECK, $GET_DOMAIN_CREDENTIALS, $DELETE_DOMAIN_CREDENTIALS, $CREATE_DOMAIN_CREDENTIALS, $GET_CONN_STRING
$Endpoints = $GET_BACKUP_REQUEST_PROGRESS, $CREATE_BACKUP_REQUEST, $CREATE_BACKUP_WITHOUT_VALIDATION, $GET_DB_BACKUP_HISTORY, $GET_DBS_SIZE_LASTBKP_DATA, $GET_BACKUP_REQUEST_CANCEL, $SCHEDULE_JOB_INSERT_API, $GET_DB_SERVER_CLUSTERED_MODE_CHECK, $GET_DOMAIN_CREDENTIALS, $DELETE_DOMAIN_CREDENTIALS, $CREATE_DOMAIN_CREDENTIALS, $GET_CONN_STRING

Start-UDRestApi -Name Powershell-RestCalls-DB-Backup -Port 8808 -Endpoint $Endpoints -Wait


# --------------------------------------------------------------------------------------------
# 								Trigger Rest Api Server with UI Dashboard
# --------------------------------------------------------------------------------------------
#Start-UDDashboard -Endpoint $Endpoints -Wait -Dashboard (
#    New-UDDashboard -Title "DB Backup Rest Api Server" -Content {
#        New-UDCard -Title 'Powershell Rest Api DB Backup Server'
#    }
#)

# Get-UDRestApi -Name Powershell-RestCalls | Stop-UDRestApi